<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Crud extends CI_Model
{

	function insertData($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->affected_rows();
	}

	public function updateData($tbl, $data, $where)
	{
		$this->db->where($where);
		$this->db->update($tbl, $data);
		return $this->db->affected_rows();
	}

	public function deleteData($tbl, $where)
	{
		$this->db->where($where);
		$this->db->delete($tbl);
		return $this->db->affected_rows();
	}
}