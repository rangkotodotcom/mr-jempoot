<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Authapi_model extends CI_model
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('appsettings_model', 'app');
        $this->load->model('email_model', 'email_model');
    }

    public function signupdriver($data_signup, $data_kendaraan, $data_berkas, $data_kontak_darurat, $data_berkas_2)
    {
        $inskendaraan = $this->db->insert('vehicle', $data_kendaraan);
        $inserid = $this->db->insert_id();
        $datasignup = array(
            'id' => $data_signup['id'],
            'driver_name' => $data_signup['driver_name'],
            'user_nationid' => $data_signup['user_nationid'],
            'dob' => $data_signup['dob'],
            'phone_number' => $data_signup['phone_number'],
            'phone' => $data_signup['phone'],
            'email' => $data_signup['email'],
            'countrycode' => $data_signup['countrycode'],
            'photo' => $data_signup['photo'],
            'password' => $data_signup['password'],
            'job' => $data_signup['job'],
            'gender' => $data_signup['gender'],
            'driver_address' => $data_signup['driver_address'],
            'reg_id' => $data_signup['reg_id'],
            'vehicle' => $inserid,
            'driver_token'  => $this->uuid->v4(),
            'exp_token' => time() + 60 * 30,
            'status' => '0'
        );
        $signup = $this->db->insert('driver', $datasignup);




        $dataconfig = array(
            'driver_id' => $data_signup['id'],
            'latitude' => '0',
            'longitude' => '0',
            'status' => '5'
        );
        $insconfig = $this->db->insert('config_driver', $dataconfig);

        $databerkas = array(
            'driver_id' => $data_signup['id'],
            'idcard_images' => $data_berkas['idcard_images'],
            'driver_license_images' => $data_berkas['driver_license_images'],
            'driver_license_id' => $data_berkas['driver_license_id']
        );
        $insberkas = $this->db->insert('file_driver', $databerkas);

        $databerkas2 = array(
            'driver_id' => $data_signup['id'],
            'driver_stnk' => $data_berkas_2['driver_stnk'],
            'driver_skck' => $data_berkas_2['driver_skck'],
        );
        $insberkas2 = $this->db->insert('file_driver_2', $databerkas2);

        $datakontakdarurat = array(
            'user_id' => $data_signup['id'],
            'family_relationship_id' => $data_kontak_darurat['family_relationship_id'],
            'contact_number' => $data_kontak_darurat['contact_number'],
            'contact_name' => $data_kontak_darurat['contact_name']
        );
        $inskontakdarurat = $this->db->insert('emergency_contact', $datakontakdarurat);

        $datasaldo = array(
            'id_user' => $data_signup['id'],
            'balance' => 0
        );
        $insSaldo = $this->db->insert('balance', $datasaldo);
        return $signup;
    }


    public function sendLinkVerify($dataPost)
    {
        $id = $dataPost['id'];
        $driver_token = $this->uuid->v4();
        $exp_token = time() + 30 * 60;
        $email = $dataPost['email'];

        $dataUpdate = [
            'driver_token'  => $driver_token,
            'exp_token'     => $exp_token
        ];

        $data['app'] = $this->app->getappbyid();


        $subject = 'Verify Email';
        $emailmessage = 'Please click <a href="' . base_url() . '/api/authapi/verify/' . $id . '/' . $driver_token . '">here</a>';
        $host = $data['app']['smtp_host'];
        $port = $data['app']['smtp_port'];
        $username = $data['app']['smtp_username'];
        $password = $data['app']['smtp_password'];
        $from = $data['app']['smtp_from'];
        $appname = $data['app']['app_name'];
        $secure = $data['app']['smtp_secure'];
        $address = $data['app']['app_address'];
        $linkgoogle = $data['app']['app_linkgoogle'];
        $web = $data['app']['app_website'];

        $update = $this->db->update('driver', $dataUpdate, ['id' => $id]);

        if ($update) {
            $content = $this->email_model->template2($subject, $emailmessage, $address, $appname, $linkgoogle, $web);
            $success = $this->email_model->emailsend($subject, $email, $content, $host, $port, $username, $password, $from, $appname, $secure);

            $response = [
                'status' => true,
                'message' => 'success'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'failed'
            ];
        }

        return $response;
    }


    public function sendLinkVerifyCustomer($dataPost)
    {
        $id = $dataPost['id'];
        $token_customer = $this->uuid->v4();
        $exp_token = time() + 30 * 60;
        $email = $dataPost['email'];

        $dataUpdate = [
            'token_customer'  => $token_customer,
            'exp_token'     => $exp_token
        ];

        $data['app'] = $this->app->getappbyid();


        $subject = 'Verify Email';
        $emailmessage = 'Please click <a href="' . base_url() . '/api/authapi/verifycustomer/' . $id . '/' . $token_customer . '">here</a>';
        $host = $data['app']['smtp_host'];
        $port = $data['app']['smtp_port'];
        $username = $data['app']['smtp_username'];
        $password = $data['app']['smtp_password'];
        $from = $data['app']['smtp_from'];
        $appname = $data['app']['app_name'];
        $secure = $data['app']['smtp_secure'];
        $address = $data['app']['app_address'];
        $linkgoogle = $data['app']['app_linkgoogle'];
        $web = $data['app']['app_website'];

        $update = $this->db->update('customer', $dataUpdate, ['id' => $id]);

        if ($update) {
            $content = $this->email_model->template2($subject, $emailmessage, $address, $appname, $linkgoogle, $web);
            $success = $this->email_model->emailsend($subject, $email, $content, $host, $port, $username, $password, $from, $appname, $secure);

            $response = [
                'status' => true,
                'message' => 'success'
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'failed'
            ];
        }

        return $response;
    }


    public function verifyEmailDriver($dataPost)
    {
        $driver_token   = $dataPost['driver_token'];
        $id = $dataPost['id'];
        $timeNow = time();

        $checkId = $this->db->get_where('driver', ['id' => $id])->row_array();

        if ($checkId) {
            if ($checkId['driver_token'] == $driver_token) {
                if ($timeNow <= $checkId['exp_token']) {
                    $this->db->set('is_verify', 'yes');
                    $this->db->where('id', $id);
                    $verify = $this->db->update('driver');

                    if ($verify) {
                        $response = [
                            'status' => true,
                            'message' => 'success'
                        ];
                    } else {
                        $response = [
                            'status' => false,
                            'message' => 'failed'
                        ];
                    }
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'token expired'
                    ];
                }
            } else {
                $response = [
                    'status' => false,
                    'message' => 'wrong token'
                ];
            }
        } else {
            $response = [
                'status' => false,
                'message' => 'not found'
            ];
        }


        return $response;
    }


    public function verifyEmailCustomer($dataPost)
    {
        $token_customer   = $dataPost['token_customer'];
        $id = $dataPost['id'];
        $timeNow = time();

        $checkId = $this->db->get_where('customer', ['id' => $id])->row_array();

        if ($checkId) {
            if ($checkId['token_customer'] == $token_customer) {
                if ($timeNow <= $checkId['exp_token']) {
                    $this->db->set('is_verify', 'yes');
                    $this->db->where('id', $id);
                    $verify = $this->db->update('driver');

                    if ($verify) {
                        $response = [
                            'status' => true,
                            'message' => 'success'
                        ];
                    } else {
                        $response = [
                            'status' => false,
                            'message' => 'failed'
                        ];
                    }
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'token expired'
                    ];
                }
            } else {
                $response = [
                    'status' => false,
                    'message' => 'wrong token'
                ];
            }
        } else {
            $response = [
                'status' => false,
                'message' => 'not found'
            ];
        }


        return $response;
    }


    public function updateToken($id_user, $token)
    {
        $kode = substr($id_user, 0, 1);

        if ($kode == 'D') {
            $data = ['reg_id' => $token];
            $table = 'driver';
            $where = ['id' => $id_user];
        } else if ($kode == 'C') {
            $data = ['token' => $token];
            $table = 'customer';
            $where = ['id' => $id_user];
        } else if ($kode == 'M') {

            $merchant_id = $this->db->get_where('partner', ['partner_id' => $id_user])->row_array()['merchant_id'];

            $data = ['merchant_token' => $token];
            $table = 'merchant';
            $where = ['merchant_id' => $merchant_id];
        }

        return $this->db->update($table, $data, $where);
    }
}
