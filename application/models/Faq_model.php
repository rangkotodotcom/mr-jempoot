<?php


class Faq_model extends CI_model
{
    public function getAll()
    {
        return $this->db->get('tb_faq')->result_array();
    }


    public function getById($id)
    {
        return $this->db->get_where('tb_faq', ['id' => $id])->row_array();
    }


    public function save($data, $id = null)
    {
        if ($id == null) {
            return $this->db->insert('tb_faq', $data);
        } else {
            return $this->db->update('tb_faq', $data, ['id' => $id]);
        }
    }

    public function delete($id)
    {
        return $this->db->delete('tb_faq', ['id' => $id]);
    }
}
