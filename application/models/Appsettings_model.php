<?php


class Appsettings_model extends CI_model
{
    public function getcurrency()
    {
        $this->db->select('app_currency as currency');
        $this->db->where('id', '1');
        return $this->db->get('app_settings')->row('currency');
    }

    public function getallbanktransfer()
    {
        $this->db->select('*');
        $this->db->from('new_bank_list');
        return $this->db->get()->result_array();
    }

    public function getMenuAdmin()
    {
        $this->db->select('*');
        $this->db->from('menu_admin');
        $this->db->order_by('menu_admin.menu_id ASC');
        $menu = $this->db->get()->result_array();



        $result = array();
        foreach ($menu as $mnu) {
            $submenu = $this->getSubMenuAdmin($mnu['menu_id']);
            $result[] = array(
                "menu_id" => $mnu['menu_id'],
                "menu_icon" => $mnu['menu_icon'],
                "menu_title" => $mnu['menu_title'],
                "menu_url" => $mnu['menu_url'],
                "menu_sub" => $mnu['menu_sub'],
                "sub_menu" => $submenu
            );
        }

        return $result;
    }

    public function getSubMenuAdmin($id)
    {
        $this->db->select('*');
        $this->db->from('submenu_admin');
        $this->db->where('menu_id', $id);
        $this->db->order_by('submenu_admin.sub_id ASC');
        return $this->db->get()->result_array();
    }

    public function getappbyid()
    {
        return $this->db->get_where('app_settings', ['id' => '1'])->row_array();
    }

    public function editdataappsettings($data)
    {
        $this->db->where('id', '1');
        return $this->db->update('app_settings', $data);
    }

    public function adddatarekening($data)
    {
        return $this->db->insert('new_bank_list', $data);
    }

    public function deleterekening($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('new_bank_list');
    }

    public function getbankid($id)
    {
        $this->db->select('*');
        $this->db->from('new_bank_list');
        $this->db->where('id', $id);
        return $this->db->get()->row_array();
    }

    public function editdatarekening($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('new_bank_list', $data);
    }

    public function getBankCodeIris()
    {
        $method = 'GET';
        $url = 'iris/api/v1/beneficiary_banks';
        $type = 'query';

        $request = restclientapprover($method, $url, $type);

        $listbank = $request['beneficiary_banks'];

        $jmlInsert = 0;
        $jmlUpdate = 0;

        foreach ($listbank as $row) {

            $dataSave = [
                'kode_bank'     => $row['code'],
                'name_bank'     => $row['name'],
                'status_bank'   => '1'
            ];

            $cek = $this->db->get_where('new_bank_list', ['kode_bank' => $row['code']])->num_rows();

            if ($cek < 1) {
                $dataSave['id'] = $this->uuid->v4();

                $insert = $this->db->insert('new_bank_list', $dataSave);

                if ($insert) {
                    $jmlInsert++;
                }
            } else {
                $update = $this->db->update('new_bank_list', $dataSave, ['kode_bank' => $row['code']]);

                if ($update) {
                    $jmlUpdate++;
                }
            }
        }

        $return = [
            'insert' => $jmlInsert,
            'update' => $jmlUpdate
        ];

        return $return;
    }
}
