<?php


class Additional_model extends CI_model
{
    public function getAllFamilyRelation()
    {
        return  $this->db->get('family_relationship')->result_array();
    }

    public function addfamilyrelation($data)
    {
        $this->db->set('id', 'UUID()', FALSE);
        return $this->db->insert('family_relationship', $data);
    }

    public function editfamilyrelation($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('family_relationship', $data);
    }

    public function deletefamilyrelation($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('family_relationship');
    }
    
    public function getAllItemCuisine()
    {
        return  $this->db->get('item_cuisine')->result_array();
    }

    public function additemcuisine($data)
    {
        return $this->db->insert('item_cuisine', $data);
    }

    public function getitemcuisinebyid($id)
    {
        $this->db->from('item_cuisine');
        $this->db->where('id_cuisine', $id);
        return $this->db->get()->row_array();
    }

    public function edititemcuisine($data, $id)
    {
        $this->db->where('id_cuisine', $id);
        return $this->db->update('item_cuisine', $data);
    }

    public function deleteitemcuisine($id)
    {
        $this->db->where('id_cuisine', $id);
        return $this->db->delete('item_cuisine');
    }
    
    public function getAllItemTag()
    {
        return  $this->db->get('item_tag')->result_array();
    }

    public function additemtag($data)
    {
        return $this->db->insert('item_tag', $data);
    }

    public function getitemtagbyid($id)
    {
        $this->db->from('item_tag');
        $this->db->where('id_tag', $id);
        return $this->db->get()->row_array();
    }

    public function edititemtag($data, $id)
    {
        $this->db->where('id_tag', $id);
        return $this->db->update('item_tag', $data);
    }

    public function deleteitemtag($id)
    {
        $this->db->where('id_tag', $id);
        return $this->db->delete('item_tag');
    }
    
    
    public function getAllsouvenir()
    {
        $this->db->select('*');
        return  $this->db->get('souvenir_poin')->result_array();
    }
    

    public function getsouvenirbyid($id)
    {
        $this->db->select('*');
        $this->db->where('id_souvenir', $id);
        return $this->db->get('souvenir_poin')->row_array();
    }
    
    public function adddatasouvenir($data)
    {
        return $this->db->insert('souvenir_poin', $data);

    }
    
    public function editdatasouvenir($data)
    {
        $this->db->set('img_souvenir', $data['img_souvenir']);
        $this->db->set('name_souvenir', $data['name_souvenir']);
        $this->db->set('poin_buy', $data['poin_buy']);
        $this->db->set('status_souvenir', $data['status_souvenir']);
        $this->db->where('id_souvenir', $data['id_souvenir']);
        return $this->db->update('souvenir_poin');

    }

    public function deletesouvenirById($id)
    {
        $this->db->where('id_souvenir', $id);
        return $this->db->delete('souvenir_poin');
    }
}