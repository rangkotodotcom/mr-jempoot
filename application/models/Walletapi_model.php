<?php


class Walletapi_model extends CI_model
{

    public function getwallet($driver_id, $startdate, $enddate)
    {
        $this->db->select('driver.driver_name');
        $this->db->select('partner.partner_name');
        $this->db->select('tb_new_wallet.*');
        $this->db->select('user_rek.no_rek, user_rek.nama_rek');
        $this->db->select('new_bank_list.name_bank, new_bank_list.kode_bank');

        $this->db->join('driver', 'tb_new_wallet.id_user = driver.id', 'left');
        $this->db->join('partner', 'tb_new_wallet.id_user = partner.partner_id', 'left');
        $this->db->join('user_rek', 'tb_new_wallet.keterangan = user_rek.id', 'left');
        $this->db->join('new_bank_list', 'user_rek.id_bank=new_bank_list.id', 'left');

        $this->db->order_by('tb_new_wallet.date', 'DESC');
        $this->db->where('id_user', $driver_id);
        // $this->db->where('tb_new_wallet.status', '2');

        if ($startdate != '' && $enddate != '') {
            $this->db->where('date(date) >=', $startdate);
            $this->db->where('date(date) <=', $enddate);
        }

        return $this->db->get('tb_new_wallet')->result_array();
    }

    public function getallsaldouser($driver_id)
    {
        $this->db->select('driver.driver_name');
        $this->db->select('partner.partner_name');
        $this->db->select('balance.*');
        $this->db->join('driver', 'balance.id_user = driver.id', 'left');
        $this->db->join('partner', 'tb_new_wallet.id_user = partner.partner_id', 'left');
        $this->db->where('id_user', $driver_id);
        return $this->db->get('balance')->result_array();
    }

    public function gettotaldiscount($driver_id, $startdate, $enddate)
    {
        $this->db->select('SUM(promo_discount) as discount');
        $this->db->join('transaction_history', 'transaction.id = transaction_history.transaction_id', 'left');
        $this->db->where('transaction_history.status', 4);
        $this->db->where('transaction_history.driver_id', $driver_id);
        if ($startdate != '' && $enddate != '') {
            $this->db->where('date(transaction_history.date) >=', $startdate);
            $this->db->where('date(transaction_history.date) <=', $enddate);
        }
        return $this->db->get('transaction')->row_array();
    }

    public function gettotalorderplus($driver_id, $startdate, $enddate)
    {
        $this->db->select('SUM(wallet_amount)as total');
        $this->db->where('type', 'Order+');
        $this->db->where('id_user', $driver_id);
        if ($startdate != '' && $enddate != '') {
            $this->db->where('date(date) >=', $startdate);
            $this->db->where('date(date) <=', $enddate);
        }
        return $this->db->get('tb_new_wallet')->row_array();
    }

    public function gettotalordermin($driver_id, $startdate, $enddate)
    {
        $this->db->select('SUM(wallet_amount)as total');
        $this->db->where('type', 'Order-');
        $this->db->where('id_user', $driver_id);
        if ($startdate != '' && $enddate != '') {
            $this->db->where('date(date) >=', $startdate);
            $this->db->where('date(date) <=', $enddate);
        }
        return $this->db->get('tb_new_wallet')->row_array();
    }

    public function gettotalwithdraw($driver_id, $startdate, $enddate)
    {
        $this->db->select('SUM(wallet_amount)as total');
        $this->db->where('type', 'Withdraw');
        $this->db->where('status', 2);
        $this->db->where('id_user', $driver_id);
        if ($startdate != '' && $enddate != '') {
            $this->db->where('date(date) >=', $startdate);
            $this->db->where('date(date) <=', $enddate);
        }
        return $this->db->get('tb_new_wallet')->row_array();
    }

    public function gettotaltopup($driver_id, $startdate, $enddate)
    {
        $this->db->select('SUM(wallet_amount)as total');
        $this->db->where('status', 2);
        $this->db->where('type', 'Topup');
        $this->db->where('id_user', $driver_id);
        if ($startdate != '' && $enddate != '') {
            $this->db->where('date(date) >=', $startdate);
            $this->db->where('date(date) <=', $enddate);
        }
        return $this->db->get('tb_new_wallet')->row_array();
    }

    public function getallbalance($driver_id)
    {
        $this->db->select('SUM(balance)as total');
        $this->db->where('id_user', $driver_id);
        return $this->db->get('balance')->row_array();
    }

    public function getrekbank($driver_id)
    {
        $this->db->select('user_rek.*');
        $this->db->select('new_bank_list.name_bank');
        $this->db->join('new_bank_list', 'user_rek.id_bank=new_bank_list.id', 'left');
        $this->db->where('user_id', $driver_id);
        return $this->db->get('user_rek')->result_array();
    }

    public function getrekbankbyid($id)
    {
        $this->db->select('user_rek.*');
        $this->db->select('new_bank_list.kode_bank');
        $this->db->select('new_bank_list.name_bank');
        $this->db->join('new_bank_list', 'user_rek.id_bank=new_bank_list.id', 'left');
        $this->db->where('user_rek.id', $id);
        return $this->db->get('user_rek')->row_array();
    }

    public function saverekbank($datapost)
    {
        $method = 'GET';
        $url = 'iris/api/v1/account_validation';
        $type = 'query';

        $typeP = 'raw';

        $kode_rek = random_string('alnum', 20);

        $user_id = $datapost['user_id'];
        $id_bank = $datapost['id_bank'];
        $no_rek = $datapost['no_rek'];
        $nama_rek = $datapost['nama_rek'];
        $buku_tab = $datapost['buku_tab'];

        if ($id_bank == '' or $id_bank == 'null') {
            $return = [
                'status'    => false,
                'message'   => 'Bank cannot empty'
            ];

            return $return;
        }

        $kode_bank = $this->getalllistbank($id_bank)['kode_bank'];

        $data = [
            'bank'      => $kode_bank,
            'account'   => $no_rek
        ];

        $validationRek = restclientapprover($method, $url, $type, $data);

        if (array_key_exists('errors', $validationRek)) {
            $return = [
                'status'    => false,
                'message'   => $validationRek['errors']['account'][0]
            ];
        } else {

            if ($validationRek['account_name'] == $nama_rek) {

                $kodeUser = substr($user_id, 0, 1);

                if ($kodeUser == 'D') {
                    $user = $this->db->get_where('driver', ['id' => $user_id])->row_array();

                    $email = $user['email'];
                } else if ($kodeUser == 'M') {

                    $user = $this->db->get_where('partner', ['partner_id' => $user_id])->row_array();

                    $email = $user['partner_email'];
                } else if ($kodeUser == 'C') {

                    $user = $this->db->get_where('customer', ['id' => $user_id])->row_array();

                    $email = $user['email'];
                } else {
                    $email = 'email@domain.com';
                }

                $dataSave = [
                    'user_id'   => $user_id,
                    'kode_rek'  => $kode_rek,
                    'id_bank'   => $id_bank,
                    'no_rek'    => $no_rek,
                    'nama_rek'  => $nama_rek,
                    'buku_tab'  => $buku_tab
                ];

                $dataRaw = [
                    'name'          => $nama_rek,
                    'account'       => $no_rek,
                    'bank'          => $kode_bank,
                    'alias_name'    => $kode_rek,
                    'email'         => $email
                ];

                if (!array_key_exists('id', $datapost)) {
                    $dataSave['id'] = $this->uuid->v4();

                    $methodI = 'POST';
                    $urlI = 'iris/api/v1/beneficiaries';

                    $created = restclientapprover($methodI, $urlI, $typeP, json_encode($dataRaw));

                    if ($created['status'] == 'created') {
                        $save = $this->db->insert('user_rek', $dataSave);
                    } else {
                        $save = false;
                    }
                } else {
                    $id = $datapost['id'];

                    $methodI = 'PATCH';
                    $urlI = 'iris/api/v1/beneficiaries/' . $user_id;

                    $dataSave['buku_tab'] = $buku_tab;

                    $updated = restclientapprover($methodI, $urlI, $typeP, json_encode($dataRaw));

                    if ($updated['status'] == 'updated') {
                        $save = $this->db->update('user_rek', $dataSave, ['id' => $id]);
                    } else {
                        $save = false;
                    }
                }

                if ($save) {
                    $return = [
                        'status'    => true,
                        'message'   => 'Bank Account has been saved'
                    ];
                } else {
                    $return = [
                        'status'    => false,
                        'message'   => 'Failed to save bank account'
                    ];
                }
            } else {
                $return = [
                    'status'    => false,
                    'message'   => 'Account name not same'
                ];
            }
        }
        return $return;
    }


    public function savewithdraw($datapost)
    {
        $id_user = $datapost['id_user'];
        $wallet_amount = $datapost['wallet_amount'];
        $ket = $datapost['id_rekbank'];
        $type = 'Withdraw';
        $status = 1;

        $kodeUser = substr($id_user, 0, 1);

        if ($kodeUser == 'D') {
            $user = $this->db->get_where('driver', ['id' => $id_user])->row_array();

            $email = $user['email'];
        } else if ($kodeUser == 'M') {

            $user = $this->db->get_where('partner', ['partner_id' => $id_user])->row_array();

            $email = $user['partner_email'];
        } else if ($kodeUser == 'C') {

            $user = $this->db->get_where('customer', ['id' => $id_user])->row_array();

            $email = $user['email'];
        } else {
            $email = 'email@domain.com';
        }


        $notes = 'Withdraw ' . date('j F Y');

        $dataUserRek = $this->getrekbankbyid($ket);

        $nama_rek = $dataUserRek['nama_rek'];
        $no_rek = $dataUserRek['no_rek'];
        $kode_bank = $dataUserRek['kode_bank'];

        $method = 'POST';
        $url = 'iris/api/v1/payouts';
        $typeP = 'raw';

        $dataSave = [
            'id'                => $this->uuid->v4(),
            'id_user'           => $id_user,
            'wallet_amount'     => $wallet_amount,
            'type'              => $type,
            'keterangan'        => $ket,
            'status'            => $status
        ];

        $dataRaw = [
            'payouts' => [
                [
                    'beneficiary_name'      => $nama_rek,
                    'beneficiary_account'   => $no_rek,
                    'beneficiary_bank'      => $kode_bank,
                    'beneficiary_email'     => $email,
                    'amount'                => $wallet_amount,
                    'notes'                 => $notes
                ]
            ]
        ];

        $postRaw = restclientcreator($method, $url, $typeP, json_encode($dataRaw));

        if (array_key_exists('payouts', $postRaw)) {

            $dataWalletPayout = [
                'id_wallet_payout'  => $this->uuid->v4(),
                'id_wallet'         => $dataSave['id'],
                'status_payout'     => $postRaw['payouts'][0]['status'],
                'reference_no'      => $postRaw['payouts'][0]['reference_no'],
                'note'              => $notes
            ];

            $save = $this->db->insert('tb_new_wallet', $dataSave);

            if ($save) {
                $saveWallet = $this->db->insert('tb_wallet_payout', $dataWalletPayout);
            } else {
                $saveWallet = false;
            }
        } else {
            $saveWallet = false;
        }

        return $saveWallet;
    }


    public function getalllistbank($id_bank = null)
    {
        $this->db->order_by('name_bank', 'ASC');
        if ($id_bank != null) {
            $this->db->where('id', $id_bank);
            return $this->db->get('new_bank_list')->row_array();
        } else {
            $this->db->where('status_bank', '1');
            return $this->db->get('new_bank_list')->result_array();
        }
    }


    public function getwalletpoin($driver_id)
    {
        $this->db->select('driver.driver_name');
        $this->db->select('partner.partner_name');
        $this->db->select('tb_wallet_poin.*');
        $this->db->join('driver', 'tb_wallet_poin.id_user = driver.id', 'left');
        $this->db->join('partner', 'tb_wallet_poin.id_user = partner.partner_id', 'left');
        $this->db->order_by('date_wallet_poin', 'DESC');
        $this->db->where('id_user', $driver_id);
        return $this->db->get('tb_wallet_poin')->result_array();
    }

    public function getallbalancepoin($driver_id)
    {
        $this->db->select('SUM(balance_poin)as total, balance_poin.*');
        $this->db->where('id_user', $driver_id);
        return $this->db->get('balance_poin')->row_array();
    }

    public function savepoin($datapost)
    {
        $id_wallet_poin = $this->uuid->v4();
        $id_user = $datapost['id_user'];
        $amount_poin = $datapost['amount_poin'];
        $type = 'Add';
        $keterangan_poin = $datapost['keterangan_poin'];

        $dataSave = [
            'id_wallet_poin'    => $id_wallet_poin,
            'id_user'           => $id_user,
            'amount_poin'       => $amount_poin,
            'type'              => $type,
            'keterangan_poin'   => $keterangan_poin
        ];

        $savepoin = $this->db->insert('tb_wallet_poin', $dataSave);

        if ($savepoin) {
            $balance_poin = $this->db->get_where('balance_poin', ['id_user' => $id_user])->row_array();

            if ($balance_poin) {
                $oldpoin = $balance_poin['balance_poin'];
                $newpoin = $oldpoin + $amount_poin;

                $savebalancepoin = $this->db->update('balance_poin', ['balance_poin' => $newpoin], ['id_user' => $id_user]);
            } else {
                $datasavebalance = [
                    'id_poin'       => $this->uuid->v4(),
                    'id_user'       => $id_user,
                    'balance_poin'  => $amount_poin
                ];

                $savebalancepoin = $this->db->insert('balance_poin', $datasavebalance);
            }

            return $savebalancepoin;
        } else {
            return false;
        }
    }


    public function getsouvenir($id_souvenir = null)
    {
        if ($id_souvenir == null) {

            $this->db->where('status_souvenir', 1);
            return $this->db->get('souvenir_poin')->result_array();
        } else {
            $this->db->where('id_souvenir', $id_souvenir);
            return $this->db->get('souvenir_poin')->row_array();
        }
    }


    public function saveredeempoin($datapost)
    {
        $id_redeem_poin = $this->uuid->v4();
        $id_user = $datapost['id_user'];
        $id_souvenir = $datapost['id_souvenir'];
        $status_redeem = 0;

        $balance_poin = $this->getallbalancepoin($id_user)['total'];
        $poin_buy = $this->getsouvenir($id_souvenir)['poin_buy'];

        $dataSave = [
            'id_redeem_poin'    => $id_redeem_poin,
            'id_user'           => $id_user,
            'id_souvenir'       => $id_souvenir,
            'status_redeem'     => $status_redeem
        ];

        if ($balance_poin < $poin_buy) {
            return false;
        } else {

            $saveredeempoin = $this->db->insert('tb_redeem_poin', $dataSave);

            if ($saveredeempoin) {

                $oldpoin = $balance_poin;
                $newpoin = $oldpoin - $poin_buy;

                $savebalancepoin = $this->db->update('balance_poin', ['balance_poin' => $newpoin], ['id_user' => $id_user]);

                return $savebalancepoin;
            } else {
                return false;
            }
        }
    }
}
