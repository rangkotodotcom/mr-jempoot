<?php

class Chatcs_model extends CI_Model
{
    public function startChat($id_user)
    {
        $id = $this->uuid->v4();

        $dataStart = [
            'id'                => $id,
            'id_user'           => $id_user,
            'status_message'    => 0
        ];

        $startChat = $this->db->insert('message', $dataStart);


        if ($startChat) {
            $response = [
                'status'    => true,
                'data'      => [
                    'id_message'    => $id
                ]
            ];
        } else {
            $response = [
                'status'    => false,
                'data'      => ''
            ];
        }

        return $response;
    }

    public function endChat($id_message)
    {

        $endChat = $this->db->update('message', ['status_message' => 1], ['id' => $id_message]);


        if ($endChat) {
            $response = [
                'status'    => true,
                'data'      => [
                    'id_message'    => $id_message
                ]
            ];
        } else {
            $response = [
                'status'    => false,
                'data'      => ''
            ];
        }

        return $response;
    }

    public function sendChat($dataSendChat)
    {
        $id = $this->uuid->v4();

        $dataSendChat['id'] = $id;

        $id_message = $dataSendChat['id_message'];

        $sendChat = $this->db->insert('message_detail', $dataSendChat);

        $dataChat = $this->getChat($id_message);


        if ($sendChat) {
            $response = [
                'status'    => true,
                'data'      => $dataChat
            ];
        } else {
            $response = [
                'status'    => false,
                'data'      => ''
            ];
        }

        return $response;
    }

    public function getChat($id_message = null)
    {
        if ($id_message == null) {

            $this->db->select('message.*');
            $this->db->select('driver.driver_name, driver.photo as foto_driver');
            $this->db->select('customer.customer_fullname, customer.customer_image as foto_customer');
            $this->db->select('partner.partner_name');
            $this->db->select('merchant.merchant_image as foto_merchant');

            $this->db->join('driver', 'driver.id=message.id_user', 'left');
            $this->db->join('customer', 'customer.id=message.id_user', 'left');
            $this->db->join('partner', 'partner.partner_id=message.id_user', 'left');
            $this->db->join('merchant', 'merchant.merchant_id=partner.merchant_id', 'left');

            $this->db->order_by('message.date_created', 'DESC');

            $chat = $this->db->get('message')->result_array();

            if (!empty($chat)) {
                $dataResponse = $this->_extract_chat($chat, true);
            } else {
                $dataResponse = false;
            }
        } else {

            $this->db->select('message.*');
            $this->db->select('driver.driver_name, driver.photo as foto_driver');
            $this->db->select('customer.customer_fullname, customer.customer_image as foto_customer');
            $this->db->select('partner.partner_name');
            $this->db->select('merchant.merchant_image as foto_merchant');

            $this->db->join('driver', 'driver.id=message.id_user', 'left');
            $this->db->join('customer', 'customer.id=message.id_user', 'left');
            $this->db->join('partner', 'partner.partner_id=message.id_user', 'left');
            $this->db->join('merchant', 'merchant.merchant_id=partner.merchant_id', 'left');

            $chat = $this->db->get_where('message', ['message.id' => $id_message])->row_array();

            if (!empty($chat)) {
                $dataResponse = $this->_extract_chat($chat, false);
            } else {
                $dataResponse = false;
            }
        }

        if ($dataResponse) {
            $response = [
                'status'    => true,
                'data'      => $dataResponse,
            ];
        } else {
            $response = [
                'status'    => false,
                'data'      => []
            ];
        }

        return $response;
    }


    private function _searchDetailChat($id_message)
    {
        $this->db->order_by('created_at', 'ASC');
        $detailChat = $this->db->get_where('message_detail', ['id_message' => $id_message])->result_array();

        return $detailChat;
    }



    private function _extract_chat($dataChat, $multiple)
    {
        if ($multiple) {
            foreach ($dataChat as $row) {
                $row['url_img_driver'] = base_url('images/driverphoto');
                $row['url_img_customer'] = base_url('images/customer');
                $row['url_img_merchant'] = base_url('images/merchant');
                $row['url_img_chat'] = base_url('images/chatcs');

                $row['detail_chat'] = $this->_searchDetailChat($row['id']);

                $newDataResponse[] = $row;
            }
        } else {
            $row = $dataChat;


            $row['url_img_driver'] = base_url('images/driverphoto');
            $row['url_img_customer'] = base_url('images/customer');
            $row['url_img_merchant'] = base_url('images/merchant');
            $row['url_img_chat'] = base_url('images/chatcs');

            $row['detail_chat'] = $this->_searchDetailChat($row['id']);

            $newDataResponse = $row;
        }

        return $newDataResponse;
    }
}
