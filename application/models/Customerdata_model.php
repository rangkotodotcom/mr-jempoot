<?php


class Customerdata_model extends CI_model
{
    public function getAllcustomer()
    {
        $this->db->order_by('id DESC');
        return $this->db->get('customer')->result_array();
    }

    public function getcustomerbyid($id)
    {
        $this->db->select('balance.balance');
        $this->db->select('customer.*');
        $this->db->join('balance', 'customer.id = balance.id_user', 'left');
        return  $this->db->get_where('customer', ['customer.id' => $id])->row_array();
    }

    public function wallet($id)
    {
        $this->db->order_by('tb_new_wallet.date', 'DESC');
        return $this->db->get_where('tb_new_wallet', ['id_user' => $id])->result_array();
    }

    public function countorder($id)
    {

        $this->db->select('transaction_status.*');
        $this->db->select('transaction_history.*');
        $this->db->select('service.*');
        $this->db->select('transaction.*');
        $this->db->join('transaction_history', 'transaction.id = transaction_history.transaction_id', 'left');
        $this->db->join('transaction_status', 'transaction_history.status = transaction_status.id', 'left');
        $this->db->join('service', 'transaction.service_order = service.service_id', 'left');
        $this->db->where('transaction_history.status != 1');
        $this->db->order_by('transaction.id', 'DESC');
        $query =    $this->db->get_where('transaction', ['customer_id' => $id])->result_array();
        return $query;
    }
    public function getcurrency()
    {
        $this->db->select('app_currency as duit');
        $this->db->where('id', '1');
        return $this->db->get('app_settings')->row_array();
    }

    public function editdatacustomer($data)
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('customer', $data);
    }

    public function adddatacustomer($data)
    {
        $this->db->insert('customer', $data);
        $data2 = [
            'id_user' => $data['id'],
            'balance'   => 0,
        ];
        return $this->db->insert('balance', $data2);
    }

    public function getusersbyid($id)
    {
        $this->db->select('balance.balance');
        $this->db->select('customer.*');
        $this->db->join('balance', 'customer.id = balance.id_user', 'left');
        return  $this->db->get_where('customer', ['customer.id' => $id])->row_array();
    }

    public function deletedatauserbyid($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('customer');

        $this->db->where('customer_id', $id);
        $this->db->delete('transaction');

        $this->db->where('id_user', $id);
        $this->db->delete('balance');

        $this->db->where('userid', $id);
        $this->db->delete('forgot_password');

        $this->db->where('customer_id', $id);
        $this->db->delete('driver_rating');

        $this->db->where('id_user', $id);
        $this->db->delete('tb_new_wallet');
        return true;
    }

    public function blockusersById($id)
    {
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        return $this->db->update('customer');
    }

    public function unblockusersById($id)
    {
        $this->db->set('status', 1);
        $this->db->where('id', $id);
        return $this->db->update('customer');
    }

    public function getAllusers()
    {
        return $this->db->get('customer')->result_array();
    }

    public function getMember($idmember = null)
    {
        $this->db->select('*');
        $this->db->from('customer_member');
        $this->db->join('customer', 'customer_member.id_customer=customer.id');
        if ($idmember == null) {
            return $this->db->get()->result_array();
        } else {
            $this->db->where('id_member', $idmember);
            return $this->db->get()->row_array();
        }
    }

    public function getTransaksi($idmember)
    {
        $customer_id = $this->getMember($idmember)['id_customer'];

        $this->db->select('*');

        $this->db->where('transaction.customer_id', $customer_id);
        $this->db->where('transaction_history.status', '4');

        $this->db->join('transaction_history', 'transaction.id=transaction_history.transaction_id');

        return $this->db->get('transaction');
    }

    public function saveRequestMember($idmember, $kode)
    {
        if ($kode == '0') {
            $this->db->set('status_member', '3');
        } else {
            $type = 'c';
            $oneyear = mktime(0, 0, 0, date("n"), date("j"), date("Y") + 1);
            $valid_until = date("Y-m-d", $oneyear);

            $no_member = date('Ymd') . random_string('numeric');

            $this->db->set('no_member', $no_member);
            $this->db->set('status_member', '2');
            $this->db->set('type', $type);
            $this->db->set('valid_until', $valid_until);
        }

        $this->db->where('id_member', $idmember);

        return $this->db->update('customer_member');
    }


    public function saveStatusMember($idmember, $status)
    {
        $this->db->set('status_member', $status);
        $this->db->where('id_member', $idmember);
        return $this->db->update('customer_member');
    }


    public function saveUpgradeMember($idmember)
    {
        $jlmTrx = $this->getTransaksi($idmember)->num_rows();

        $typeMember = $this->getMember($idmember)['type'];


        $oneyear = mktime(0, 0, 0, date("n"), date("j"), date("Y") + 1);
        $valid_until = date("Y-m-d", $oneyear);

        if ($typeMember == 'c') {
            $newType = 's';
            $minTrx = 100;

            if ($jlmTrx < $minTrx) {
                return false;
            } else {
                $this->db->set('type', $newType);
                $this->db->set('valid_until', $valid_until);
            }
        } else if ($typeMember == 's') {
            $newType = 'g';
            $minTrx = 200;

            if ($jlmTrx < $minTrx) {
                return false;
            } else {
                $this->db->set('type', $newType);
                $this->db->set('valid_until', $valid_until);
            }
        } else {
            return false;
        }

        $this->db->where('id_member', $idmember);

        return $this->db->update('customer_member');
    }
}
