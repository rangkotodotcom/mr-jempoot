<?php


class Poin_model extends CI_model
{

    public function getwalletpoin()
    {
        $this->db->select('driver.driver_name');
        $this->db->select('customer.customer_fullname');
        $this->db->select('tb_wallet_poin.*');
        $this->db->join('driver', 'tb_wallet_poin.id_user = driver.id', 'left');
        $this->db->join('customer', 'tb_wallet_poin.id_user = customer.id', 'left');
        $this->db->order_by('tb_wallet_poin.date_wallet_poin', 'DESC');
        return $this->db->get('tb_wallet_poin')->result_array();
    }

    public function gettotalpoinadd()
    {
        $this->db->select('SUM(amount_poin)as total');
        $this->db->where('type', 'Add');
        return $this->db->get('tb_wallet_poin')->row_array();
    }

    public function gettotalpoinredeem()
    {
        $this->db->select('SUM(amount_poin)as total');
        $this->db->where('type', 'Redeem');
        return $this->db->get('tb_wallet_poin')->row_array();
    }

    public function getallbalancepoin()
    {
        $this->db->select('SUM(balance_poin)as total');
        return $this->db->get('balance_poin')->row_array();
    }

    public function getredeembyid($id)
    {
        $this->db->select('tb_redeem_poin.*');
        $this->db->select('souvenir_poin.*');
        $this->db->select('customer.customer_fullname');
        $this->db->select('driver.driver_name');

        $this->db->where('id_redeem_poin', $id);

        $this->db->join('souvenir_poin', 'tb_redeem_poin.id_souvenir=souvenir_poin.id_souvenir', 'left');
        $this->db->join('customer', 'tb_redeem_poin.id_user=customer.id', 'left');
        $this->db->join('driver', 'tb_redeem_poin.id_user=driver.id', 'left');

        return $this->db->get('tb_redeem_poin')->row_array();
    }

    public function getredeemcustomer()
    {
        $this->db->select('tb_redeem_poin.*');
        $this->db->select('souvenir_poin.*');
        $this->db->select('customer.customer_fullname');

        $this->db->like('id_user', 'C', 'after');

        $this->db->join('souvenir_poin', 'tb_redeem_poin.id_souvenir=souvenir_poin.id_souvenir', 'left');
        $this->db->join('customer', 'tb_redeem_poin.id_user=customer.id', 'left');


        return $this->db->get('tb_redeem_poin')->result_array();
    }

    public function getredeemcustomerpending()
    {
        $this->db->select('tb_redeem_poin.*');
        $this->db->select('souvenir_poin.*');
        $this->db->select('customer.customer_fullname');

        $this->db->like('id_user', 'C', 'after');

        $this->db->where('status_redeem', '0');

        $this->db->join('souvenir_poin', 'tb_redeem_poin.id_souvenir=souvenir_poin.id_souvenir', 'left');
        $this->db->join('customer', 'tb_redeem_poin.id_user=customer.id', 'left');

        return $this->db->get('tb_redeem_poin')->result_array();
    }

    public function getredeemdriver()
    {
        $this->db->select('tb_redeem_poin.*');
        $this->db->select('souvenir_poin.*');
        $this->db->select('driver.driver_name');

        $this->db->like('id_user', 'D', 'after');

        $this->db->join('souvenir_poin', 'tb_redeem_poin.id_souvenir=souvenir_poin.id_souvenir', 'left');
        $this->db->join('driver', 'tb_redeem_poin.id_user=driver.id', 'left');

        return $this->db->get('tb_redeem_poin')->result_array();
    }

    public function getredeemdriverpending()
    {
        $this->db->select('tb_redeem_poin.*');
        $this->db->select('souvenir_poin.*');
        $this->db->select('driver.driver_name');

        $this->db->like('id_user', 'D', 'after');

        $this->db->where('status_redeem', '0');

        $this->db->join('souvenir_poin', 'tb_redeem_poin.id_souvenir=souvenir_poin.id_souvenir', 'left');
        $this->db->join('driver', 'tb_redeem_poin.id_user=driver.id', 'left');

        return $this->db->get('tb_redeem_poin')->result_array();
    }


    public function savepoinwallet($data)
    {
        $this->db->set('id_wallet_poin', 'UUID()', FALSE);
        $this->db->set('type', 'Redeem');
        $this->db->set('keterangan_poin', 'Redeem poin dengan ' . $data['name_souvenir']);
        $this->db->set('amount_poin', $data['amount_poin']);
        $this->db->set('id_user', $data['id_user']);
        return $this->db->insert('tb_wallet_poin');
    }

    public function editstatusredeem($id, $status)
    {
        $this->db->set('status_redeem', $status);
        $this->db->where('id_redeem_poin', $id);
        return $this->db->update('tb_redeem_poin');
    }

    public function editpoinredeem($id_user, $amount_poin, $balance_poin)
    {
        $this->db->set('balance_poin', $balance_poin['balance_poin'] - $amount_poin);
        $this->db->where('id_user', $id_user);
        return $this->db->update('balance_poin');
    }

    public function send_notif($title, $message, $topic)
    {

        $data = array(
            'title' => $title,
            'message' => $message,
            'type' => 3
        );
        $senderdata = array(
            'data' => $data,
            'to' => $topic
        );

        $headers = array(
            'Content-Type : application/json',
            'Authorization: key=' . keyfcm
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($senderdata),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
    }


    public function gettoken($id_user)

    {
        $this->db->select('token');
        $this->db->where('id', $id_user);
        return $this->db->get('customer')->row_array();
    }

    public function getregid($id_user)
    {
        $this->db->select('reg_id');
        $this->db->where('id', $id_user);
        return $this->db->get('driver')->row_array();
    }

    public function getbalancepoin($id_user)
    {
        $this->db->select('balance_poin');
        $this->db->where('id_user', $id_user);
        return $this->db->get('balance_poin')->row_array();
    }
}
