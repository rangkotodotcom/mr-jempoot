<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Merchantapi_model extends CI_model
{

    public function check_banned($phone)
    {
        $stat =  $this->db->query("SELECT partner_id FROM partner WHERE partner_status='3' AND partner_telephone='$phone'");
        if ($stat->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function check_exist($email, $phone)
    {
        $cek = $this->db->query("SELECT partner_id FROM partner where partner_email = '$email' AND partner_telephone='$phone'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function check_exist_phone($phone)
    {
        $cek = $this->db->query("SELECT partner_id FROM partner where partner_telephone='$phone'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function check_exist_email($email)
    {
        $cek = $this->db->query("SELECT partner_id FROM partner where partner_email='$email'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function check_ktp($ktp)
    {
        $cek = $this->db->query("SELECT partner_id FROM partner where partner_identity_number='$ktp'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function get_data_merchant($condition)
    {
        $this->db->select('partner.*, balance.balance, merchant.*');
        $this->db->from('partner');
        $this->db->join('balance', 'partner.partner_id = balance.id_user');
        $this->db->join('merchant', 'partner.merchant_id = merchant.merchant_id');
        $this->db->where($condition);
        $this->db->where('partner_status', '1');
        return $this->db->get();
    }

    public function onmerchant($data, $where)
    {
        $this->db->where($where);
        $this->db->update('merchant', $data);
        return true;
    }

    public function edit_profile_token($data, $phone)
    {
        $this->db->where('merchant_telephone_number', $phone);
        $this->db->update('merchant', $data);
        return true;
    }

    public function edit_profile($data, $phone)
    {
        $this->db->where('partner_telephone', $phone);
        $this->db->update('partner', $data);
        return true;
    }

    public function edit_profile_mitra_merchant($data, $phone)
    {
        $datamitra = [
            'partner_name' => $data['nama'],
            'partner_telephone' => $data['phone_number'],
            'partner_phone' => $data['phone'],
            'partner_email' => $data['email'],
            'partner_country_code' => $data['countrycode'],
            'partner_address' => $data['alamat']
        ];

        $datamerchant = [
            'merchant_telephone_number' => $data['phone_number'],
            'merchant_phone_number' => $data['phone'],
            'merchant_country_code' => $data['countrycode']
        ];

        $this->db->where('merchant_telephone_number', $phone);
        $this->db->update('merchant', $datamerchant);

        $this->db->where('partner_telephone', $phone);
        $this->db->update('partner', $datamitra);
        return true;
    }

    public function signup($data_signup, $data_merchant, $data_berkas)
    {
        $this->db->insert('merchant', $data_merchant);
        $inserid = $this->db->insert_id();
        $datasignup = array(
            'partner_id' => $data_signup['partner_id'],
            'partner_name' => $data_signup['partner_name'],
            'partner_type_identity' => $data_signup['partner_type_identity'],
            'partner_identity_number' => $data_signup['partner_identity_number'],
            'partner_address' => $data_signup['partner_address'],
            'partner_email' => $data_signup['partner_email'],
            'password' => $data_signup['password'],
            'partner_telephone' => $data_signup['partner_telephone'],
            'partner_phone' => $data_signup['partner_phone'],
            'partner_country_code' => $data_signup['partner_country_code'],
            'partner' => '0',
            'merchant_id' => $inserid,
            'partner_status' => '0'
        );
        $signup = $this->db->insert('partner', $datasignup);

        $databerkas = array(
            'driver_id' => $data_signup['partner_id'],
            'idcard_images' => $data_berkas['idcard_images'],
            'driver_license_images' => "",
            'driver_license_id' => ""
        );
        $insberkas = $this->db->insert('file_driver', $databerkas);

        $datasaldo = array(
            'id_user' => $data_signup['partner_id'],
            'balance' => 0
        );
        $insSaldo = $this->db->insert('balance', $datasaldo);
        return $signup;
    }

    public function savePassword($datapost)
    {
        $partner_telephone = $datapost['phone_number'];
        $password = sha1($datapost['password']);

        $this->db->set('password', $password);
        $this->db->where('partner_telephone', $partner_telephone);
        return $this->db->update('partner');
    }

    public function saveeditmerchant($dataPost)
    {

        $merchant_id = $dataPost['merchant_id'];
        $open_hour = $dataPost['open_hour'];
        $close_hour = $dataPost['close_hour'];

        $dataSave = [
            'open_hour'     => $open_hour,
            'close_hour'    => $close_hour
        ];

        $merchant = $this->db->get_where('merchant', ['merchant_id' => $merchant_id])->row_array();

        if ($_FILES['merchant_logo']['name'] != '') {

            $oldLogo = $merchant['merchant_logo'];

            $upload_logo = $this->_upload('merchant_logo', './images/merchant/');

            if ($upload_logo) {

                if ($oldLogo != '') {
                    if (file_exists('./images/merchant/' . $oldLogo)) {
                        unlink('./images/merchant/' . $oldLogo);
                    }
                }

                $merchant_logo = $upload_logo;
            } else {
                $merchant_logo = $oldLogo;
            }

            $dataSave['merchant_logo'] = $merchant_logo;
        }

        if ($_FILES['merchant_image']['name'] != '') {

            $upload_image = $this->_upload('merchant_image', './images/merchant/');


            $oldImage = $merchant['merchant_image'];

            if ($upload_image) {

                if ($oldImage != '') {
                    if (file_exists('./images/merchant/' . $oldImage)) {
                        unlink('./images/merchant/' . $oldImage);
                    }
                }

                $merchant_image = $upload_image;
            } else {
                $merchant_image = $oldImage;
            }

            $dataSave['merchant_image'] = $merchant_image;
        }



        $save = $this->db->update('merchant', $dataSave, ['merchant_id' => $merchant_id]);

        if ($save) {
            $data = [
                'status'            => true,
                'merchant_logo'     => $dataSave['merchant_logo'],
                'merchant_image'    => $dataSave['merchant_image'],
                'path_image'        => base_url('/images/merchant')
            ];
        } else {
            $data = [
                'status'            => false
            ];
        }

        return $data;
    }

    public function transaksi_home($idmerchant)
    {
        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, customer.phone_number as phone_customer, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo, driver.phone_number as phone_driver');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id', 'right');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id', 'left');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');


        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        // $this->db->where('transaction_history.status != 2');
        $this->db->where('transaction_history.status != 3');
        $this->db->where('transaction_history.status != 4');
        $this->db->where('transaction_history.status != 5');
        // $this->db->where('transaction_history.status_merchant is null');
        // $this->db->where('transaction_history.status_merchant', '1');
        // $this->db->where_not_in('transaction_history.status_merchant', ['2','3']);
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function transaksi_siap_kirim($idmerchant)
    {
        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, customer.phone_number as phone_customer, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo, driver.phone_number as phone_driver');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id', 'left');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id', 'left');
        $this->db->join('customer', 'transaction.customer_id = customer.id', 'left');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');


        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where_in('transaction_history.status', ['3']);
        $this->db->where_in('transaction_history.status_merchant', ['2', '3']);
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }

    public function transaksi_all($idmerchant, $id = null)
    {
        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, customer.phone_number as phone_customer, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo, driver.phone_number as phone_driver');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id', 'right');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id', 'left');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');

        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        if ($id != null) {
            $this->db->where('transaction.id', $id);
        }
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function transaksi_bystatus($idmerchant, $id = null)
    {
        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, customer.phone_number as phone_customer, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo, driver.phone_number as phone_driver');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');

        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', $id);
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function total_pendapatan($idmerchant, $id)
    {
        $pecah = explode('-', $id);
        $month = $pecah[0];
        $year = $pecah[1];

        $this->db->select('sum(transaction.final_cost) as pendapatan');
        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');

        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        $this->db->where("MONTH(transaction.order_time)", $month);
        $this->db->where("YEAR(transaction.order_time)", $year);
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function tanggal_transaksi($idmerchant, $id)
    {
        $pecah = explode('-', $id);
        $month = $pecah[0];
        $year = $pecah[1];

        $this->db->select("DATE(transaction.order_time) as date_trx");
        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');

        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        $this->db->where("MONTH(transaction.order_time)", $month);
        $this->db->where("YEAR(transaction.order_time)", $year);
        $this->db->group_by('date_trx');
        $this->db->order_by('date_trx DESC');
        return $this->db->get();
    }


    public function transaksi_today($idmerchant, $id)
    {
        $pecah = explode('-', $id);

        $tgl = $pecah[0];
        $bln = $pecah[1];
        $thn = $pecah[2];

        $newDate = $thn . '-' . $bln . '-' . $tgl;

        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');

        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        $this->db->where("DATE(transaction.order_time)", $newDate);
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function transaksi_yesterday($idmerchant, $id)
    {
        $pecah = explode('-', $id);

        $tgl = $pecah[0];
        $bln = $pecah[1];
        $thn = $pecah[2];

        $newDate = $thn . '-' . $bln . '-' . $tgl;

        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');

        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        $this->db->where("DATE(transaction.order_time) <", $newDate);
        $this->db->limit('1');
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function transaksi_week($idmerchant, $id)
    {
        $pecah = explode('-', $id);

        $tgl = $pecah[0];
        $bln = $pecah[1];
        $thn = $pecah[2];

        $newDate = $thn . '-' . $bln . '-' . $tgl;

        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');

        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        $this->db->where("DATE(transaction.order_time) <", $newDate);
        $this->db->limit('7');
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function transaksi_bydate($idmerchant, $id = null)
    {

        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');

        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        $this->db->where("DATE(transaction.order_time)", $id);
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function transaksi_bydate1($idmerchant, $id = null)
    {
        $pecah = explode('-', $id);
        $month = $pecah[0];
        $year = $pecah[1];

        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');

        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        $this->db->where("MONTH(transaction.order_time)", $month);
        $this->db->where("YEAR(transaction.order_time)", $year);
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function transaksi_byid($id)
    {
        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity, customer.customer_fullname');
        $this->db->select('transaction_history.status as status_transaksi');
        $this->db->select('driver.driver_name, driver.photo');
        $this->db->select('vehicle.brand, vehicle.type, vehicle.variant, vehicle.vehicle_registration_number, vehicle.color');
        $this->db->select('transaction.*');

        $this->db->from('merchant_detail_transaction');

        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id', 'right');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id');
        $this->db->join('driver', 'transaction.driver_id=driver.id', 'left');
        $this->db->join('vehicle', 'driver.vehicle=vehicle.vehicle_id', 'left');

        $this->db->where('transaction.id', $id);

        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }


    public function item_transaksi($transaction_id)
    {
        $this->db->select('*');
        $this->db->from('item_transaction');
        $this->db->join('item', 'item.item_id = item_transaction.item_id', 'left');
        $this->db->where('transaction_id', $transaction_id);
        return $this->db->get();
    }

    public function transaksi_history($idmerchant, $id = null)
    {
        $this->db->select('merchant_detail_transaction.*, transaction_history.*, transaction.customer_id, customer.customer_fullname, (SELECT SUM(ti.item_amount)
        FROM item_transaction ti
        WHERE ti.transaction_id = merchant_detail_transaction.transaction_id) quantity');
        $this->db->from('merchant_detail_transaction');
        $this->db->join('transaction', 'merchant_detail_transaction.transaction_id = transaction.id');
        $this->db->join('transaction_history', 'transaction.id = transaction_history.transaction_id');
        $this->db->join('customer', 'transaction.customer_id = customer.id');
        $this->db->where('merchant_detail_transaction.merchant_id', $idmerchant);
        $this->db->where('transaction_history.status != 2');
        $this->db->where('transaction_history.status != 1');
        $this->db->where('transaction_history.status != 0');
        if ($id != null) {
            $this->db->where('transaction.id', $id);
        }
        $this->db->order_by('transaction.id DESC');
        return $this->db->get();
    }

    public function transaksi_last_week($day, $idmerchant)
    {
        $this->db->select('SUM(merchant_detail_transaction.total_price) profit, date(merchant_detail_transaction.created) tanggal');
        // $this->db->group_by('date(merchant_detail_transaction.created)');
        $this->db->from('merchant_detail_transaction');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->where('date(merchant_detail_transaction.created)', $day);
        $this->db->where('merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        // $this->db->order_by('date(merchant_detail_transaction.created)', 'DESC');
        // $this->db->limit(7);
        return $this->db->get();
    }

    public function total_history_daily($day, $idmerchant)
    {
        $this->db->select('SUM(merchant_detail_transaction.total_price) daily');
        $this->db->from('merchant_detail_transaction');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->where('date(merchant_detail_transaction.created)', $day);
        $this->db->where('merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        return $this->db->get();
    }

    public function total_history_earning($idmerchant)
    {
        $this->db->select('SUM(merchant_detail_transaction.total_price) earning');
        $this->db->from('merchant_detail_transaction');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->where('merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        return $this->db->get();
    }

    public function total_history_month($month, $idmerchant)
    {
        $this->db->select('SUM(merchant_detail_transaction.total_price) month');
        $this->db->from('merchant_detail_transaction');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->where('MONTH(merchant_detail_transaction.created)', $month);
        $this->db->where('merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        return $this->db->get();
    }

    public function total_history_yearly($year, $idmerchant)
    {
        $this->db->select('SUM(merchant_detail_transaction.total_price) yearly');
        $this->db->from('merchant_detail_transaction');
        $this->db->join('transaction_history', 'merchant_detail_transaction.transaction_id = transaction_history.transaction_id');
        $this->db->where('YEAR(merchant_detail_transaction.created)', $year);
        $this->db->where('merchant_id', $idmerchant);
        $this->db->where('transaction_history.status', '4');
        return $this->db->get();
    }

    public function kategori_active($idmerchant)
    {
        $this->db->select('category_item.*, (SELECT COUNT(ti.item_id)
        FROM item ti
        WHERE ti.item_category = category_item.category_item_id) total_item');
        $this->db->from('category_item');
        $this->db->where('category_item.merchant_id', $idmerchant);
        $this->db->where('category_item.all_category != 1');
        $this->db->where('category_item.category_status = 1');
        return $this->db->get()->result();
    }

    public function kategori_activenew($idmerchant)
    {
        $this->db->select('merchant_category_join.id_category, (SELECT COUNT(ti.item_id)
        FROM item ti
        WHERE ti.item_category = merchant_category_join.id_category) total_item');
        $this->db->select('merchant_category.category_name');
        $this->db->from('merchant_category_join');
        $this->db->where('merchant_category_join.id_merchant', $idmerchant);
        $this->db->where('merchant_category.category_status', '1');
        $this->db->join('merchant_category', 'merchant_category_join.id_category=merchant_category.category_merchant_id');
        return $this->db->get()->result();
    }

    public function kategori_nonactive($idmerchant)
    {
        $this->db->select('category_item.*, (SELECT COUNT(ti.item_id)
        FROM item ti
        WHERE ti.item_category = category_item.category_item_id) total_item');
        $this->db->from('category_item');
        $this->db->where('category_item.merchant_id', $idmerchant);
        $this->db->where('category_item.all_category != 1');
        $this->db->where('category_item.category_status = 0');
        return $this->db->get()->result();
    }

    public function kategori_nonactivenew($idmerchant)
    {
        $this->db->select('merchant_category_join.id_category, (SELECT COUNT(ti.item_id)
        FROM item ti
        WHERE ti.item_category = merchant_category_join.id_category) total_item');
        $this->db->select('merchant_category.category_name');
        $this->db->from('merchant_category_join');
        $this->db->where('merchant_category_join.id_merchant', $idmerchant);
        $this->db->where('merchant_category.category_status', '0');
        $this->db->join('merchant_category', 'merchant_category_join.id_category=merchant_category.category_merchant_id');
        return $this->db->get()->result();
    }

    public function kategori_byid($idmerchant, $id)
    {
        $this->db->select('category_item.*, (SELECT COUNT(ti.item_id)
        FROM item ti
        WHERE ti.item_category = category_item.category_item_id) total_item');
        $this->db->from('category_item');
        $this->db->where('category_item.merchant_id', $idmerchant);
        $this->db->where('category_item.category_item_id', $id);
        return $this->db->get()->row();
    }

    public function kategori_byidnew($idmerchant, $id)
    {
        $this->db->select('merchant_category_join.id_category, (SELECT COUNT(ti.item_id)
        FROM item ti
        WHERE ti.item_category = merchant_category_join.id_category) total_item');
        $this->db->select('merchant_category.category_name');
        $this->db->from('merchant_category_join');
        $this->db->where('merchant_category_join.id_merchant', $idmerchant);
        $this->db->where('merchant_category_join.id_category', $id);
        $this->db->join('merchant_category', 'merchant_category_join.id_category=merchant_category.category_merchant_id');
        return $this->db->get()->row();
    }

    public function savecategory($datapost)
    {
        $merchant_id = $datapost['merchant_id'];
        $category_name_item = $datapost['category_name_item'];
        $all_category = $datapost['all_category'];

        $dataSave = [
            'category_name_item'    => $category_name_item,
            'category_item_images'  => "",
            'merchant_id'           => $merchant_id,
            'all_category'          => $all_category
        ];

        if (array_key_exists('category_item_id', $datapost)) {

            $category_item_id = $datapost['category_item_id'];

            return $this->db->update('category_item', $dataSave, ['category_item_id' => $category_item_id]);
        } else {
            return $this->db->insert('category_item', $dataSave);
        }
    }

    public function itemcuisine($id = null)
    {
        if ($id == null) {
            return $this->db->get_where('item_cuisine', ['status_cuisine' => '1'])->result_array();
        } else {
            return $this->db->get_where('item_cuisine', ['id_cuisine' => $id])->row_array();
        }
    }

    public function itemtag($id = null)
    {
        if ($id == null) {
            return $this->db->get_where('item_tag', ['status_tag' => '1'])->result_array();
        } else {
            return $this->db->get_where('item_tag', ['id_tag' => $id])->row_array();
        }
    }

    public function itemtagjoin($id)
    {
        $this->db->select('item_tag.name_tag');
        $this->db->select('item_tag_join.id_tag, item_tag_join.id_item_tag');
        $this->db->where('id_item', $id);
        $this->db->join('item_tag', 'item_tag_join.id_tag=item_tag.id_tag');

        return $this->db->get('item_tag_join')->result_array();
    }

    public function itembycatactive($idmerchant, $idcat)
    {
        $this->db->select('item.*');
        $this->db->select('item_cuisine.name_cuisine');
        $this->db->select('merchant_etalase.name_etalase');
        $this->db->select('merchant_category.category_name');
        $this->db->from('item');
        $this->db->where('item.merchant_id', $idmerchant);
        $this->db->where('item.item_category', $idcat);
        $this->db->join('merchant_etalase', 'item.etalase_id = merchant_etalase.id_etalase');
        $this->db->join('item_cuisine', 'item.item_cuisine = item_cuisine.id_cuisine', 'left');
        $this->db->join('merchant_category', 'item.item_category = merchant_category.category_merchant_id', 'left');
        $this->db->order_by('item.item_id DESC');
        return $this->db->get();
    }

    public function itemallactive($idmerchant)
    {
        $this->db->select('item.*');
        $this->db->select('item_cuisine.name_cuisine');
        $this->db->select('merchant_etalase.name_etalase');
        $this->db->select('merchant_category.category_name');
        $this->db->from('item');
        $this->db->where('item.merchant_id', $idmerchant);
        $this->db->join('merchant_etalase', 'item.etalase_id = merchant_etalase.id_etalase');
        $this->db->join('item_cuisine', 'item.item_cuisine = item_cuisine.id_cuisine', 'left');
        $this->db->join('merchant_category', 'item.item_category = merchant_category.category_merchant_id', 'left');
        $this->db->order_by('item.item_id DESC');
        return $this->db->get();
    }

    public function itembyid($idmerchant, $id)
    {
        $this->db->select('item.*');
        $this->db->select('item_cuisine.name_cuisine');
        $this->db->select('merchant_etalase.name_etalase');
        $this->db->select('merchant_category.category_name');
        $this->db->from('item');
        $this->db->where('item.merchant_id', $idmerchant);
        $this->db->where('item_id', $id);
        $this->db->join('merchant_etalase', 'item.etalase_id = merchant_etalase.id_etalase');
        $this->db->join('item_cuisine', 'item.item_cuisine = item_cuisine.id_cuisine', 'left');
        $this->db->join('merchant_category', 'item.item_category = merchant_category.category_merchant_id', 'left');
        return $this->db->get();
    }

    public function itempromo($idmerchant, $id = null)
    {
        $this->db->select('item.item_id, item.merchant_id, item.item_name, item.promo_price, item.item_desc, item.item_image, item.etalase_id');
        $this->db->select('merchant_etalase.name_etalase');
        $this->db->from('item');
        $this->db->where('item.merchant_id', $idmerchant);
        $this->db->join('merchant_etalase', 'item.etalase_id = merchant_etalase.id_etalase', 'left');
        if ($id) {
            $this->db->where('item_id', $id);
        }
        $this->db->where('promo_status', 1);
        return $this->db->get();
    }

    public function saveitem($dataPost, $image)
    {

        $merchant_id = $dataPost['merchant_id'];
        $item_name = $dataPost['item_name'];
        $item_price = $dataPost['item_price'];
        $stock = $dataPost['stock'];
        $item_category = $dataPost['item_category'];
        $item_desc = $dataPost['item_desc'];
        $etalase_id = $dataPost['etalase_id'];
        $item_status = $dataPost['item_status'];
        $promo_status = 0;
        $rating = 0;
        $disuka = 0;
        $lev_or_ext = $dataPost['lev_or_ext'];
        $extra = $dataPost['extra'];
        $varian = $dataPost['varian'];
        $level = $dataPost['level'];
        //$jenis = $dataPost['jenis'];
        $item_cuisine = $dataPost['item_cuisine'];
        $item_tag = $dataPost['item_tag'];
        $item_image = $image;


        $index = 0;

        $dataSave = [
            'merchant_id'     => $merchant_id,
            'item_name'    => $item_name,
            'item_price'    => $item_price,
            'promo_price'    => 0,
            'stock'    => $stock,
            'item_category'    => $item_category,
            'item_cuisine'  => $item_cuisine,
            'item_desc'    => $item_desc,
            'item_status'    => $item_status,
            'etalase_id'    => $etalase_id,
            'promo_status'    => $promo_status,
            'rating'    => $rating,
            'disuka'    => $disuka,
            'lev_or_ext'    => $lev_or_ext,
            'extra'    => $extra,
            'varian'    => $varian,
            'level'    => $level,
            'item_image'    => $item_image
            //       'jenis'    => $jenis,
        ];

        if (array_key_exists('item_id', $dataPost)) {
            $saveProduk = $this->db->update('item', $dataSave, ['item_id' => $dataPost['item_id']]);

            $item_id = $dataPost['item_id'];
        } else {
            $saveProduk = $this->db->insert('item', $dataSave);

            $item_id = $this->db->insert_id();
        }

        $pecahTag = explode(';', $item_tag);

        foreach ($pecahTag as $tag) {
            $pecahTag1 = explode(':', $tag);


            $id_tag = $pecahTag1[0];

            $name_tag = strtolower($pecahTag1[1]);

            $cekNameTag = $this->db->get_where('item_tag', ['name_tag' => $name_tag])->num_rows();

            if ($cekNameTag < 1) {

                $dataTag = [
                    'id_tag'        => $id_tag,
                    'name_tag'      => $name_tag,
                    'status_tag'    => '1'
                ];

                $saveTag = $this->db->insert('item_tag', $dataTag);
            }

            $idTag[] = $id_tag;
        }


        if ($saveProduk) {
            foreach ($idTag as $row) {
                $dataSaveTag[] = [
                    'id_item_tag'   => $this->uuid->v4(),
                    'id_item'       => $item_id,
                    'id_tag'        => $row
                ];
            }

            return $this->db->insert_batch('item_tag_join', $dataSaveTag);
        } else {
            return false;
        }
    }

    public function savepromo($dataPost)
    {
        $item_id = $dataPost['item_id'];
        $diskon = $dataPost['diskon'];
        $promo_status = $dataPost['promo_status'];

        $num = 0;

        foreach($item_id as $id){
            $dataItem = $this->db->get_where('item', ['item_id' => $id])->row_array();

            $item_price = $dataItem['item_price'];

            $promo_price= $item_price - $diskon;

            if($promo_status == '1'){
                $this->db->set('promo_price', $promo_price);
            }

            $this->db->set('promo_status', $promo_status);
            $this->db->where('item_id', $id);

            $save = $this->db->update('item');

            if($save){
                $num++;
            }
        }

        if($num > 0){
            return true;
        }else{
            return false;
        }
        
    }

    public function totalitemactive($idmerchant)
    {
        $this->db->select('COUNT(item_id) as active, (SELECT COUNT(ti.item_id)
        FROM item ti
        WHERE ti.merchant_id =' . $idmerchant . ' and ti.item_status = 0) as nonactive, (SELECT COUNT(ti.item_id)
        FROM item ti
        WHERE ti.merchant_id =' . $idmerchant . ' and ti.item_status = 1 and ti.promo_status = 1) as promo');
        $this->db->from('item');
        $this->db->where('item.merchant_id', $idmerchant);
        $this->db->where('item.item_status', '1');
        return $this->db->get();
    }

    public function actived_kategori($id, $status)
    {
        $data = array(
            'category_status' => $status
        );
        $this->db->where('category_item_id', $id);
        $this->db->update('category_item', $data);
        return true;
    }

    public function actived_item($id, $status)
    {
        $data = array(
            'item_status' => $status
        );
        $this->db->where('item_id', $id);
        $this->db->update('item', $data);
        return true;
    }

    public function addkategori($nama, $status, $id)
    {
        $data = array(
            'category_name_item' => $nama,
            'category_status' => $status,
            'merchant_id' => $id,
            'all_category' => 0,
        );
        $this->db->insert('category_item', $data);
        return true;
    }

    public function editkategori($editdata, $id)
    {
        $this->db->where('category_item_id', $id);
        $this->db->update('category_item', $editdata);
        return true;
    }

    public function deletekategori($id)
    {
        $this->db->where('category_item_id', $id);
        $this->db->delete('category_item');

        $this->db->where('item_category', $id);
        $this->db->delete('item');
        return true;
    }

    public function additem($data)
    {
        $this->db->insert('item', $data);
        return true;
    }

    public function edititem($editdata, $id)
    {
        $this->db->where('item_id', $id);
        $this->db->update('item', $editdata);
        return true;
    }

    public function deleteitem($id)
    {
        $this->db->where('item_id', $id);
        $this->db->delete('item');
        return true;
    }

    public function check_exist_phone_edit($id, $phone)
    {
        $cek = $this->db->query("SELECT partner_telephone FROM partner where partner_telephone='$phone' AND partner_id!='$id'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function check_exist_email_edit($id, $email)
    {
        $cek = $this->db->query("SELECT partner_id FROM partner where partner_email='$email' AND partner_id!='$id'");
        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function kategori_merchant_active()
    {
        $this->db->select('category_name,category_merchant_id');
        $this->db->where('category_status != 0');
        return $this->db->get('merchant_category')->result_array();
    }

    public function kategori_merchant_active_data($idfitur)
    {
        $this->db->select('category_name,category_merchant_id');
        $this->db->where('category_status != 0');
        $this->db->where('service_id', $idfitur);
        return $this->db->get('merchant_category')->result_array();
    }

    public function fitur_merchant_active()
    {
        $this->db->select('service_id,service');
        $this->db->from('service');
        $this->db->where('home = 4');
        $this->db->where('active', '1');
        $this->db->order_by('service_id ASC');
        return $this->db->get()->result_array();
    }

    public function getetalase($idmerchant, $id = null)
    {
        $this->db->select('*');
        $this->db->from('merchant_etalase');
        $this->db->where('merchant_etalase.merchant_id', $idmerchant);
        if ($id) {
            $this->db->where('id_etalase', $id);
        }
        return $this->db->get();
    }

    public function saveetalase($dataPost)
    {

        $merchant_id = $dataPost['merchant_id'];
        $name_etalase = $dataPost['name_etalase'];

        $dataSave = [
            'merchant_id'       => $merchant_id,
            'name_etalase'      => $name_etalase,
        ];

        if (array_key_exists('id_etalase', $dataPost)) {
            return $this->db->update('merchant_etalase', $dataSave, ['id_etalase' => $dataPost['id_etalase']]);
        } else {
            $dataSave['id_etalase'] = $this->uuid->v4();
            return $this->db->insert('merchant_etalase', $dataSave);
        }
    }

    public function deleteetalase($id)
    {
        return $this->db->delete('merchant_etalase', ['id_etalase' => $id]);
    }

    public function getvoucher($idmerchant, $by = null, $id = null)
    {
        $this->db->select('voucher.*');
        $this->db->select('service.service_id, service.service');
        $this->db->from('voucher');
        $this->db->where('voucher.merchant_id', $idmerchant);
        $this->db->join('service', 'voucher.voucher_service = service.service_id', 'left');
        if ($by == 'id') {
            $this->db->where('id', $id);
        }
        if ($by == 'isvalid') {
            $this->db->where('is_valid', $id);
        }
        return $this->db->get();
    }

    public function savevoucher($dataPost)
    {

        $merchant_id = $dataPost['merchant_id'];
        $voucher = $dataPost['voucher'];
        $voucher_code = $dataPost['voucher_code'];
        $voucher_type = 1;
        $voucher_service = $dataPost['voucher_service'];
        $voucher_percen = $dataPost['voucher_percen'];
        $min_purch = $dataPost['min_purch'];
        $max_disc = $dataPost['max_disc'];
        $valid_from = $dataPost['valid_from'];
        $expired = $dataPost['expired'];
        $voucher_discount = $dataPost['voucher_discount'];
        $description = $dataPost['description'];
        $count_to_use = 0;
        $is_valid = 'yes';

        $dataSave = [
            'merchant_id'       => $merchant_id,
            'voucher'           => $voucher,
            'voucher_code'      => $voucher_code,
            'voucher_type'      => $voucher_type,
            'voucher_service'   => $voucher_service,
            'voucher_percen'    => $voucher_percen,
            'min_purch'         => $min_purch,
            'max_disc'          => $max_disc,
            'valid_from'        => $valid_from,
            'expired'           => $expired,
            'voucher_discount'  => $voucher_discount,
            'description'       => $description,
            'count_to_use'      => $count_to_use,
            'is_valid'          => $is_valid,
        ];

        if (array_key_exists('id', $dataPost)) {
            return $this->db->update('voucher', $dataSave, ['id' => $dataPost['id']]);
        } else {
            return $this->db->insert('voucher', $dataSave);
        }
    }

    public function deletevoucher($id)
    {
        $this->db->where_in('id', $id);
        return $this->db->delete('voucher');
    }


    public function getDriverFree()
    {
        $this->db->select('driver.id, driver.driver_name, driver.phone_number');
        $this->db->select('vehicle.*');
        $this->db->select('config_driver.*');


        $this->db->where('driver.status', '1');
        $this->db->where('config_driver.status', '1');
        $this->db->where('driver.job', '7');

        $this->db->join('vehicle', 'vehicle.vehicle_id=driver.vehicle');
        $this->db->join('config_driver', 'config_driver.driver_id=driver.id');

        return $this->db->get('driver')->result_array();
    }


    public function saveAcceptTrx($dataPost)
    {
        $transaction_id = $dataPost['transaction_id'];
        $status = '1';
        $status_merchant = '1';

        $dataSaveStatusTrx = [
            'status'            => $status,
            'status_merchant'   => $status_merchant
        ];

        return $this->db->update('transaction_history', $dataSaveStatusTrx, ['transaction_id' => $transaction_id]);
    }


    public function saveStatusTrx($dataPost)
    {
        $transaction_id = $dataPost['transaction_id'];
        $status_merchant = $dataPost['status_trx_merchant'];

        $dataSaveStatusTrx = [
            'status_merchant'   => $status_merchant
        ];

        return $this->db->update('transaction_history', $dataSaveStatusTrx, ['transaction_id' => $transaction_id]);
    }


    public function findDriver($dataPost)
    {
        $transaction_id = $dataPost['transaction_id'];
        $merchant_id = $dataPost['merchant_id'];
        $status = '2';

        $dataDriverFree = $this->getDriverFree();

        $dataMerchant = $this->db->get_where('merchant', ['merchant_id' => $merchant_id])->row_array();

        $merchant_latitude = $dataMerchant['merchant_latitude'];
        $merchant_longitude = $dataMerchant['merchant_longitude'];

        if (!empty($dataDriverFree)) {

            foreach ($dataDriverFree as $row) {
                $driver_latitude = $row['latitude'];
                $driver_longitude = $row['longitude'];



                $distance = getDistanceBetweenPoints($merchant_latitude, $merchant_longitude, $driver_latitude, $driver_longitude);


                $newDriverFree[] = [
                    'driver_id'     => $row['driver_id'],
                    'distance'      => $distance,
                    'driver_name'   => $row['driver_name'],
                    'brand'         => $row['brand'],
                    'type'          => $row['type'],
                    'vehicle_registration_number' => $row['vehicle_registration_number'],
                    'color'         => $row['color'],
                    'variant'       => $row['variant']
                ];
            }


            $sortDriverFree = array_orderby($newDriverFree, 'distance', SORT_ASC);


            $index = 0;

            $firtsDriver = $sortDriverFree[$index]['driver_id'];

            $oldDriver = $this->db->get_where('transaction', ['id' => $transaction_id])->row_array()['driver_id'];

            if ($firtsDriver == $oldDriver) {
                $index++;

                $driver_id = $sortDriverFree[$index]['driver_id'];
            } else {
                $driver_id = $firtsDriver;
            }


            $dataSaveTransaction = [
                'driver_id'  => $driver_id
            ];

            $updateTransaction = $this->db->update('transaction', $dataSaveTransaction, ['id' => $transaction_id]);

            if ($updateTransaction) {
                $dataSaveStatusTrx = [
                    'driver_id' => $driver_id,
                    'status'    => $status
                ];

                $this->db->update('config_driver', ['status' => 2], ['driver_id' => $driver_id]);

                return $this->db->update('transaction_history', $dataSaveStatusTrx, ['transaction_id' => $transaction_id]);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public function getCategoryMerchant()
    {
        return $this->db->get_where('merchant_category', ['category_status' => '1'])->result_array();
    }









    private function _upload($name, $path)
    {
        $config['upload_path']          = $path;
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['file_name']            = time() . '-' . rand(0, 9999);
        $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($name)) {
            $result = [$name => $this->upload->data()];

            $Mfile = model2model('File_model');

            $Mfile->file->compress($path . $result[$name]['file_name'], 300, 300);

            return $result[$name]['file_name'];
        } else {
            return false;
        }
    }
}
