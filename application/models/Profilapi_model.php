<?php


class Profilapi_model extends CI_model
{

    public function getrelationship($id)
    {
        $this->db->select('*');

        if ($id != '') {
            $this->db->where('id', $id);
        }

        return $this->db->get('family_relationship')->result_array();
    }


    public function getHealth($driver_id)
    {
        $this->db->select('driver.driver_name');
        $this->db->select('driver_health.*');

        $this->db->join('driver_health', 'driver_health.driver_id=driver.id', 'left');

        $this->db->where('driver.id', $driver_id);

        return $this->db->get('driver')->row_array();
    }


    public function healthverification($dataPost)
    {
        $driver_id = $dataPost['driver_id'];

        $cek = $this->db->get_where('driver_health', ['driver_id' => $driver_id])->row_array();

        if ($cek) {
            return $this->db->update('driver_health', $dataPost, ['driver_id' => $driver_id]);
        } else {
            return $this->db->insert('driver_health', $dataPost);
        }
    }


    public function savePasswordDriver($datapost)
    {
        $phone_number = $datapost['phone_number'];
        $password = sha1($datapost['password']);

        $this->db->set('password', $password);
        $this->db->where('phone_number', $phone_number);
        return $this->db->update('driver');
    }


    public function saveStatusDriver($datapost)
    {
        $driver_id = $datapost['driver_id'];
        $status = $datapost['status'];

        $this->db->set('status', $status);
        $this->db->where('driver_id', $driver_id);
        $save = $this->db->update('config_driver');

        if ($save) {
            $data = $this->db->get_where('config_driver', ['driver_id' => $driver_id])->row_array();

            return $data;
        } else {
            return false;
        }
    }


    public function savePasswordCustomer($datapost)
    {
        $phone_number = $datapost['phone_number'];
        $password = sha1($datapost['password']);

        $this->db->set('password', $password);
        $this->db->where('phone_number', $phone_number);
        return $this->db->update('customer');
    }


    public function saveProfilCustomer($datapost)
    {
        $phone_number = $datapost['phone_number'];
        $customer_fullname = $datapost['fullname'];
        $dob = $datapost['dob'];
        $jk = $datapost['jk'];

        $this->db->set('customer_fullname', $customer_fullname);
        $this->db->set('dob', $dob);
        $this->db->set('jk', $jk);
        $this->db->where('phone_number', $phone_number);
        return $this->db->update('customer');
    }


    public function saveEmailCustomer($datapost)
    {
        $phone_number = $datapost['phone_number'];
        $email = $datapost['email'];

        $cekEmail = $this->db->get_where('customer', ['email' => $email])->row_array();

        if ($cekEmail) {
            return false;
        } else {

            $this->db->set('email', $email);
            $this->db->set('is_verify_email', 'N');
            $this->db->where('phone_number', $phone_number);
            return $this->db->update('customer');
        }
    }


    public function saveEmailDriver($datapost)
    {
        $phone_number = $datapost['phone_number'];
        $email = $datapost['email'];

        $cekEmail = $this->db->get_where('driver', ['email' => $email])->row_array();

        if ($cekEmail) {
            return false;
        } else {

            $this->db->set('email', $email);
            $this->db->set('is_verify', 'no');
            $this->db->where('phone_number', $phone_number);
            return $this->db->update('driver');
        }
    }


    public function savePhoneCustomer($datapost)
    {
        $phone_number = str_replace("+", "", $datapost['phone_number']);
        $phone = substr($datapost['phone_number'], 3);
        $id = $datapost['id'];

        $cekPhone = $this->db->get_where('customer', ['phone_number' => $phone_number])->row_array();

        if ($cekPhone) {
            return false;
        } else {

            $this->db->set('phone_number', $phone_number);
            $this->db->set('phone', $phone);
            $this->db->where('id', $id);
            return $this->db->update('customer');
        }
    }

    public function savePhoneDriver($datapost)
    {
        $phone_number = str_replace("+", "", $datapost['phone_number']);
        $phone = substr($datapost['phone_number'], 3);
        $id = $datapost['id'];

        $cekPhone = $this->db->get_where('driver', ['phone_number' => $phone_number])->row_array();

        if ($cekPhone) {
            return false;
        } else {

            $this->db->set('phone_number', $phone_number);
            $this->db->set('phone', $phone);
            $this->db->where('id', $id);
            return $this->db->update('driver');
        }
    }
}
