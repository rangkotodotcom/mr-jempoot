<?php


class Transactionapi_model extends CI_model
{

    public function getAllservice()
    {
        $this->db->select('*');
        $this->db->order_by('service_id', 'ASC');
        return  $this->db->get('service')->result_array();
    }

    public function getTransaksiById($kode)
    {


        $urlDriver = base_url('images/driverphoto');
        $urlMerchant = base_url('images/merchant');
        $urlCustomer = base_url('images/customer');

        $this->db->select('merchant.*');
        $this->db->select('merchant_detail_transaction.total_price as total_belanja');
        $this->db->select('detail_send_transaction.send_id, detail_send_transaction.goods_item, detail_send_transaction.receiver_name, detail_send_transaction.sender_name, detail_send_transaction.sender_phone, detail_send_transaction.receiver_phone');
        $this->db->select('transaction_history.*');
        $this->db->select('transaction_status.*');
        $this->db->select('transaction_langganan.id_langganan, transaction_langganan.start_date, transaction_langganan.end_date, transaction_langganan.duration');
        $this->db->select('transaction_history.keterangan as keterangan_history_trx');
        // $this->db->select('voucher.*');
        $this->db->select('service.*');
        $this->db->select('driver_rating.number as number_rating_driver');
        $this->db->select('driver_rating.rating as rate_driver');
        $this->db->select('driver_rating.note as note_rating_driver');
        $this->db->select('customer.customer_fullname,customer.email as email_pelanggan,customer.phone_number as telepon_pelanggan,customer.customer_image,customer.token');
        $this->db->select('driver.driver_name,driver.photo,driver.email,driver.phone_number,driver.reg_id');
        $this->db->select('transaction.*');

        $this->db->join('merchant_detail_transaction', 'transaction.id = merchant_detail_transaction.transaction_id', 'left');
        $this->db->join('merchant', 'merchant_detail_transaction.merchant_id = merchant.merchant_id', 'left');
        $this->db->join('detail_send_transaction', 'transaction.id = detail_send_transaction.transaction_id', 'left');
        $this->db->join('transaction_history', 'transaction.id = transaction_history.transaction_id', 'left');
        $this->db->join('transaction_status', 'transaction_history.status = transaction_status.id', 'left');
        $this->db->join('transaction_langganan', 'transaction.id = transaction_langganan.transaction_id', 'left');
        // $this->db->join('voucher', 'transaction.service_order = voucher.voucher_service', 'left');
        $this->db->join('service', 'transaction.service_order = service.service_id', 'left');
        $this->db->join('driver_rating', 'transaction.id = driver_rating.transaction_id', 'left');
        $this->db->join('driver', 'transaction.driver_id = driver.id', 'left');
        $this->db->join('customer', 'transaction.customer_id = customer.id', 'left');
        $this->db->order_by('transaction.id', 'DESC');


        $data = $this->db->get_where('transaction', ['transaction.id' => $kode])->result_array();

        $newData = [];

        if (!empty($data)) {

            foreach ($data as $row) {
                $keys = array_keys($row);

                $item_transaction = $this->getitembyid($row['id']);
                $receive_packet = $this->getReceivepacket($row['id']);

                foreach ($keys as $key => $value) {
                    $newData[$value] = $row[$value];
                }

                $newData['item_transaction'] = $item_transaction;
                $newData['receive_packet'] = $receive_packet;
                $newData['url_driver'] = $urlDriver;
                $newData['url_merchant'] = $urlMerchant;
                $newData['url_customer'] = $urlCustomer;

                $newReturn[] = $newData;
            }
        } else {
            $newReturn = $newData;
        }

        return $newReturn;
    }

    public function getTrxDriver($driver_id, $service_id, $by, $kode)
    {


        $urlDriver = base_url('images/driverphoto');
        $urlMerchant = base_url('images/merchant');

        $this->db->select('merchant.*');
        $this->db->select('merchant_detail_transaction.total_price as total_belanja');
        $this->db->select('detail_send_transaction.send_id, detail_send_transaction.goods_item, detail_send_transaction.receiver_name, detail_send_transaction.sender_name, detail_send_transaction.sender_phone, detail_send_transaction.receiver_phone');
        $this->db->select('transaction_history.*');
        $this->db->select('transaction_status.*');
        $this->db->select('transaction_langganan.id_langganan, transaction_langganan.start_date, transaction_langganan.end_date, transaction_langganan.duration');
        $this->db->select('transaction_history.keterangan as keterangan_history_trx');
        // $this->db->select('voucher.*');
        $this->db->select('service.*');
        $this->db->select('tb_val_cus_trans.grade_value as rate_cus_trans');
        $this->db->select('driver_rating.number as number_rating_driver');
        $this->db->select('driver_rating.rating as rate_driver');
        $this->db->select('driver_rating.note as note_rating_driver');
        $this->db->select('customer.customer_fullname,customer.email as email_pelanggan,customer.phone_number as telepon_pelanggan,customer.customer_image,customer.token');
        $this->db->select('driver.driver_name,driver.photo,driver.email,driver.phone_number,driver.reg_id');
        $this->db->select('transaction.*');

        $this->db->join('merchant_detail_transaction', 'transaction.id = merchant_detail_transaction.transaction_id', 'left');
        $this->db->join('tb_val_cus_trans', 'transaction.id = tb_val_cus_trans.id_transaction', 'left');
        $this->db->join('merchant', 'merchant_detail_transaction.merchant_id = merchant.merchant_id', 'left');
        $this->db->join('detail_send_transaction', 'transaction.id = detail_send_transaction.transaction_id', 'left');
        $this->db->join('transaction_history', 'transaction.id = transaction_history.transaction_id', 'left');
        $this->db->join('transaction_status', 'transaction_history.status = transaction_status.id', 'left');
        $this->db->join('transaction_langganan', 'transaction.id = transaction_langganan.transaction_id', 'left');
        // $this->db->join('voucher', 'transaction.service_order = voucher.voucher_service', 'left');
        $this->db->join('service', 'transaction.service_order = service.service_id', 'left');
        $this->db->join('driver_rating', 'transaction.id = driver_rating.transaction_id', 'left');
        $this->db->join('driver', 'transaction.driver_id = driver.id', 'left');
        $this->db->join('customer', 'transaction.customer_id = customer.id', 'left');
        $this->db->order_by('transaction.id', 'DESC');

        if ($by == 'status') {

            $this->db->where('transaction_history.status', $kode);
        } else if ($by == 'id') {
            $this->db->where('transaction.id', $kode);
        }

        if ($service_id != null) {
            $this->db->where('transaction.service_order', $service_id);
        }

        $data = $this->db->get_where('transaction', ['transaction.driver_id' => $driver_id])->result_array();

        $newData = [];

        if (!empty($data)) {

            foreach ($data as $row) {
                $keys = array_keys($row);

                $item_transaction = $this->getitembyid($row['id']);
                $receive_packet = $this->getReceivepacket($row['id']);

                foreach ($keys as $key => $value) {
                    $newData[$value] = $row[$value];
                }

                $newData['item_transaction'] = $item_transaction;
                $newData['receive_packet'] = $receive_packet;
                $newData['url_driver'] = $urlDriver;
                $newData['url_merchant'] = $urlMerchant;

                $newReturn[] = $newData;
            }
        } else {
            $newReturn = $newData;
        }

        return $newReturn;
    }


    public function getTrxCustomer($customer_id, $service_id, $by, $kode)
    {
        $urlDriver = base_url('images/driverphoto');
        $urlMerchant = base_url('images/merchant');


        $this->db->select('merchant.*');
        $this->db->select('merchant_detail_transaction.total_price as total_belanja');
        $this->db->select('detail_send_transaction.send_id,detail_send_transaction.goods_item,detail_send_transaction.sender_name,detail_send_transaction.receiver_name,detail_send_transaction.sender_phone,detail_send_transaction.receiver_phone');
        $this->db->select('transaction_history.date,transaction_history.status,transaction_history.note,transaction_history.status_merchant');
        $this->db->select('transaction_status.transaction_status');
        $this->db->select('transaction_langganan.id_langganan,transaction_langganan.start_date,transaction_langganan.end_date, transaction_langganan.duration');
        $this->db->select('transaction_history.keterangan as keterangan_history_trx');
        $this->db->select('transaction.*');
        // $this->db->select('voucher.*');
        $this->db->select('service.*');
        $this->db->select('tb_val_cus_trans.*');
        $this->db->select('customer.id as customer_id,customer.customer_fullname,customer.email as email_pelanggan,customer.phone_number as telepon_pelanggan,customer.customer_image,customer.token');
        $this->db->select('driver.id as driver_id,driver.driver_name,driver.photo as driver_photo,driver.email,driver.phone_number,driver.reg_id');

        $this->db->join('merchant_detail_transaction', 'transaction.id = merchant_detail_transaction.transaction_id', 'left');
        $this->db->join('merchant', 'merchant_detail_transaction.merchant_id = merchant.merchant_id', 'left');
        $this->db->join('detail_send_transaction', 'transaction.id = detail_send_transaction.transaction_id', 'left');
        $this->db->join('transaction_history', 'transaction.id = transaction_history.transaction_id', 'left');
        $this->db->join('transaction_status', 'transaction_history.status = transaction_status.id', 'left');
        $this->db->join('transaction_langganan', 'transaction.id = transaction_langganan.transaction_id', 'left');
        // $this->db->join('voucher', 'transaction.service_order = voucher.voucher_service', 'left');
        $this->db->join('service', 'transaction.service_order = service.service_id', 'left');
        $this->db->join('customer', 'transaction.customer_id = customer.id', 'left');
        $this->db->join('tb_val_cus_trans', 'transaction.id = tb_val_cus_trans.id_transaction', 'left');
        $this->db->join('driver', 'transaction.driver_id = driver.id', 'left');
        $this->db->order_by('transaction.id', 'DESC');

        $this->db->where('transaction.customer_id', $customer_id);
        $this->db->where('transaction.service_order', $service_id);

        if ($by == 'status') {
            $this->db->where('transaction_history.status', $kode);
        }

        if ($by == 'id') {
            $this->db->where('transaction.id', $kode);
        }



        $data = $this->db->get('transaction')->result_array();

        $newData = [];

        if (!empty($data)) {

            foreach ($data as $row) {
                $keys = array_keys($row);

                $item_transaction = $this->getitembyid($row['id']);
                $receive_packet = $this->getReceivepacket($row['id']);

                foreach ($keys as $key => $value) {
                    $newData[$value] = $row[$value];
                }

                $newData['item_transaction'] = $item_transaction;
                $newData['receive_packet'] = $receive_packet;
                $newData['url_driver'] = $urlDriver;
                $newData['url_merchant'] = $urlMerchant;

                $newReturn[] = $newData;
            }
        } else {
            $newReturn = $newData;
        }

        return $newReturn;
    }


    public function getitembyid($id)
    {
        $this->db->select('*');
        $this->db->where("item_transaction.transaction_id = $id");
        $this->db->join('item', 'item_transaction.item_id = item.item_id');
        $dataItemTrx = $this->db->get('item_transaction')->result_array();

        $data = [];
        foreach ($dataItemTrx as $g) {
            $extra = $g['extra'];
            $level = $g['level'];
            $varian = $g['varian'];


            if (!empty($extra) && $extra != '') {
                $pecahE = explode(';', $extra);

                foreach ($pecahE as $e) {

                    if ($e != '') {

                        $pecahE1 = explode(':', $e);

                        $dataExtra = [
                            'nama'  => $pecahE1[0],
                            'harga'  => $pecahE1[1],
                        ];

                        $newExtra[] = $dataExtra;
                    }
                }
            } else {
                $newExtra = null;
            }

            if (!empty($level) && $level != '') {
                $pecahL = explode(';', $level);

                foreach ($pecahL as $e) {

                    if ($e != '') {



                        $pecahL1 = explode(':', $e);

                        $dataLevel = [
                            'nama'  => $pecahL1[0],
                            'harga'  => $pecahL1[1],
                        ];

                        $newLevel[] = $dataLevel;
                    }
                }
            } else {
                $newLevel = null;
            }

            if (!empty($varian) && $varian != '') {
                $pecahV = explode(';', $varian);

                foreach ($pecahV as $e) {

                    if ($e != '') {

                        $pecahV1 = explode(':', $e);

                        $dataVarian = [
                            'nama'  => $pecahV1[0],
                            'harga'  => $pecahV1[1],
                        ];

                        $newVarian[] = $dataVarian;
                    }
                }
            } else {
                $newVarian = null;
            }

            $g['extra'] = $newExtra;
            $g['level'] = $newLevel;
            $g['varian'] = $newVarian;
            $data[] = $g;
        }

        return $data;
    }

    public function savestatustrx($datapost)
    {
        $transaction_id = $datapost['transaction_id'];
        $driver_id = $datapost['driver_id'];
        $status = $datapost['status'];
        $keterangan = $datapost['keterangan'];

        if ($status == '3') {
            $status_config = 3;
        } else {
            $status_config = 1;
        }

        $datasave = [
            'transaction_id'    => $transaction_id,
            'driver_id'         => $driver_id,
            'status'            => $status,
            'keterangan'        => $keterangan
        ];


        $this->db->set('status', $status_config);
        $this->db->where('driver_id', $driver_id);
        $this->db->update('config_driver');


        return $this->db->update('transaction_history', $datasave, ['transaction_id' => $transaction_id]);
    }


    public function getnotif($id_user)
    {
        $this->db->select('*');
        $this->db->order_by('date_notif', 'DESC');
        $this->db->where('id_user', $id_user);
        return $this->db->get('tb_notif')->result_array();
    }


    public function savenotif($datapost)
    {
        $id_notif = $this->uuid->v4();
        $id_user = $datapost['id_user'];
        $title_notif = $datapost['title_notif'];
        $text_notif = $datapost['text_notif'];

        $dataSave = [
            'id_notif'      => $id_notif,
            'id_user'       => $id_user,
            'title_notif'   => $title_notif,
            'text_notif'    => $text_notif
        ];

        return $this->db->insert('tb_notif', $dataSave);
    }


    public function clearallnotif($id_user)
    {
        return $this->db->delete('tb_notif', ['id_user' => $id_user]);
    }


    public function savevalcus($datapost)
    {
        $id_val_cus_trans = $this->uuid->v4();
        $id_transaction = $datapost['transaction_id'];
        $id_driver = $datapost['driver_id'];
        $id_customer = $datapost['customer_id'];
        $grade_value = $datapost['grade_value'];

        $dataSave = [
            'id_val_cus_trans'  => $id_val_cus_trans,
            'id_transaction'    => $id_transaction,
            'id_driver'         => $id_driver,
            'id_customer'       => $id_customer,
            'grade_value'       => $grade_value
        ];

        if ($dataSave['grade_value'] > 0) {
            $this->db->insert('tb_val_cus_trans', $dataSave);
            if ($this->db->affected_rows() == 1) {


                $get_rating = $this->count_rate_customer($dataSave['id_customer']);
                $this->db->where('id', $dataSave['id_customer']);
                $this->db->update('customer', array('customer_rating' => $get_rating));

                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function count_rate_customer($id)
    {
        $this->db->select('grade_value');
        $this->db->from('tb_val_cus_trans');
        $this->db->where('id_customer', $id);
        $cek = $this->db->get();
        $rate = 0;
        $hasil = 0;
        if ($cek->num_rows() > 0) {
            foreach ($cek->result() as $row) {
                $rate += $row->grade_value;
            }
            $hasil = $rate / $cek->num_rows();
        }
        return $hasil;
    }


    public function getPerformanceDriver($driver_id, $type, $rate = null)
    {

        $this->db->where('driver_id', $driver_id);

        if ($type == 'detail') {

            $this->db->select('driver_rating.*');
            $this->db->select('customer.customer_fullname');
            $this->db->join('customer', 'customer.id=driver_rating.customer_id', 'left');
            return $this->db->get('driver_rating')->result_array();
        } else {

            if ($rate == null) {
                $this->db->select_avg('rating');
                return $this->db->get('driver_rating')->row_array();
            } else {
                $this->db->where('rating', $rate);
                return $this->db->count_all_results('driver_rating');
            }
        }
    }

    public function vercodmer($dataPost)
    {
        $transaction_id = $dataPost['transaction_id'];
        $code = $dataPost['code'];

        $trx = $this->db->get_where('merchant_detail_transaction', ['transaction_id' => $transaction_id])->row_array();

        if ($trx) {
            $validation_code = $trx['validation_code'];

            if ($validation_code == $code) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public function saveStruk($dataPost)
    {
        return $this->db->insert('transaction_struk', $dataPost);
    }

    public function getReceivepacket($transaction_id)
    {
        return $this->db->get_where('transaction_receive', ['transaction_id' => $transaction_id])->row_array();
    }


    public function saveReceivePacket($dataPost)
    {
        $cek = $this->getReceivepacket($dataPost['transaction_id']);

        if ($cek) {
            $this->db->set('image_after', $dataPost['image_after']);
            $this->db->where('transaction_id', $dataPost['transaction_id']);
            return $this->db->update('transaction_receive');
        } else {
            return $this->db->insert('transaction_receive', $dataPost);
        }
    }


    public function PayMerchant($cond, $condtr, $final_price)
    {
        $appsetting = model2model('Appsettings_model');

        $app_cost = $appsetting->getappbyid()['app_cost'];

        $last_trans = $this->get_data_last_transaksi($condtr);

        $get_mitra = $this->get_trans_merchant($last_trans->row('transaction_id'));
        $datapelanggan = $this->get_data_pelangganid($last_trans->row('customer_id'));
        $datadriver = $this->get_data_driverid($cond['driver_id']);


        if ($final_price == $get_mitra->row('total_price')) {

            $data_cut_mitra = array(
                'partner_id' => $get_mitra->row('partner_id'),
                'price' => $get_mitra->row('total_price'),
                'final_cost' => $last_trans->row('final_cost'),
                'service' => $last_trans->row('service'),
                'service_order' => $last_trans->row('service_order'),
                'wallet_payment' => $last_trans->row('wallet_payment')
            );

            // $this->delete_chat($get_mitra->row('merchant_id'), $last_trans->row('customer_id'));
            // $this->delete_chat($get_mitra->row('merchant_id'), $cond['driver_id']);

            $cutMerchant = $this->cut_mitra_saldo_by_order($data_cut_mitra);

            if ($cutMerchant['status']) {

                return array(
                    'status' => true,
                    'data' => $last_trans->result(),
                    'idp' => $last_trans->row('customer_id'),
                );
            } else {
                return array(
                    'status' => false,
                    'data' => 'false in cutting'
                );
            }
        } else {
            return [
                'status'    => false,
                'message'   => 'price different'
            ];
        }
    }


    public function PayJempoot($cond, $condtr, $price_to_jempoot)
    {
        $appsetting = model2model('Appsettings_model');

        $app_cost = $appsetting->getappbyid()['app_cost'];

        if ($app_cost == $price_to_jempoot) {



            $this->db->where($condtr);
            $this->db->update('transaction', array('finish_time' => date('Y-m-d H:i:s')));


            if ($this->db->affected_rows() > 0) {
                $last_trans = $this->get_data_last_transaksi($condtr);

                $get_mitra = $this->get_trans_merchant($last_trans->row('transaction_id'));
                $datapelanggan = $this->get_data_pelangganid($last_trans->row('customer_id'));
                $datadriver = $this->get_data_driverid($cond['driver_id']);

                $data_cut = array(
                    'driver_id' => $cond['driver_id'],
                    'price' => $last_trans->row('final_cost') - ($get_mitra->row('total_price') + $app_cost),
                    'final_cost' => $last_trans->row('final_cost'),
                    'promo_discount' => $last_trans->row('promo_discount'),
                    'transaction_id' => $last_trans->row('transaction_id'),
                    'service' => $last_trans->row('service'),
                    'service_order' => $last_trans->row('service_order'),
                    'driver_name' => $datadriver->row('driver_name'),
                    'wallet_payment' => $last_trans->row('wallet_payment'),
                    'app_cost'      => $app_cost
                );

                // $deletechat = $this->delete_chat($cond['driver_id'], $last_trans->row('customer_id'));
                // if ($deletechat) {

                $cutting = $this->cut_driver_saldo_by_order($data_cut, '');


                if ($cutting['status']) {

                    $data = array(
                        'status' => '4'
                    );
                    $this->db->where($cond);
                    $this->db->update('transaction_history', $data);
                    $datD = array(
                        'status' => '1'
                    );
                    $this->db->where(array('driver_id' => $cond['driver_id']));
                    $this->db->update('config_driver', $datD);
                    return array(
                        'status' => true,
                        'data' => $last_trans->result(),
                        'idp' => $last_trans->row('customer_id'),
                    );
                } else {
                    return array(
                        'status' => false,
                        'data' => 'false in cutting'
                    );
                }

                // }
            } else {
                return array(
                    'status' => false,
                    'data' => 'abc'
                );
            }
        } else {
            return array(
                'status' => false,
                'message' => 'price different'
            );
        }
    }

    public function get_data_pelangganid($id)
    {
        $this->db->select('customer.*, balance.balance');
        $this->db->from('customer');
        $this->db->join('balance', 'customer.id = balance.id_user');
        $this->db->where('id', $id);
        return $this->db->get();
    }

    public function get_data_driverid($id)
    {
        $this->db->select('driver.*, balance.balance');
        $this->db->from('driver');
        $this->db->join('balance', 'driver.id = balance.id_user');
        $this->db->where('id', $id);
        return $this->db->get();
    }

    function cut_user_saldo_by_order($dataC)
    {

        $this->db->where('id_user', $dataC['customer_id']);
        $balance = $this->db->get('balance')->row('balance');

        if ($dataC['wallet_payment'] == 1) {
            $data_ins = array(
                'id'                => $this->uuid->v4(),
                'id_user'           => $dataC['customer_id'],
                'wallet_amount'     => $dataC['final_cost'],
                'type'              => 'Payout',
                'keterangan'        => $dataC['service'],
                'status'            => '2'
            );

            $ins_trans = $this->db->insert('tb_new_wallet', $data_ins);
            if ($ins_trans) {
                $this->db->where('id_user', $dataC['customer_id']);
                return $this->db->update('balance', array('balance' => ($balance - $data_ins['wallet_amount'])));
            } else {
                return false;
            }
        } else {
            return true;
        }
    }


    function cut_driver_saldo_by_order($data)
    {
        $this->db->select('commision');
        $this->db->where('service_id', $data['service_order']);
        $persen = $this->db->get('service')->row('commision');

        $this->db->where('id_user', $data['driver_id']);
        $balance = $this->db->get('balance')->row('balance');

        if ($data['wallet_payment'] == 1) {


            $kred = $data['price'];
            $hasil = $kred;

            $data_ins = array(
                'id'            => $this->uuid->v4(),
                'id_user'       => $data['driver_id'],
                'wallet_amount' => $hasil,
                'type'          => 'Order+',
                'keterangan'    => $data["service"],
                'status'        => 2
            );

            $newBalance = $balance + $hasil;

            $ins_trans = $this->db->insert('tb_new_wallet', $data_ins);


            if ($ins_trans) {
                $this->db->where('id_user', $data['driver_id']);
                $upd = $this->db->update('balance', array('balance' => $newBalance));

                if ($this->db->affected_rows() > 0) {
                    return array(
                        'status' => true,
                        'data' => array('balance' => ($newBalance))
                    );
                } else {
                    return array(
                        'status' => false,
                        'data' => 'fail in update'
                    );
                }
            } else {
                return array(
                    'status' => false,
                    'data' => []
                );
            }
        } else {
            $hasil = $data['app_cost'];

            $data_ins = array(
                'id'            => $this->uuid->v4(),
                'id_user'       => $data['driver_id'],
                'wallet_amount' => $hasil,
                'type'          => 'Order-',
                'keterangan'    => 'Pay To Jempoot (' . $data["service"] . ')',
                'status'        => 2
            );

            $data_ins_promo = array(
                'id'            => $this->uuid->v4(),
                'id_user'       => $data['driver_id'],
                'wallet_amount' => $data['promo_discount'],
                'type'          => 'Order+',
                'keterangan'    => 'Diskon Transaksi (' . $data["service"] . ')',
                'status'        => 2
            );

            $ins_trans = $this->db->insert('tb_new_wallet', $data_ins);
            if ($ins_trans) {
                $this->db->where('id_user', $data['driver_id']);
                $upd = $this->db->update('balance', array('balance' => ($balance - $hasil)));

                if ($upd) {
                    if ($data['promo_discount'] > 0) {
                        $this->db->where('id_user', $data['driver_id']);
                        $balanceupd = $this->db->get('balance')->row('balance');

                        $this->db->insert('tb_new_wallet', $data_ins_promo);
                        $this->db->where('id_user', $data['driver_id']);
                        $this->db->update('balance', array('balance' => ($balanceupd + $data['promo_discount'])));
                    }
                    return array(
                        'status' => true,
                        'data' => []
                    );
                } else {
                    return array(
                        'status' => false,
                        'data' => 'fail in update'
                    );
                }
            } else {
                return array(
                    'status' => false,
                    'data' => []
                );
            }
        }
    }

    function cut_mitra_saldo_by_order($data)
    {
        $this->db->select('commision');
        $this->db->where('service_id', $data['service_order']);
        $persen = $this->db->get('service')->row('commision');

        $this->db->where('id_user', $data['partner_id']);
        $balance = $this->db->get('balance')->row('balance');
        if ($data['wallet_payment'] == 1) {
            $kred = $data['price'];
            $hasil = $kred;

            $data_ins = array(
                'id'            => $this->uuid->v4(),
                'wallet_amount' => $hasil,
                'type'          => 'Order+',
                'keterangan'    => $data['service'],
                'status'        => 2,
                'id_user'       => $data['partner_id'],
            );
            $ins_trans = $this->db->insert('tb_new_wallet', $data_ins);

            if ($ins_trans) {
                $this->db->where('id_user', $data['partner_id']);
                $upd = $this->db->update('balance', array('balance' => ($balance + $hasil)));
                if ($this->db->affected_rows() > 0) {
                    return array(
                        'status' => true,
                        'data' => array('balance' => ($balance + $hasil))
                    );
                } else {
                    return array(
                        'status' => false,
                        'data' => 'fail in update'
                    );
                }
            } else {
                return array(
                    'status' => false,
                    'data' => []
                );
            }
        } else {

            return array(
                'status' => true,
                'data' => []
            );
        }
    }

    function get_data_last_transaksi($cond)
    {
        $this->db->select('id as transaction_id,'
            . '(finish_time - order_time) as lama,'
            . 'finish_time,'
            . 'price,'
            . 'final_cost,'
            . 'promo_discount,'
            . 'service_order,'
            . 'customer_id,'
            . 'service.home, service.service,'
            . 'wallet_payment');
        $this->db->from('transaction');
        $this->db->join('service', 'transaction.service_order = service.service_id', 'left');
        $this->db->where($cond);
        $cek = $this->db->get();
        return $cek;
    }

    public function get_trans_merchant($idtransaksi)
    {
        $this->db->select('partner.*,merchant_detail_transaction.merchant_id,merchant_detail_transaction.total_price');
        $this->db->from('merchant_detail_transaction');
        $this->db->join('partner', 'merchant_detail_transaction.merchant_id = partner.merchant_id');
        $this->db->where('transaction_id', $idtransaksi);
        return $this->db->get();
    }


    function delete_chat($otherid, $userid)
    {
        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json"
        );
        $data3 = array();
        $url3 = firebaseDb . '/chat/' . $otherid . '-' . $userid . '/.json';
        $ch3 = curl_init($url3);

        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch3, CURLOPT_POSTFIELDS, json_encode($data3));
        curl_setopt($ch3, CURLOPT_HTTPHEADER, $headers);

        $return3 = curl_exec($ch3);

        $json_data3 = json_decode($return3, true);

        $data2 = array();

        $url2 = firebaseDb . '/chat/' . $userid . '-' . $otherid . '/.json';
        $ch2 = curl_init($url2);

        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($data2));
        curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);

        $return2 = curl_exec($ch2);

        $json_data2 = json_decode($return2, true);

        $data1 = array();

        $url1 = firebaseDb . '/Inbox/' . $userid . '/' . $otherid . '/.json';
        $ch1 = curl_init($url1);

        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($data1));
        curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers);

        $return1 = curl_exec($ch1);

        $json_data1 = json_decode($return1, true);

        $data = array();

        $url = firebaseDb . '/Inbox/' . $otherid . '/' . $userid . '/.json';
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $return = curl_exec($ch);

        $json_data = json_decode($return, true);

        return $return;
    }
}
