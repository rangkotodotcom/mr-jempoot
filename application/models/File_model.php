<?php


class File_model extends CI_model
{
    public function compress($path, $width, $height)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '30%';
        $config['width'] = $width;
        $config['height'] = $height;
        $config['new_image'] = $path;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }
}
