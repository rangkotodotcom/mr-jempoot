<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faq extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }

        $this->load->model('Appsettings_model', 'appset');
        $this->load->model('Faq_model', 'faq');
    }

    public function index()
    {
        $getview['view'] = 'faq';
        $getview['menu'] = $this->appset->getMenuAdmin();
        $data['faq'] = $this->faq->getAll();

        $this->load->view('includes/header', $getview);
        $this->load->view('faq/view', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function add()
    {
        $getview['view'] = 'faq';
        $getview['menu'] = $this->appset->getMenuAdmin();

        $this->load->view('includes/header', $getview);
        $this->load->view('faq/add');
        $this->load->view('includes/footer', $getview);
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect(base_url('faq'));

        $getview['view'] = 'faq';
        $getview['menu'] = $this->appset->getMenuAdmin();
        $data['faq'] = $this->faq->getById($id);

        $this->load->view('includes/header', $getview);
        $this->load->view('faq/edit', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function save()
    {
        $post = $this->input->post();

        $data = [
            'pertanyaan' => $post['pertanyaan'],
            'jawaban'       => $post['jawaban']
        ];

        if (isset($post['id'])) {
            $save = $this->faq->save($data, $post['id']);
        } else {
            $save = $this->faq->save($data);
        }

        if ($save) {
            $this->session->set_flashdata('success', 'Faq Has Been save');
            redirect(base_url('faq'));
        } else {
            $this->session->set_flashdata('danger', 'Failed, please try again');

            if (isset($post['id'])) {
                redirect(base_url('faq/edit/' . $post['id']));
            } else {
                redirect(base_url('faq/add'));
            }
        }
    }

    public function delete($id)
    {
        $del = $this->faq->delete($id);

        if ($del) {
            $this->session->set_flashdata('success', 'Faq Has Been delete');
        } else {
            $this->session->set_flashdata('danger', 'Failed, please try again');
        }
        redirect(base_url('faq'));
    }
}
