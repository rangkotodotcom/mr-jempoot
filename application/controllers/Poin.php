<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Poin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }
        $this->load->model('Poin_model', 'poin');
        $this->load->model('customerdata_model', 'user');
        $this->load->model('Appsettings_model', 'appset');
    }

    public function walletpoin()
    {
        $getview['view'] = 'walletpoindata';
        $data['totalpoinadd'] = $this->poin->gettotalpoinadd();
        $data['totalpoinredeem'] = $this->poin->gettotalpoinredeem();
        $data['balancepoin'] = $this->poin->getallbalancepoin();
        $data['walletpoin'] = $this->poin->getwalletpoin();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('poin/walletpoindata', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function redeemcustomer()
    {
        $getview['view'] = 'redeemcustomer';
        $data['redeemcustomer'] = $this->poin->getredeemcustomer();
        $data['redeemcustomerpending'] = $this->poin->getredeemcustomerpending();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('poin/redeemcustomer', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function redeemdriver()
    {
        $getview['view'] = 'redeemdriver';
        $data['redeemdriver'] = $this->poin->getredeemdriver();
        $data['redeemdriverpending'] = $this->poin->getredeemdriverpending();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('poin/redeemdriver', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function tconfirm($id, $id_user, $amount_poin)
    {
        $token = $this->poin->gettoken($id_user);
        $regid = $this->poin->getregid($id_user);

        if ($token == NULL and $regid != NULL) {
            $topic = $regid['reg_id'];
        } else if ($regid == NULL  and $token != NULL) {
            $topic = $token['token'];
        }

        $title = 'Redeem Poin Success';
        $message = 'We Have Confirmed Your Redeem Poin';
        $balance_poin = $this->poin->getbalancepoin($id_user);

        $kode = substr($id_user, 0, 1);

        if ($kode == 'D') {
            $method = 'redeemdriver';
        } else {
            $method = 'redeemcustomer';
        }

        $name_souvenir = $this->poin->getredeembyid($id)['name_souvenir'];

        $dataWalletPoin = [
            'id_user'   => $id_user,
            'amount_poin'   => $amount_poin,
            'name_souvenir' => $name_souvenir
        ];



        $updateBalancePoin = $this->poin->editpoinredeem($id_user, $amount_poin, $balance_poin);



        if ($updateBalancePoin) {
            $this->poin->savepoinwallet($dataWalletPoin);

            $success = $this->poin->editstatusredeem($id, 1);

            if ($success) {
                $this->poin->send_notif($title, $message, $topic);
                $this->session->set_flashdata('success', 'Redeem Poin Has Been Confirmed');
            } else {
                $this->session->set_flashdata('danger', 'Error, please try again!');
            }
        } else {

            $this->session->set_flashdata('danger', 'Error, please try again!');
        }
        redirect('poin/' . $method);
    }

    public function tcancel($id, $id_user)
    {
        $token = $this->poin->gettoken($id_user);
        $regid = $this->poin->getregid($id_user);

        if ($token == NULL and $regid != NULL) {
            $topic = $regid['reg_id'];
        }

        if ($regid == NULL and $token != NULL) {
            $topic = $token['token'];
        }


        $kode = substr($id_user, 0, 1);

        if ($kode == 'D') {
            $method = 'redeemdriver';
        } else {
            $method = 'redeemcustomer';
        }

        $title = 'Redeem poin canceled';
        $message = 'Sorry, redeem poin has been canceled';

        $success = $this->poin->editstatusredeem($id, 2);
        $this->poin->send_notif($title, $message, $topic);
        if ($success) {
            $this->session->set_flashdata('success', 'Redeem poin has been canceled');
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
        }


        redirect('poin/' . $method);
    }
}
