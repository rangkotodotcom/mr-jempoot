<?php


class Test extends CI_Controller
{
    function index()
    {

        $id_user = 'D1639896307';

        $balance = $this->db->get_where('balance', ['id_user' => $id_user])->row_array();

        $dataTrx = [
            'balance'   => $balance['balance'],
            'type'      => 'withdraw'
        ];

        $dataFCM = [
            'title' => 'Halo, Ini Judul',
            'body'  => 'Ini adala bodynya',
            'kode_transaksi' => '',
            'data'  => $dataTrx
        ];



        echo notif_fcm($dataFCM, $id_user);
    }
}
