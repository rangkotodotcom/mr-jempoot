<?php

class Charge extends CI_Controller
{

    public function index()
    {


        $re = file_get_contents("php://input");
        $data = json_decode($re);
        $ch = curl_init();
        $headers = [
            'Content-Type: application/json',
            'Authorization: Basic U0ItTWlkLXNlcnZlci1Hb2IzRFpMajlsS01tSC00Y3o1dWtaQko6',
            'Accept: application/json'
        ];

        curl_setopt($ch, CURLOPT_URL, "https://app.sandbox.midtrans.com/snap/v1/transactions");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $apiResponse = curl_exec($ch);

        print($apiResponse);
    }
    public function detailTrans()
    {
        $ch = curl_init();
        $headers = [
            'Content-Type: application/json',
            'Authorization: Basic U0ItTWlkLXNlcnZlci1Hb2IzRFpMajlsS01tSC00Y3o1dWtaQko6',
            'Accept: application/json'
        ];

        curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.midtrans.com/v2/c9043669-ab12-465d-b76e-8a61d89fd4f3/status");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $apiResponse = curl_exec($ch);

        echo $apiResponse;
    }
}
