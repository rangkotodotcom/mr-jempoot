<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Additional extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }
        $this->load->model('Additional_model', 'Madd');
        $this->load->library('form_validation');
        $this->load->model('Appsettings_model', 'appset');
    }

    public function familyrelation()
    {
        $getview['view'] = 'familyrelation';
        $getview['menu'] = $this->appset->getMenuAdmin();
        $data['familyrelation'] = $this->Madd->getAllFamilyRelation();

        $this->load->view('includes/header', $getview);
        $this->load->view('additional/familyrelation', $data);
        $this->load->view('includes/footer', $getview);
    }


    public function addfamilyrelation()
    {

        $this->form_validation->set_rules('relationship', 'relationship', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {


            if ($this->input->post('view') == 'edit') {
                $data1             = [
                    'id'                => html_escape($this->input->post('id', TRUE)),
                    'relationship'                => html_escape($this->input->post('relationship', TRUE))
                ];
            } else {
                $data             = [
                    'relationship'                => html_escape($this->input->post('relationship', TRUE))
                ];
            }

            if (demo == TRUE) {
                $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
                redirect('additional/familyrelation');
            } else {
                if ($this->input->post('view') == 'edit') {
                    $success = $this->Madd->editfamilyrelation($data1, $data1['id']);
                    if ($success) {
                        $this->session->set_flashdata('success', 'Family Relation Has Been changed');
                        redirect('additional/familyrelation');
                    } else {
                        $this->session->set_flashdata('danger', 'Error, please try again!');
                        redirect('additional/familyrelation');
                    }
                } else {
                    $success = $this->Madd->addfamilyrelation($data);
                    if ($success) {
                        $this->session->set_flashdata('success', 'Family Relation Has Been added');
                        redirect('additional/familyrelation');
                    } else {
                        $this->session->set_flashdata('danger', 'Error, please try again!');
                        redirect('additional/familyrelation');
                    }
                }
            }
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
            redirect('additional/familyrelation');
        }
    }


    public function deletefamilyrelation($id)
    {
        if (demo == TRUE) {
            $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
            redirect('additional/familyrelation');
        } else {
            $success =  $this->Madd->deletefamilyrelation($id);
            if ($success) {
                $this->session->set_flashdata('delete', 'Category News Has Been Deleted');
                redirect('additional/familyrelation');
            } else {
                $this->session->set_flashdata('danger', 'Error, Please try again!');
                redirect('additional/familyrelation');
            }
        }
    }


    public function itemcuisine()
    {
        $getview['view'] = 'itemcuisine';
        $data['itemcuisine'] = $this->Madd->getAllItemCuisine();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('additional/itemcuisine', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function additemcuisineview()
    {
        $getview['view'] = 'additemcuisine';

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('additional/additemcuisine');
        $this->load->view('includes/footer', $getview);
    }

    public function additemcuisine()
    {
        $this->form_validation->set_rules('name_cuisine', 'name_cuisine', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {

            $data = [
                'id_cuisine'        => $this->uuid->v4(),
                'name_cuisine'      => html_escape($this->input->post('name_cuisine', TRUE)),
                'status_cuisine'    => html_escape($this->input->post('status_cuisine', TRUE)),
            ];

            $insert = $this->Madd->additemcuisine($data);

            if ($insert) {
                $this->session->set_flashdata('success', 'Cuisine Item has been Added');
                redirect('additional/itemcuisine');
            } else {
                $this->session->set_flashdata('danger', 'Error, please try again!');
                redirect('additional/addaitemcuisineview');
            }
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
            redirect('additional/addaitemcuisineview');
        }
    }

    public function edititemcuisineview($id)
    {
        $getview['view'] = 'additemcuisine';
        $data['itemcuisine'] = $this->Madd->getitemcuisinebyid($id);

        $getview['menu'] = $this->appset->getMenuAdmin();

        $this->load->view('includes/header', $getview);
        $this->load->view('additional/edititemcuisine', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function edititemcuisine()
    {

        $this->form_validation->set_rules('name_cuisine', 'name_cuisine', 'trim|prep_for_form');
        $id = $this->input->post('id_cuisine');
        if ($this->form_validation->run() == TRUE) {

            $data = [
                'name_cuisine'     => html_escape($this->input->post('name_cuisine', TRUE)),
                'status_cuisine'   => html_escape($this->input->post('status_cuisine', TRUE)),
            ];

            $insert = $this->Madd->edititemcuisine($data, $id);

            if ($insert) {
                $this->session->set_flashdata('success', 'Cuisine Item has been changed');
                redirect('additional/itemcuisine');
            } else {
                $this->session->set_flashdata('danger', 'Error, please try again!');
                redirect('additional/edititemcuisineview/', $id);
            }
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
            redirect('additional/edititemcuisineview/', $id);
        }
    }

    public function deleteitemcuisine($id)
    {
        $this->Madd->deleteitemcuisine($id);
        $this->session->set_flashdata('success', 'Cuisine Item has been deleted');
        redirect('additional/itemcuisine');
    }


    public function itemtag()
    {
        $getview['view'] = 'itemtag';
        $data['itemtag'] = $this->Madd->getAllItemTag();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('additional/itemtag', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function additemtagview()
    {
        $getview['view'] = 'additemtag';

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('additional/additemtag');
        $this->load->view('includes/footer', $getview);
    }

    public function additemtag()
    {
        $this->form_validation->set_rules('name_tag', 'name_tag', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {

            $data = [
                'id_tag'        => $this->uuid->v4(),
                'name_tag'      => html_escape($this->input->post('name_tag', TRUE)),
                'status_tag'    => html_escape($this->input->post('status_tag', TRUE)),
            ];

            $insert = $this->Madd->additemtag($data);

            if ($insert) {
                $this->session->set_flashdata('success', 'Tag Item has been Added');
                redirect('additional/itemtag');
            } else {
                $this->session->set_flashdata('danger', 'Error, please try again!');
                redirect('additional/addaitemtagview');
            }
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
            redirect('additional/addaitemtagview');
        }
    }

    public function edititemtagview($id)
    {
        $getview['view'] = 'additemtag';
        $data['itemtag'] = $this->Madd->getitemtagbyid($id);

        $getview['menu'] = $this->appset->getMenuAdmin();

        $this->load->view('includes/header', $getview);
        $this->load->view('additional/edititemtag', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function edititemtag()
    {

        $this->form_validation->set_rules('name_tag', 'name_tag', 'trim|prep_for_form');
        $id = $this->input->post('id_tag');
        if ($this->form_validation->run() == TRUE) {

            $data = [
                'name_tag'     => html_escape($this->input->post('name_tag', TRUE)),
                'status_tag'   => html_escape($this->input->post('status_tag', TRUE)),
            ];

            $insert = $this->Madd->edititemtag($data, $id);

            if ($insert) {
                $this->session->set_flashdata('success', 'Tag Item has been changed');
                redirect('additional/itemtag');
            } else {
                $this->session->set_flashdata('danger', 'Error, please try again!');
                redirect('additional/edititemtagview/', $id);
            }
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
            redirect('additional/edititemtagview/', $id);
        }
    }

    public function deleteitemtag($id)
    {
        $this->Madd->deleteitemtag($id);
        $this->session->set_flashdata('success', 'Tag Item has been deleted');
        redirect('additional/itemtag');
    }

    public function souvenir()
    {
        $getview['view'] = 'souvenir';
        $getview['menu'] = $this->appset->getMenuAdmin();
        $data['souvenir'] = $this->Madd->getallsouvenir();

        $this->load->view('includes/header', $getview);
        $this->load->view('additional/souvenir', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function addsouvenir()
    {
        $getview['view'] = 'addsouvenir';

        $getview['menu'] = $this->appset->getMenuAdmin();

        $this->load->view('includes/header', $getview);
        $this->load->view('additional/addsouvenir');
        $this->load->view('includes/footer', $getview);
    }

    public function editsouvenir($id)
    {
        $getview['view'] = 'editsouvenir';
        $data = $this->Madd->getsouvenirbyid($id);

        $getview['menu'] = $this->appset->getMenuAdmin();

        $this->load->view('includes/header', $getview);
        $this->load->view('additional/editsouvenir', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function addsouvenirdata()

    {
        $this->form_validation->set_rules('name_souvenir', 'name_souvenir', 'trim|prep_for_form');
        $this->form_validation->set_rules('poin_buy', 'poin_buy', 'trim|prep_for_form');
        $this->form_validation->set_rules('status_souvenir', 'status_souvenir', 'trim|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
            $config['upload_path']     = './images/souvenir/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']         = 100;
            $config['file_name']     = 'name';
            $config['encrypt_name']     = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('img_souvenir')) {
                $gambar = html_escape($this->upload->data('file_name'));


                $data             = [
                    'id_souvenir'       => $this->uuid->v4(),
                    'img_souvenir'      => $gambar,
                    'name_souvenir'     => html_escape($this->input->post('name_souvenir', TRUE)),
                    'poin_buy'          => html_escape($this->input->post('poin_buy', TRUE)),
                    'status_souvenir'   => html_escape($this->input->post('status_souvenir', TRUE)),
                ];

                if (demo == TRUE) {
                    $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
                    redirect('additional/souvenir');
                } else {

                    $success = $this->Madd->adddatasouvenir($data);
                    if ($success) {
                        $this->session->set_flashdata('success', 'Souvenir Has Been Added');
                        redirect('additional/souvenir');
                    } else {
                        $this->session->set_flashdata('danger', 'Error, please try again!');
                        redirect('additional/addsouvenir');
                    }
                }
            } else {
                $this->session->set_flashdata('danger', $this->upload->display_errors());
                redirect('additional/addsouvenir');
            }
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
            redirect('additional/addsouvenir');
        }
    }

    public function editsouvenirdata($id)
    {


        $this->form_validation->set_rules('name_souvenir', 'name_souvenir', 'trim|prep_for_form');
        $this->form_validation->set_rules('poin_buy', 'poin_buy', 'trim|prep_for_form');
        $this->form_validation->set_rules('status_souvenir', 'status_souvenir', 'trim|prep_for_form');


        $data = $this->Madd->getsouvenirbyid($id);


        if ($this->form_validation->run() == TRUE) {
            $config['upload_path']     = './images/souvenir/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']         = '100';
            $config['file_name']     = 'name';
            $config['encrypt_name']     = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('img_souvenir')) {
                if ($data['img_souvenir'] != 'noimage.jpg') {
                    $gambar = $data['img_souvenir'];
                    unlink('images/souvenir/' . $gambar);
                }

                $gambar = html_escape($this->upload->data('file_name'));
            } else {
                $gambar = $data['img_souvenir'];
            }

            $data             = [
                'id_souvenir'       => $id,
                'img_souvenir'      => $gambar,
                'name_souvenir'     => html_escape($this->input->post('name_souvenir', TRUE)),
                'poin_buy'          => html_escape($this->input->post('poin_buy', TRUE)),
                'status_souvenir'   => html_escape($this->input->post('status_souvenir', TRUE)),
            ];

            if (demo == TRUE) {
                $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
                redirect('additional/souvenir');
            } else {
                $success = $this->Madd->editdatasouvenir($data);
                if ($success) {
                    $this->session->set_flashdata('success', 'Souvenir Has Been Changed');
                    redirect('additional/souvenir');
                } else {
                    $this->session->set_flashdata('danger', 'Error, please try again!');
                    redirect('additional/editsouvenir/' . $id);
                }
            }
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
            redirect('additional/editsouvenir/' . $id);
        }
    }

    public function deletesouvenir($id)
    {
        if (demo == TRUE) {
            $this->session->set_flashdata('demo', 'NOT ALLOWED FOR DEMO');
            redirect('additional/souvenir');
        } else {
            $data = $this->Madd->getsouvenirbyid($id);

            if ($data['img_souvenir'] != 'noimage.jpg') {
                $gambar = $data['img_souvenir'];
                unlink('images/souvenir/' . $gambar);
            }

            $success = $this->Madd->deletesouvenirById($id);
            if ($success) {
                $this->session->set_flashdata('success', 'Souvenir Has Been deleted');
                redirect('additional/souvenir');
            } else {
                $this->session->set_flashdata('danger', 'Error, please try again!');
                redirect('additional/souvenir');
            }
        }
    }
}
