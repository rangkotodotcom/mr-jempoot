<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wallet extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }
        $this->load->model('Wallet_model', 'wlt');
        $this->load->model('Appsettings_model', 'appset');
        $this->load->model('customerdata_model', 'user');
        $this->load->model('Appsettings_model', 'appset');
    }

    public function walletdata()
    {
        $getview['view'] = 'walletdata';
        $data['totaldiscount'] = $this->wlt->gettotaldiscount();
        $data['totalorderplus'] = $this->wlt->gettotalorderplus();
        $data['totalordermin'] = $this->wlt->gettotalordermin();
        $data['totalwithdraw'] = $this->wlt->gettotalwithdraw();
        $data['totaltopup'] = $this->wlt->gettotaltopup();
        $data['balance'] = $this->wlt->getallbalance();
        $data['currency'] = $this->appset->getcurrency();
        $data['wallet'] = $this->wlt->getwallet();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('wallet/walletdata', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function topupdata()
    {
        $getview['view'] = 'topupdata';
        $data['totaldiscount'] = $this->wlt->gettotaldiscount();
        $data['totalorderplus'] = $this->wlt->gettotalorderplus();
        $data['totalordermin'] = $this->wlt->gettotalordermin();
        $data['totalwithdraw'] = $this->wlt->gettotalwithdraw();
        $data['totaltopup'] = $this->wlt->gettotaltopup();
        $data['balance'] = $this->wlt->getallbalance();
        $data['currency'] = $this->appset->getcurrency();
        $data['wallet'] = $this->wlt->getwallet();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('wallet/topupdata', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function addtopup()
    {
        $getview['view'] = 'addtopup';
        $data['currency'] = $this->user->getcurrency();
        $data['balance'] = $this->wlt->getallsaldouser();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('wallet/manualtopup', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function addwithdraw()
    {
        $getview['view'] = 'addtopup';
        $data['currency'] = $this->user->getcurrency();
        $data['balance'] = $this->wlt->getallsaldouser();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('wallet/manualwithdraw', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function withdrawdata()
    {
        $getview['view'] = 'withdrawdata';
        $data['totaldiscount'] = $this->wlt->gettotaldiscount();
        $data['totalorderplus'] = $this->wlt->gettotalorderplus();
        $data['totalordermin'] = $this->wlt->gettotalordermin();
        $data['totalwithdraw'] = $this->wlt->gettotalwithdraw();
        $data['totaltopup'] = $this->wlt->gettotaltopup();
        $data['balance'] = $this->wlt->getallbalance();
        $data['currency'] = $this->appset->getcurrency();
        $data['wallet'] = $this->wlt->getwallet();

        $getview['menu'] = $this->appset->getMenuAdmin();
        $this->load->view('includes/header', $getview);
        $this->load->view('wallet/withdrawdata', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function addtopupdata()
    {
        if ($this->input->post('type_user') == 'customer') {
            $id_user = $this->input->post('customer_id');
        } elseif ($this->input->post('type_user') == 'partner') {
            $id_user = $this->input->post('partner_id');
        } else {
            $id_user = $this->input->post('driver_id');
        }

        $balance = html_escape($this->input->post('balance', TRUE));
        $remove = array(".", ",");
        $add = array("", "");


        $data = [
            'id_user'                       => $id_user,
            'balance'                         => str_replace($remove, $add, $balance) / 100,
            'type_user'                     => $this->input->post('type_user')
        ];

        $success = $this->wlt->updatesaldowallet($data);
        if ($success) {
            $this->session->set_flashdata('success', 'Top Up Has Been Added');
            redirect('wallet/walletdata');
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
            redirect('wallet/addtopup');
        }
    }

    public function addwithdrawdata()
    {
        if ($this->input->post('type_user') == 'customer') {
            $id_user = $this->input->post('customer_id');
        } elseif ($this->input->post('type_user') == 'partner') {
            $id_user = $this->input->post('partner_id');
        } else {
            $id_user = $this->input->post('driver_id');
        }


        $balance = html_escape($this->input->post('balance', TRUE));
        $remove = array(".", ",");
        $add = array("", "");

        $data = [
            'id_user'                       => $id_user,
            'balance'                         => str_replace($remove, $add, $balance) / 100,
            'type_user'                     => $this->input->post('type_user')
        ];

        $success = $this->wlt->updatesaldowalletwithdraw($data);

        if ($success) {
            $this->session->set_flashdata('success', 'Withdraw Has Been Added');
            redirect('wallet/walletdata');
        } else {
            $this->session->set_flashdata('danger', 'Error, please try again!');
            redirect('wallet/addwithdraw');
        }
    }

    public function wconfirm($id)
    {
        $method = 'POST';
        $url = 'iris/api/v1/payouts/approve';
        $type = 'raw';

        $dataWalletPayout = $this->db->get_where('tb_wallet_payout', ['id_wallet' => $id])->row_array();

        if ($dataWalletPayout) {
            $reference_no = $dataWalletPayout['reference_no'];

            $data = [
                'reference_nos' => [$reference_no]
            ];

            $approv = restclientapprover($method, $url, $type, json_encode($data));

            if (array_key_exists('status', $approv)) {
                if ($approv['status'] == 'ok') {
                    $success = true;
                } else {
                    $success = false;
                }
            } else {
                $success = false;
            }

            if ($success) {
                $this->session->set_flashdata('success', 'withdraw Has Been Confirmed');
            } else {
                $this->session->set_flashdata('danger', 'Error, please try again!');
            }
            redirect('wallet/withdrawdata');
        } else {
            $this->session->set_flashdata('danger', 'Error, data invalid');
            redirect('wallet/withdrawdata');
        }
    }

    public function wcancel()
    {

        $id = $this->input->post('id');
        $reject_reason = $this->input->post('reject_reason');


        $method = 'POST';
        $url = 'iris/api/v1/payouts/reject';
        $type = 'raw';

        $dataWalletPayout = $this->db->get_where('tb_wallet_payout', ['id_wallet' => $id])->row_array();

        if ($dataWalletPayout) {
            $reference_no = $dataWalletPayout['reference_no'];

            $data = [
                'reference_nos' => [$reference_no],
                'reject_reason' => $reject_reason
            ];

            $reject = restclientapprover($method, $url, $type, json_encode($data));

            if (array_key_exists('status', $reject)) {
                if ($reject['status'] == 'ok') {
                    $success = true;
                } else {
                    $success = false;
                }
            } else {
                $success = false;
            }

            if ($success) {
                $this->session->set_flashdata('success', 'withdraw Has Been Rejected');
            } else {
                $this->session->set_flashdata('danger', 'Error, please try again!');
            }
            redirect('wallet/withdrawdata');
        } else {
            $this->session->set_flashdata('danger', 'Error, data invalid');
            redirect('wallet/withdrawdata');
        }
    }

    public function payout()
    {
        $method = 'GET';
        $url = 'iris/api/v1/balance';
        $type = 'query';


        $getview['view'] = 'payout';

        $data['currency'] = $this->appset->getcurrency();
        $data['balance'] = restclientapprover($method, $url, $type);
        $data['payout'] = $this->wlt->getPayout();

        $getview['menu'] = $this->appset->getMenuAdmin();

        $this->load->view('includes/header', $getview);
        $this->load->view('wallet/payout', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function getDetailPayout($reference_no)
    {

        $method = 'GET';
        $url = 'iris/api/v1/payouts/' . $reference_no;
        $type = 'query';


        $data = restclientapprover($method, $url, $type);

        echo json_encode($data);
    }

    public function findHistoryPayout()
    {

        $method = 'GET';
        $url = 'iris/api/v1/statements';
        $type = 'raw';

        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');

        $data = [
            'from_date' => $from_date,
            'to_date'   => $to_date,
        ];


        $return = restclientapprover($method, $url, $type, json_encode($data));

        echo json_encode($return);
    }
}
