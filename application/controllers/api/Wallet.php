<?php

class Wallet extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Appsettings_model', 'Mapp');
    }

    function reqTopup()
    {
        $json = file_get_contents("php://input");
        $data_dec = json_decode($json, true);
        $data['id'] = $data_dec['id'];
        $data['id_user'] = $data_dec['idUser'];
        $data['wallet_amount'] = $data_dec['walletAmount'];
        $data['type'] = 'Topup';
        $data['keterangan'] = 'Topup By Midtrans';
        $data['status'] = 1;
        unset($data_dec['idUser']);
        unset($data_dec['walletAmount']);

        // $this->db->set('id','UUID()',FALSE);

        $inst = $this->db->insert("tb_new_wallet", $data);
        if ($inst) {
            echo json_encode(['message' => 'Permintaan berhasi diterima. Silahkan lakukan pembayaran.', 'code' => 200]);
        } else {
            echo json_encode(['message' => 'failed to request topup', 'code' => 500]);
        }
    }

    function getWalletHistory()
    {
        $json = file_get_contents("php://input");
        $d = json_decode($json);

        // $d->id = $this->input->get('id');
        $where = ['id_user' => $d->id];
        $history = $this->db->get_where("tb_new_wallet", $where)->result();
        $balance = $this->db->query("SELECT * FROM balance WHERE id_user IN ('$d->id')")->row()->balance;

        $data['data'] = $history;
        $data['balance'] = $balance;
        $data['code'] = 200;
        $data['message'] = "data ditemukan";
        echo json_encode($data);
    }

    function callbackNotification()
    {
        $json = file_get_contents("php://input");
        $data = json_decode($json);
        $where = ['id' => $data->order_id];

        $transStatus = $data->transaction_status;
        $fraudStatus = $data->fraud_status;

        $title = 'Admin Jempoot';

        if ($transStatus == 'capture') {
            if ($fraudStatus == 'challenge') {
                // TODO set transaction status on your database to 'challenge'
                // and response with 200 OK
                $update = ['status' => 4];

                $ket = 'gagal';
            } else if ($fraudStatus == 'accept') {
                // TODO set transaction status on your database to 'success'
                // and response with 200 OK
                $update = ['status' => 2];

                $ket = 'berhasil';
            }
        } else if ($transStatus == 'settlement') {
            // TODO set transaction status on your database to 'success'
            // and response with 200 OK
            $update = ['status' => 2];

            $ket = 'berhasil';
        } else if (
            $transStatus == 'cancel' ||
            $transStatus == 'deny' ||
            $transStatus == 'expire'
        ) {
            // TODO set transaction status on your database to 'failure'
            // and response with 200 OK
            $update = ['status' => 3];

            $ket = 'gagal';
        } else if ($transStatus == 'pending') {
            // TODO set transaction status on your database to 'pending' / waiting payment
            // and response with 200 OK
            $update = ['status' => 1];

            $ket = 'sedang diproses';
        }

        $upd = $this->db->update('tb_new_wallet', $update, $where);

        $dataTopUp = $this->db->get_where('tb_new_wallet', ['id' => $data->order_id])->row_array();

        $id_user = $dataTopUp['id_user'];
        $wallet_amount = $dataTopUp['wallet_amount'];
        $status = $dataTopUp['status'];

        $namaUser = $this->findNama($id_user);

        $currency = $this->Mapp->getcurrency();
        $formatAmount = number_format($wallet_amount, 0, ".", ".");

        $body = 'Halo ' . $namaUser . ', Topup saldo anda sebesar ' . $currency . ' ' . $formatAmount . ' ' . $ket;

        if ($upd) {


            $old_balance = $this->db->get_where('balance', ['id_user' => $id_user])->row_array()['balance'];

            if ($status == '2') {

                $newBalance = $old_balance + $wallet_amount;

                $this->db->set('balance', $newBalance);
                $this->db->where('id_user', $id_user);
                $this->db->update('balance');
            }

            $balance = $this->db->get_where('balance', ['id_user' => $id_user])->row_array();

            $dataTrx = [
                'balance'   => $balance
            ];

            $dataFCM = [
                'title' => $title,
                'body'  => $body,
                'type'  => 'topup',
                'data'  => $dataTrx
            ];

            notif_fcm($dataFCM, $id_user);

            //push notification
            echo json_encode(['message' => 'success to update data', 'code' => 200]);
        } else {
            echo json_encode(['message' => 'failed to update data', 'code' => 500]);
        }
    }

    function callbackNotifPayout()
    {
        $json = file_get_contents("php://input");
        $data = json_decode($json, true);

        $reference_no = $data['reference_no'];
        $amount = str_replace('.', '', $data['amount']) / 10;
        $status = $data['status'];
        $updated_at = $data['updated_at'];

        $newDate = date("j F Y H:i", strtotime($updated_at));


        $currency = $this->Mapp->getcurrency();
        $formatAmount = $data['amount'];

        $this->db->select('tb_wallet_payout.*');
        $this->db->select('tb_new_wallet.id_user');

        $this->db->where('tb_wallet_payout.reference_no', $reference_no);

        $this->db->join('tb_new_wallet', 'tb_wallet_payout.id_wallet=tb_new_wallet.id');

        $dataWalletPayout = $this->db->get('tb_wallet_payout')->row_array();

        $id_user = $dataWalletPayout['id_user'];
        $id_wallet = $dataWalletPayout['id_wallet'];

        $namaUser = $this->findNama($id_user);



        $title = 'Admin Jempoot';

        if ($status == 'completed') {

            $body = 'Halo ' . $namaUser . ', Saldo anda sebesar ' . $currency . ' ' . $formatAmount . ' berhasil ditransfer pada ' . $newDate;

            $old_balance = $this->db->get_where('balance', ['id_user' => $id_user])->row_array()['balance'];

            $newBalance = $old_balance - $amount;

            $this->db->set('balance', $newBalance);
            $this->db->where('id_user', $id_user);
            $updateBalance = $this->db->update('balance');

            if ($updateBalance) {
                $this->db->set('status_payout', $status);
                $this->db->where('reference_no', $reference_no);
                $updatePayout = $this->db->update('tb_wallet_payout');
            } else {
                $updatePayout = false;
            }

            if ($updatePayout) {
                $this->db->set('status', '2');
                $this->db->where('id', $id_wallet);
                $update =  $this->db->update('tb_new_wallet');
            } else {
                $update = false;
            }
        } else if ($status == 'processed' || $status == 'approved') {

            $body = 'Halo ' . $namaUser . ', Penarikan saldo sebesar ' . $currency . ' ' . $formatAmount . ' sedang diproses pada ' . $newDate;

            $this->db->set('status_payout', $status);
            $this->db->where('reference_no', $reference_no);
            $updatePayout = $this->db->update('tb_wallet_payout');

            if ($updatePayout) {
                $update = true;
            } else {
                $update = false;
            }
        } else if ($status == 'failed' || $status == 'rejected') {

            $body = 'Halo ' . $namaUser . ', Penarikan Saldo anda sebesar ' . $currency . ' ' . $formatAmount . ' gagal ditransfer pada ' . $newDate;

            $this->db->set('status_payout', $status);
            $this->db->where('reference_no', $reference_no);
            $updatePayout = $this->db->update('tb_wallet_payout');

            if ($updatePayout) {
                $this->db->set('status', '3');
                $this->db->where('id', $id_wallet);
                $update = $this->db->update('tb_new_wallet');
            } else {
                $update = false;
            }
        }


        if ($update) {

            $balance = $this->db->get_where('balance', ['id_user' => $id_user])->row_array();

            $dataTrx = [
                'balance'   => $balance,
            ];

            $dataFCM = [
                'title' => $title,
                'body'  => $body,
                'type'  => 'withdraw',
                'data'  => $dataTrx
            ];



            notif_fcm($dataFCM, $id_user);

            echo json_encode(['message' => 'success to update data', 'code' => 200]);
        } else {
            echo json_encode(['message' => 'failed', 'code' => 400]);
        }
    }


    public function findNama($id_user)
    {
        $kode = substr($id_user, 0, 1);

        if ($kode == 'M') {

            $index = 'partner_name';

            $this->db->select('partner.*');
            $this->db->select('merchant.merchant_token');
            $this->db->join('merchant', 'partner.merchant_id = merchant.merchant_id', 'left');
            $this->db->where('partner.partner_id', $id_user);
            $data = $this->db->get('partner')->row_array();
        } else if ($kode == 'C') {

            $index = 'customer_fullname';

            $this->db->select('token');
            $this->db->where('id', $id_user);
            $data = $this->db->get('customer')->row_array();
        } else if ($kode == 'D') {

            $index = 'driver_name';

            $this->db->select('reg_id');
            $this->db->where('id', $id_user);
            $data = $this->db->get('driver')->row_array();
        }

        if (!empty($data)) {
            return $data[$index];
        } else {
            return '';
        }
    }
}
