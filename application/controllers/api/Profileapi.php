<?php

//'tes' => number_format(200 / 100, 2, ",", "."),

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Profileapi extends REST_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('Profilapi_model', 'prfl');
        $this->load->model('Customer_model', 'cus');
        $this->load->model('Driver_model', 'drv');
        $this->load->model('File_model', 'file');


        date_default_timezone_set(time_zone);
    }


    function relationship_get($id = null)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $data = $this->prfl->getRelationship($id);


        $message = array(

            'code' => '200',

            'message' => 'found',

            'data' => $data

        );


        $this->response($message, 200);
    }


    function health_get($id_driver)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $data = $this->prfl->getHealth($id_driver);


        if ($data) {



            $message = array(

                'code' => '200',

                'message' => 'found',

                'data' => $data

            );
        } else {

            $message = array(

                'code' => '404',

                'message' => 'not found',

                'data' => ''

            );
        }


        $this->response($message, 200);
    }


    public function health_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $imagevaccine = $dec_data->vaccine;
        $namafotovaccine = time() . '-' . rand(0, 99999) . ".jpg";
        $pathvaccine = "images/photofile/health/" . $namafotovaccine;
        file_put_contents($pathvaccine, base64_decode($imagevaccine));

        $this->file->compress($pathvaccine, 400, 300);

        $imageswab = $dec_data->swab;
        $namafotoswab = time() . '-' . rand(0, 99999) . ".jpg";
        $pathswab = "images/photofile/health/" . $namafotoswab;
        file_put_contents($pathswab, base64_decode($imageswab));

        $this->file->compress($pathswab, 300, 400);

        $imagefreecovid = $dec_data->free_covid;
        $namafotofreecovid = time() . '-' . rand(0, 99999) . ".jpg";
        $pathfreecovid = "images/photofile/health/" . $namafotofreecovid;
        file_put_contents($pathfreecovid, base64_decode($imagefreecovid));

        $this->file->compress($pathfreecovid, 300, 400);

        $data_berkas = [
            'id_health'     => $this->uuid->v4(),
            'driver_id'     => $dec_data->driver_id,
            'vaccine'       => $namafotovaccine,
            'swab'          => $namafotoswab,
            'free_covid'    => $namafotovaccine
        ];


        $health = $this->prfl->healthverification($data_berkas);

        if ($health) {
            $message = array(
                'code' => '200',
                'message' => 'success',
                'data' => ''
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => ''
            );
            $this->response($message, 201);
        }
    }


    public function changePasswordDriver_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->prfl->savePasswordDriver($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }


    public function changeStatusDriver_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->prfl->saveStatusDriver($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => $save

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed',

                'data' => ''

            );
        }

        $this->response($message, 200);
    }


    public function changePasswordCustomer_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->prfl->savePasswordCustomer($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }


    public function changeProfilCustomer_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->prfl->saveProfilCustomer($this->post());

        $condition = [
            'phone_number'  => $this->post('phone_number')
        ];

        $data = $this->cus->get_data_pelanggan($condition);

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data'      => $data->row(),

                'image_cus'  => base_url('images/customer'),

                'image_driver'  => base_url('images/driverphoto'),

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }


    public function changeEmailCustomer_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->prfl->saveEmailCustomer($this->post());

        $condition = [
            'phone_number'  => $this->post('phone_number')
        ];

        $data = $this->cus->get_data_pelanggan($condition);


        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => $data->row()

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }


    public function changeEmailDriver_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->prfl->saveEmailDriver($this->post());

        $condition = [
            'phone_number'  => $this->post('phone_number')
        ];

        $data = $this->drv->get_data_pelanggan($condition);


        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => $data->row()

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }


    public function checkEmail_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $condition = [
            'phone_number'  => $this->post('phone_number')
        ];

        $data = $this->drv->get_data_pelanggan($condition)->row();

        $return = [
            'email' => $data->email,
            'is_verify' => $data->is_verify
        ];


        $message = array(

            'code' => '200',

            'message' => 'success',

            'data' => $return

        );

        $this->response($message, 200);
    }


    public function changePhoneCustomer_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->prfl->savePhoneCustomer($this->post());

        $condition = [
            'id'  => $this->post('id')
        ];

        $data = $this->cus->get_data_pelanggan($condition);


        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => $data->row()

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    public function changePhoneDriver_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->prfl->savePhoneDriver($this->post());

        $condition = [
            'id'  => $this->post('id')
        ];

        $data = $this->drv->get_data_pelanggan($condition);


        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => $data->row()

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }
}
