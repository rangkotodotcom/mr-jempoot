<?php

// include_once(dirname(__FILE__) . "/Baseapi.php");

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Appsettings extends REST_Controller
{
	public function getSyarat_get()
	{
		$syarat = $this->db->query("SELECT * FROM app_settings")->row();

		if (!empty($syarat)) {
			$message = [
				'code' => 200,
				'app_settings'	=> $syarat
			];
		} else {
			$message = [
				'code' => 404,
				'app_settings'	=> ''
			];
		}
		$this->response($message, 200);
	}

	public function getFaq_get()
	{
		$faq = $this->db->query("SELECT * FROM tb_faq")->result();
		if (!empty($faq)) {
			$message = [
				'code' => 200,
				'faq'	=> $faq
			];
		} else {
			$message = [
				'code' => 404,
				'faq'	=> ''
			];
		}
		$this->response($message, 200);
	}
}
