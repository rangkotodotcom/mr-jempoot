<?php


class User extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        header('Content-Type:application/json');
    }

    function getKtpUser()
    {
        $json = file_get_contents("php://input");

        $data = json_decode($json, true);
        $data['id'] = sprintf('%04x-%04x-%04x-%04x', mt_rand(10, 99), mt_rand(10, 99), mt_rand(10, 99), mt_rand(10, 99));

        $ins = $this->db->insert("tb_ktp", $data);

        if ($ins) {
            echo json_encode(['message' => 'data berhasil dikirimkan. Admin akan melakukan verifikasi terlebih dahulu.', 'code' => 200]);
        } else {
            echo json_encode(['message' => 'terjadi kesalahan saat server menerima data', 'code' => 500]);
        }
    }

    function checkStatusMember()
    {
        $json = file_get_contents("php://input");
        $data = json_decode($json);
        $where = ['id_user' => $data->id_user];
        // $data->id_user = $this->input->get('userid');
        $status = $this->db->query("SELECT customer.* FROM tb_ktp INNER JOIN customer ON tb_ktp.id_user=customer.id WHERE id_user IN ('$data->id_user')")->row_array();
        // print_r($where);
        if (!empty($status)) {
            if ($status['is_verify'] == "Y") {
                $status['member_type'] = "gold";
                echo json_encode(['message' => 'Selamat, Kamu Sudah menjadi member', 'code' => 200, 'data_member' => $status]);
            } else {
                echo json_encode(['message' => 'Permintaan kamu sedang dalam proses verifikasi', 'code' => 400, 'data_member' => $status]);
            }
        } else {
            echo json_encode(['message' => 'Maaf kamu belum terdaftar menjadi membership Jempoot', 'code' => 404]);
        }
    }

    function updateData()
    {
        $re = file_get_contents("php://input");
        $json = json_decode($re, true);

        $where = ['phone_number' => $json['phone_number']];
        unset($json['phone_number']);

        $upd = $this->db->update("customer", $json, $where);
        if ($upd) {
            die(json_encode(['message' => "Your account has been updated", 'code' => 200]));
        } else {
            die(json_encode(['message' => "failed to update account", 'code' => 500]));
        }
    }


    function updatePassword()
    {
        $userid = $this->input->post('userid');
        $password = $this->input->post("password");
        $sha1Pass = sha1($password);
        $where = ['phone_number' => $userid];
        $data = ['password' => $sha1Pass];

        $upd = $this->db->update("customer", $data, $where);
        if ($upd) {
            die(json_encode(['message' => "Password has been updated", 'code' => 200]));
        } else {
            die(json_encode(['message' => "failed to update password", 'code' => 500]));
        }
    }

    function getFavoriteList()
    {
        $userid = $this->input->get("userid");
        $fav = $this->db->query("SELECT * FROM favorite 
        INNER JOIN item ON favorite.item_id=item.item_id 
        INNER JOIN merchant ON item.merchant_id=merchant.merchant_id WHERE userid IN ('$userid')
        ")->result_array();

        $data = [];
        foreach ($fav as $g) {
            $extra = $g['extra'];
            $level = $g['level'];
            $varian = $g['varian'];

            $newExtra = [];
            $newLevel = [];
            $newVarian = [];

            if (!empty($extra) && $extra != '') {
                $pecahE = explode(';', $extra);

                foreach ($pecahE as $e) {

                    if ($e != '') {



                        $pecahE1 = explode(':', $e);

                        $dataExtra = [
                            'nama'  => $pecahE1[0],
                            'harga'  => $pecahE1[1],
                        ];

                        $newExtra[] = $dataExtra;
                    }
                }
            }

            if (!empty($level) && $level != '') {
                $pecahL = explode(';', $level);

                foreach ($pecahL as $e) {

                    if ($e != '') {



                        $pecahL1 = explode(':', $e);

                        $dataLevel = [
                            'nama'  => $pecahL1[0],
                            'harga'  => $pecahL1[1],
                        ];

                        $newLevel[] = $dataLevel;
                    }
                }
            }

            if (!empty($varian) && $varian != '') {
                $pecahV = explode(';', $varian);

                foreach ($pecahV as $e) {

                    if ($e != '') {

                        $pecahV1 = explode(':', $e);

                        $dataVarian = [
                            'nama'  => $pecahV1[0],
                            'harga'  => $pecahV1[1],
                        ];

                        $newVarian[] = $dataVarian;
                    }
                }
            }

            $g['extra'] = $newExtra;
            $g['level'] = $newLevel;
            $g['varian'] = $newVarian;
            $data[] = $g;
        }

        if (!empty($data)) {
            die(json_encode(['message' => 'data found', 'item_gopek' => $data, 'code' => 200]));
        } else {
            die(json_encode(['message' => 'data not found', 'code' => 404]));
        }
    }

    function addWishList()
    {
        $re = file_get_contents("php://input");
        $json = json_decode($re, true);

        $check = $this->db->query("SELECT * FROM favorite WHERE userid IN ('" . $json['userid'] . "') AND item_id IN (" . $json['item_id'] . ")")->row();
        // print_r("SELECT * FROM favorite WHERE userid IN ('".$json['userid']."') AND item_id IN (".$json['item_id'].")");
        if (!empty($check)) {
            $ins = $this->crud->deleteData("favorite", ['userid' => $json['userid'], 'item_id' => $json['item_id']]);
            if ($ins) {
                $code = 300;
                $message = "Removed from favorite";
            } else {
                $code = 500;
                $message = "Failed remove from favorite";
            }
        } else {
            $ins = $this->db->insert('favorite', $json);
            if ($ins) {
                $code = 200;
                $message = "Success to add to favorite";
            } else {
                $code = 500;
                $message = "Failed to add to favorite";
            }
        }

        echo json_encode(['message' => $message, 'code' => $code]);
    }

    function removeFromFav()
    {
        $userid = $this->input->get("userid");
        $favId = $this->input->get("favId");

        $remove = $this->db->delete("favorite", ['id' => $favId]);
        if ($remove > 0) {
            echo json_encode(['message' => 'Berhasil dihapus dari favorit', 'code' => 200]);
        } else {
            echo json_encode(['message' => 'Kesalahan server. Gagal dihapus dari favorit', 'code' => 500]);
        }
    }
}
