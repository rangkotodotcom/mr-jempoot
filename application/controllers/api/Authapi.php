<?php
//'tes' => number_format(200 / 100, 2, ",", "."),
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Authapi extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->database();
        $this->load->model('Driver_model');
        $this->load->model('Customer_model');
        $this->load->model('Merchantapi_model', 'mrc');
        $this->load->model('Merchantdata_model', 'mrcd');
        $this->load->model('Authapi_model', 'authapi');
        $this->load->model('File_model', 'file');
        date_default_timezone_set(time_zone);
    }

    function index_get()
    {
        $this->response("Api for ouride!", 200);
    }

    function register_driver_post()
    {
        // if (!isset($_SERVER['PHP_AUTH_USER'])) {
        //     header("WWW-Authenticate: Basic realm=\"Private Area\"");
        //     header("HTTP/1.0 401 Unauthorized");
        //     return false;
        // }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $email = $dec_data->email;
        $phone = $dec_data->phone_number;
        $check_exist = $this->Driver_model->check_exist($email, $phone);
        $check_exist_phone = $this->Driver_model->check_exist_phone($phone);
        $check_exist_email = $this->Driver_model->check_exist_email($email);
        $check_exist_sim = $this->Driver_model->check_sim($dec_data->driver_license_id);
        $check_exist_ktp = $this->Driver_model->check_ktp($dec_data->user_nationid);
        if ($check_exist) {
            $message = array(
                'code' => '201',
                'message' => 'email and phone number already exist',
                'data' => ''
            );
            $this->response($message, 201);
        } else if ($check_exist_phone) {
            $message = array(
                'code' => '201',
                'message' => 'phone already exist',
                'data' => ''
            );
            $this->response($message, 201);
        } else if ($check_exist_sim) {
            $message = array(
                'code' => '201',
                'message' => 'Driver license already exist',
                'data' => ''
            );
            $this->response($message, 201);
        } else if ($check_exist_ktp) {
            $message = array(
                'code' => '201',
                'message' => 'No KTP sudah terdaftar',
                'data' => ''
            );
            $this->response($message, 201);
        } else if ($check_exist_email) {
            $message = array(
                'code' => '201',
                'message' => 'email already exist',
                'data' => ''
            );
            $this->response($message, 201);
        } else {
            if ($dec_data->checked == "true") {
                $message = array(
                    'code' => '200',
                    'message' => 'next',
                    'data' => ''
                );
                $this->response($message, 200);
            } else {
                $image = $dec_data->photo;
                $namafoto = time() . '-' . rand(0, 99999) . ".jpg";
                $path = "images/driverphoto/" . $namafoto;
                file_put_contents($path, base64_decode($image));

                $this->file->compress($path, 300, 400);

                $data_signup = array(
                    'id' => 'D' . time(),
                    'driver_name' => $dec_data->driver_name,
                    'user_nationid' => $dec_data->user_nationid,
                    'dob' => $dec_data->dob,
                    'phone_number' => $dec_data->phone_number,
                    'phone' => $dec_data->phone,
                    'email' => $dec_data->email,
                    'photo' => $namafoto,
                    'password' => sha1(time()),
                    'job' => $dec_data->job,
                    'countrycode' => $dec_data->countrycode,
                    'gender' => $dec_data->gender,
                    'driver_address' => $dec_data->driver_address,
                    'reg_id' => $dec_data->token,
                    'status' => 0
                );

                $data_kendaraan = array(
                    'brand' => $dec_data->brand,
                    'type' => $dec_data->type,
                    'vehicle_registration_number' => $dec_data->vehicle_registration_number,
                    'color' => $dec_data->color
                );

                $data_kontak_darurat = [
                    'family_relationship_id'    => $dec_data->family_relationship_id,
                    'contact_number'    => $dec_data->contact_number,
                    'contact_name'      => $dec_data->contact_name
                ];

                $imagektp = $dec_data->idcard_images;
                $namafotoktp = time() . '-' . rand(0, 99999) . ".jpg";
                $pathktp = "images/photofile/ktp/" . $namafotoktp;
                file_put_contents($pathktp, base64_decode($imagektp));

                $this->file->compress($pathktp, 400, 300);

                $imagesim = $dec_data->driver_license_images;
                $namafotosim = time() . '-' . rand(0, 99999) . ".jpg";
                $pathsim = "images/photofile/sim/" . $namafotosim;
                file_put_contents($pathsim, base64_decode($imagesim));

                $this->file->compress($pathsim, 400, 300);

                $imagestnk = $dec_data->driver_stnk;
                $namafotostnk = time() . '-' . rand(0, 99999) . ".jpg";
                $pathstnk = "images/photofile/stnk/" . $namafotostnk;
                file_put_contents($pathstnk, base64_decode($imagestnk));

                $this->file->compress($pathstnk, 400, 300);

                $imageskck = $dec_data->driver_skck;
                $namafotoskck = time() . '-' . rand(0, 99999) . ".jpg";
                $pathskck = "images/photofile/skck/" . $namafotoskck;
                file_put_contents($pathskck, base64_decode($imageskck));

                $this->file->compress($pathskck, 300, 400);

                $data_berkas = array(
                    'idcard_images' => $namafotoktp,
                    'driver_license_images' => $namafotosim,
                    'driver_license_id' => $dec_data->driver_license_id
                );

                $data_berkas_2 = array(
                    'driver_stnk' => $namafotostnk,
                    'driver_skck' => $namafotoskck,
                );


                $signup = $this->authapi->signupdriver($data_signup, $data_kendaraan, $data_berkas, $data_kontak_darurat, $data_berkas_2);
                if ($signup) {
                    $message = array(
                        'code' => '200',
                        'message' => 'success',
                        'data' => 'register has been succesed!'
                    );
                    $this->response($message, 200);
                } else {
                    $message = array(
                        'code' => '201',
                        'message' => 'failed',
                        'data' => ''
                    );
                    $this->response($message, 201);
                }
            }
        }
    }

    function register_merchant_post()
    {
        // if (!isset($_SERVER['PHP_AUTH_USER'])) {
        //     header("WWW-Authenticate: Basic realm=\"Private Area\"");
        //     header("HTTP/1.0 401 Unauthorized");
        //     return false;
        // }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $email = $dec_data->email;
        $phone = $dec_data->phone_number;
        $check_exist = $this->mrc->check_exist($email, $phone);
        $check_exist_phone = $this->mrc->check_exist_phone($phone);
        $check_exist_email = $this->mrc->check_exist_email($email);
        $check_exist_ktp = $this->mrc->check_ktp($dec_data->partner_identity_number);
        if ($check_exist) {
            $message = array(
                'code' => '201',
                'message' => 'Email dan HP sudah terdaftar',
                'data' => ''
            );
            $this->response($message, 201);
        } else if ($check_exist_phone) {
            $message = array(
                'code' => '201',
                'message' => 'No HP sudah terdaftar',
                'data' => ''
            );
            $this->response($message, 201);
        } else if ($check_exist_ktp) {
            $message = array(
                'code' => '201',
                'message' => 'No KTP sudah terdaftar',
                'data' => ''
            );
            $this->response($message, 201);
        } else if ($check_exist_email) {
            $message = array(
                'code' => '201',
                'message' => 'Email sudah terdaftar',
                'data' => ''
            );
            $this->response($message, 201);
        } else {

            $countrycode = $dec_data->partner_country_code;
            $id = 'M' . time();

            $merchant_token = $dec_data->token; // harusnya token fcm

            $datamerchant = array(
                'service_id'                => $dec_data->service_id,
                'merchant_name'             => $dec_data->merchant_name,
                'merchant_address'          => $dec_data->merchant_address,
                'merchant_latitude'         => $dec_data->merchant_latitude,
                'merchant_longitude'        => $dec_data->merchant_longitude,
                'open_hour'                 => '',
                'close_hour'                => '',
                'merchant_image'            => '',
                'merchant_logo'             => '',
                'merchant_telephone_number' => str_replace("+", "", $countrycode) . $phone,
                'merchant_desc'             => '',
                'merchant_phone_number'     => $phone,
                'merchant_country_code'     => $countrycode,
                'merchant_status'           => '0',
                'merchant_open_status'      => '1',
                'merchant_token'            => $merchant_token
            );


            $idmerchant = $this->mrcd->insertmerchant($datamerchant);

            $merchant_category = $dec_data->merchant_category;

            foreach ($merchant_category as $row) {
                $datacategory[] = [
                    'id_merchant_category'  => $this->uuid->v4(),
                    'id_merchant'           => $idmerchant,
                    'id_category'           => $row
                ];
            }

            $this->db->insert_batch('merchant_category_join', $datacategory);



            $datamitra = [
                'partner_id'                => $id,
                'partner_name'              => $dec_data->partner_name,
                'partner_type_identity'     => $dec_data->partner_type_identity,
                'partner_identity_number'   => $dec_data->partner_identity_number,
                'partner_identity_name' => $dec_data->partner_identity_name,
                'partner_address'           => $dec_data->partner_address,
                'partner_email'             => $email,
                'password'                  => sha1($dec_data->password),
                'partner_telephone'         => str_replace("+", "", $countrycode) . $phone,
                'partner_phone'             => $phone,
                'partner_country_code'      => $countrycode,
                'merchant_id'               => $idmerchant,
                'partner'                   => '0',
                'partner_status'            => '0'
            ];

            $imagektp = $dec_data->idcard_images;
            $namafotoktp = time() . '-' . rand(0, 99999) . ".jpg";
            $pathktp = "images/photofile/ktp/" . $namafotoktp;
            file_put_contents($pathktp, base64_decode($imagektp));

            $this->file->compress($pathktp, 400, 300);

            $databerkas = [
                'driver_id'         => $id,
                'idcard_images'     => $namafotoktp,
            ];

            $datasaldo = [
                'id_user' => $id,
                'balance' => 0
            ];


            $imageswa = $dec_data->swa_foto;
            $namafotoswa = time() . '-' . rand(0, 99999) . ".jpg";
            $pathswa = "images/photofile/swa_foto/" . $namafotoswa;
            file_put_contents($pathswa, base64_decode($imageswa));

            $this->file->compress($pathswa, 300, 400);

            $imagenpwp = $dec_data->npwp;
            $namafotonpwp = time() . '-' . rand(0, 99999) . ".jpg";
            $pathnpwp = "images/photofile/npwp/" . $namafotonpwp;
            file_put_contents($pathnpwp, base64_decode($imagenpwp));

            $this->file->compress($pathnpwp, 400, 300);


            $databerkas2 = [
                'partner_file_id'   => $this->uuid->v4(),
                'partner_id'        => $id,
                'swa_foto'          => $namafotoswa,
                'npwp'              => $namafotonpwp
            ];

            $signup = $this->mrcd->addmerchant($datamitra, $databerkas, $datasaldo, $databerkas2);

            if ($signup) {
                $message = array(
                    'code' => '200',
                    'message' => 'success',
                    'data' => 'Registrasi berhasil, Silahkan cek status pendaftaran anda di halaman dashboard'
                );
                $this->response($message, 200);
            } else {
                $message = array(
                    'code' => '201',
                    'message' => 'failed',
                    'data' => $datamerchant
                );
                $this->response($message, 201);
            }
        }
    }

    function send_email_verify_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $send = $this->authapi->sendLinkVerify($this->post());
        $send['email'] = $this->post('email');

        if ($send['status']) {
            $message = array(
                'code' => '200',
                'data' => $send
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '400',
                'data' => $send
            );
            $this->response($message, 400);
        }
    }

    function send_email_verify_customer_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $send = $this->authapi->sendLinkVerifyCustomer($this->post());

        if ($send['status']) {
            $message = array(
                'code' => '200',
                'data' => $send
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '400',
                'data' => $send
            );
            $this->response($message, 400);
        }
    }

    public function verify_get($id, $token)
    {

        $dataPost = [
            'id'    => $id,
            'driver_token'  => $token
        ];

        $send = $this->authapi->verifyEmailDriver($dataPost);

        if ($send['status']) {
            echo "<script>alert('Email Hass Been Verify')</script>";
            // redirect(base_url());
        } else {
            echo "<script>alert('" . $send['message'] . ", please try again!')</script>";
            // redirect(base_url());
        }
    }


    public function verifycustomer_get($id, $token)
    {

        $dataPost = [
            'id'    => $id,
            'token_customer'  => $token
        ];

        $send = $this->authapi->verifyEmailCustomer($dataPost);

        if ($send['status']) {
            echo "<script>alert('Email Hass Been Verify')</script>";
            // redirect(base_url());
        } else {
            echo "<script>alert('" . $send['message'] . ", please try again!')</script>";
            // redirect(base_url());
        }
    }


    public function updateToken_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $user_id = $dec_data->user_id;
        $token = $dec_data->token;

        $update = $this->authapi->updateToken($user_id, $token);

        if ($update) {
            $message = [
                'code' => '200',
                'message'   => 'success'
            ];
        } else {
            $message = [
                'code' => '400',
                'message'   => 'failed'
            ];
        }

        $this->response($message, 200);
    }
}
