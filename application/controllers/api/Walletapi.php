<?php

//'tes' => number_format(200 / 100, 2, ",", "."),

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Walletapi extends REST_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('Walletapi_model', 'wltapi');

        $this->load->model('Appsettings_model', 'appset');

        $this->load->model('File_model', 'file');


        date_default_timezone_set(time_zone);
    }


    function walletHistory_get($driver_id, $startdate = null, $enddate = null)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $data['totaldiscount'] = $this->wltapi->gettotaldiscount($driver_id, $startdate, $enddate);
        $data['totalorderplus'] = $this->wltapi->gettotalorderplus($driver_id, $startdate, $enddate);
        $data['totalordermin'] = $this->wltapi->gettotalordermin($driver_id, $startdate, $enddate);
        $data['totalwithdraw'] = $this->wltapi->gettotalwithdraw($driver_id, $startdate, $enddate);
        $data['totaltopup'] = $this->wltapi->gettotaltopup($driver_id, $startdate, $enddate);
        $data['balance'] = $this->wltapi->getallbalance($driver_id);
        $data['currency'] = $this->appset->getcurrency();
        $data['wallet'] = $this->wltapi->getwallet($driver_id, $startdate, $enddate);
        $data['rekbank'] = $this->wltapi->getrekbank($driver_id);


        $message = array(

            'code' => '200',

            'message' => 'found',

            'data' => $data

        );


        $this->response($message, 200);
    }


    public function rekbank_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $data = file_get_contents("php://input");
        $datapost = json_decode($data, true);

        $image = $datapost['buku_tab'];
        $buku_tab = time() . '-' . rand(0, 99999) . ".jpg";

        $datapost['buku_tab'] = $buku_tab;

        $save = $this->wltapi->saverekbank($datapost);

        if ($save['status']) {


            $path = "images/savingsbook/" . $buku_tab;
            file_put_contents($path, base64_decode($image));

            $this->file->compress($path, 400, 300);

            $message = array(

                'code' => '200',

                'message' => $save['message']

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => $save['message']

            );
        }

        $this->response($message, 200);
    }


    public function withdraw_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $save = $this->wltapi->savewithdraw($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    public function poin_get($driver_id)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $data['balance_poin'] = $this->wltapi->getallbalancepoin($driver_id);
        $data['wallet_poin'] = $this->wltapi->getwalletpoin($driver_id);


        $message = array(

            'code' => '200',

            'message' => 'found',

            'data' => $data

        );


        $this->response($message, 200);
    }

    public function poin_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $save = $this->wltapi->savepoin($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    public function souvenir_get()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $data['souvenir'] = $this->wltapi->getsouvenir();


        $message = array(

            'code' => '200',

            'message' => 'found',

            'data' => $data

        );


        $this->response($message, 200);
    }

    public function redeempoin_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $save = $this->wltapi->saveredeempoin($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }
}
