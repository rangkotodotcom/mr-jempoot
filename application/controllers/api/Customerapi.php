<?php
//'tes' => number_format(200 / 100, 2, ",", "."),
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Customerapi extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->model('Customer_model');
        $this->load->model('Driver_model');
        $this->load->model('Etc');
        $this->load->model('File_model', 'file');
        $this->load->model('Transactionapi_model', 'trx');
        date_default_timezone_set(time_zone);
    }

    function index_get()
    {
        $this->response("Api for ouride!", 200);
    }

    function privacy_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $app_settings = $this->Customer_model->get_settings();

        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $app_settings
        );
        $this->response($message, 200);
    }

    function forgot_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);

        $condition = array(
            'email' => $decoded_data->email,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);
        $app_settings = $this->Customer_model->get_settings();
        $token = sha1(rand(0, 999999) . time());


        if ($cek_login->num_rows() > 0) {
            $cheker = array('msg' => $cek_login->result());
            foreach ($app_settings as $item) {
                foreach ($cheker['msg'] as $item2 => $val) {
                    $dataforgot = array(
                        'userid' => $val->id,
                        'token' => $token,
                        'idKey' => '1'
                    );
                }


                $forgot = $this->Customer_model->dataforgot($dataforgot);

                $linkbtn = base_url() . 'resetpass/rest/' . $token . '/1';
                $template = $this->Customer_model->template1($item['email_subject'], $item['email_text1'], $item['email_text2'], $item['app_website'], $item['app_name'], $linkbtn, $item['app_linkgoogle'], $item['app_address']);
                $sendmail = $this->Customer_model->emailsend($item['email_subject'] . " [ticket-" . rand(0, 999999) . "]", $decoded_data->email, $template, $item['smtp_host'], $item['smtp_port'], $item['smtp_username'], $item['smtp_password'], $item['smtp_from'], $item['app_name'], $item['smtp_secure']);
            }
            if ($forgot && $sendmail) {
                $message = array(
                    'code' => '200',
                    'message' => 'found',
                    'data' => []
                );
                $this->response($message, 200);
            } else {
                $message = array(
                    'code' => '401',
                    'message' => 'email not registered',
                    'data' => []
                );
                $this->response($message, 200);
            }
        } else {
            $message = array(
                'code' => '404',
                'message' => 'email not registered',
                'data' => []
            );
            $this->response($message, 200);
        }
    }


    function login_post()
    {
        // if (!isset($_SERVER['PHP_AUTH_USER'])) {
        //     header("WWW-Authenticate: Basic realm=\"Private Area\"");
        //     header("HTTP/1.0 401 Unauthorized");
        //     return false;
        // }

        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);
        $reg_id = array(
            'token' => $decoded_data->token
        );

        $condition = array(
            'password' => sha1($decoded_data->password),
            'phone_number' => $decoded_data->phone_number,
            //'token' => $decoded_data->token
        );
        $check_banned = $this->Customer_model->check_banned($decoded_data->phone_number);
        if ($check_banned) {
            $message = array(
                'message' => 'banned',
                'data' => []
            );
            $this->response($message, 200);
        } else {
            $cek_login = $this->Customer_model->get_data_pelanggan($condition);
            $message = array();

            if ($cek_login->num_rows() > 0) {
                $upd_regid = $this->Customer_model->edit_profile($reg_id, $decoded_data->phone_number);
                $get_pelanggan = $this->Customer_model->get_data_pelanggan($condition);

                $message = array(
                    'code' => '200',
                    'message' => 'found',
                    'data' => $get_pelanggan->result()
                );
                $this->response($message, 200);
            } else {
                $message = array(
                    'code' => '404',
                    'message' => 'wrong phone or password',
                    'data' => []
                );
                $this->response($message, 200);
            }
        }
    }

    function register_user_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $email = $dec_data->email;
        $phone = $dec_data->phone_number;
        $check_exist = $this->Customer_model->check_exist($email, $phone);
        $check_exist_phone = $this->Customer_model->check_exist_phone($phone);
        $check_exist_email = $this->Customer_model->check_exist_email($email);
        if ($check_exist) {
            $message = array(
                'code' => '201',
                'message' => 'email and phone number already exist',
                'data' => []
            );
            $this->response($message, 201);
        } else if ($check_exist_phone) {
            $message = array(
                'code' => '201',
                'message' => 'phone already exist',
                'data' => []
            );
            $this->response($message, 201);
        } else if ($check_exist_email) {
            $message = array(
                'code' => '201',
                'message' => 'email already exist',
                'data' => []
            );
            $this->response($message, 201);
        } else {
            if ($dec_data->checked == "true") {
                $message = array(
                    'code' => '200',
                    'message' => 'next',
                    'data' => []
                );
                $this->response($message, 200);
            } else {
                $image = $dec_data->customer_image;
                $namafoto = time() . '-' . rand(0, 99999) . ".jpg";
                $path = "./images/customer/" . $namafoto;
                file_put_contents($path, base64_decode($image));

                $this->file->compress($path, 300, 400);

                $data_signup = array(
                    'id' => 'P' . time(),
                    'customer_fullname' => $dec_data->customer_fullname,
                    'email' => $dec_data->email,
                    'phone_number' => $dec_data->phone_number,
                    'phone' => $dec_data->phone,
                    'password' => sha1($dec_data->password),
                    'dob' => $dec_data->dob,
                    'countrycode' => $dec_data->countrycode,
                    'customer_image' => $namafoto,
                    'token' => $dec_data->token,
                );
                $signup = $this->Customer_model->signup($data_signup);
                if ($signup) {
                    $condition = array(
                        'password' => sha1($dec_data->password),
                        'email' => $dec_data->email
                    );
                    $datauser1 = $this->Customer_model->get_data_pelanggan($condition);
                    $message = array(
                        'code' => '200',
                        'message' => 'success',
                        'data' => $datauser1->result()
                    );
                    $this->response($message, 200);
                } else {
                    $message = array(
                        'code' => '201',
                        'message' => 'failed',
                        'data' => []
                    );
                    $this->response($message, 201);
                }
            }
        }
    }

    function kodepromo_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $promocode = $this->Customer_model->promo_code_use($dec_data->code, $dec_data->service);
        if ($promocode) {
            $message = array(
                'code' => '200',
                'message' => 'success',
                'nominal' => $promocode['nominal'],
                'type' => $promocode['type']
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed'
            );
            $this->response($message, 200);
        }
    }

    function listkodepromo_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $promocode = $this->Customer_model->promo_code()->result();
        $message = array(
            'code' => '200',
            'message' => 'success',
            'data' => $promocode
        );
        $this->response($message, 200);
    }


    function meal_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;
        $merchantnearby = $this->Customer_model->merchantnearby($long, $lat);
        $allDataMealNearBy = array();
        foreach ($merchantnearby as $mn) {
            $item = $this->db->query("SELECT * FROM item WHERE merchant_id IN ('" . $mn['merchant_id'] . "')")->result_array();
            foreach ($item as $k => $i) {
                if (count($allDataMealNearBy) <= 20) {
                    $i['raw_item_price'] = $i['item_price'];
                    $i['raw_promo_price'] = $i['promo_price'];
                    $i['off'] = (int)(($i['promo_price'] / $i['item_price']) * 100);
                    $i['item_price'] = $this->Etc->thousandsCurrencyFormat($i['item_price']);
                    $i['promo_price'] = $this->Etc->thousandsCurrencyFormat($i['promo_price']);

                    $i['varian'] = json_decode($i['varian']);

                    $allDataMealNearBy[] = $i;
                } else {
                    break;
                }
            }
        }
        if (!empty($allDataMealNearBy)) {
            $message = array(
                'code' => '200',
                'message' => 'success',
                'data' => $allDataMealNearBy,
                'total_data' => count($allDataMealNearBy)
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => [],
                'total_data' => count($allDataMealNearBy)
            );
            $this->response($message, 201);
        }
    }


    function home_post()
    {
        // if (!isset($_SERVER['PHP_AUTH_USER'])) {
        //     header("WWW-Authenticate: Basic realm=\"Private Area\"");
        //     header("HTTP/1.0 401 Unauthorized");
        //     return false;
        // }
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $slider = $this->Customer_model->sliderhome();
        $service = $this->Customer_model->fiturhome();
        $allfitur = $this->Customer_model->fiturhomeall();
        $rating = $this->Customer_model->ratinghome();
        $balance = $this->Customer_model->saldouser($dec_data->id);
        $app_settings = $this->Customer_model->get_settings();
        $news = $this->Customer_model->beritahome();
        $kategorymerchant = $this->Customer_model->kategorymerchant()->result();
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;
        $merchantpromo = $this->Customer_model->merchantpromo($long, $lat)->result();
        $merchantnearby = $this->Customer_model->merchantnearby($long, $lat);

        $allDataMealNearBy = array();
        foreach ($merchantnearby as $mn) {
            $item = $this->db->query("SELECT * FROM item WHERE merchant_id IN ('" . $mn['merchant_id'] . "')")->result_array();
            foreach ($item as $k => $i) {
                if (count($allDataMealNearBy) <= 20) {
                    $i['raw_item_price'] = $i['item_price'];
                    $i['raw_promo_price'] = $i['promo_price'];
                    $i['off'] = (int)(($i['promo_price'] / $i['item_price']) * 100);
                    $i['item_price'] = $this->Etc->thousandsCurrencyFormat($i['item_price']);
                    $i['promo_price'] = $this->Etc->thousandsCurrencyFormat($i['promo_price']);
                    $allDataMealNearBy[] = $i;
                } else {
                    break;
                }
            }
        }


        //get makan disukai paling banyak
        $allFavMeal = array();
        $fav = $this->db->query("SELECT * FROM item ORDER BY disuka DESC LIMIT 20")->result_array();
        foreach ($fav as $f) {
            $f['raw_item_price'] = $f['item_price'];
            $f['raw_promo_price'] = $f['promo_price'];
            $f['off'] = (int)(($f['promo_price'] / $f['item_price']) * 100);
            $f['item_price'] = $this->Etc->thousandsCurrencyFormat($f['item_price']);
            $f['promo_price'] = $this->Etc->thousandsCurrencyFormat($f['promo_price']);
            $allFavMeal[] = $f;
        }



        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);
        $payu = $this->Customer_model->payusettings()->result();
        foreach ($app_settings as $item) {
            if ($cek_login->num_rows() > 0) {
                $message = array(
                    'code' => '200',
                    'message' => 'success',
                    'balance' => $balance->row('balance'),
                    'currency' => $item['app_currency'],
                    'currency_text' => $item['app_currency_text'],
                    'app_aboutus' => $item['app_aboutus'],
                    'app_cost' => $item['app_cost'],
                    'app_contact' => $item['app_contact'],
                    'app_website' => $item['app_website'],
                    'stripe_active' => $item['stripe_active'],
                    'stripe_publish' => $item['stripe_published_key'],
                    'paypal_key' => $item['paypal_key'],
                    'paypal_mode' => $item['paypal_mode'],
                    'paypal_active' => $item['paypal_active'],
                    'app_email' => $item['app_email'],
                    'slider' => $slider,
                    'service' => $service,
                    'allfitur' => $allfitur,
                    // 'ratinghome' => $rating,
                    'beritahome' => $news,
                    'kategorymerchanthome' => $kategorymerchant,
                    // 'merchantnearby' => $merchantnearby,
                    'nearby_meal' => $allDataMealNearBy,
                    'fav_meal' => $allFavMeal,
                    'merchantpromo' => $merchantpromo,
                    'data' => $cek_login->result(),
                    'payu' => $payu


                );
                $this->response($message, 200);
            } else {
                $message = array(
                    'code' => '201',
                    'message' => 'failed',
                    'data' => []
                );
                $this->response($message, 201);
            }
        }
    }

    public function merchantbykategori_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $category = $dec_data->category;
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;
        $merchantbykategori = $this->Customer_model->merchantbykategori($category, $long, $lat)->result();
        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);
        if ($cek_login->num_rows() > 0) {
            $message = array(
                'code' => '200',
                'message' => 'success',

                'merchantbykategori' => $merchantbykategori
            );

            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }

    public function merchantbykategoripromo_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $category = $dec_data->category;
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;

        $merchantbykategori = $this->Customer_model->merchantbykategoripromo($category, $long, $lat)->result();
        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);
        if ($cek_login->num_rows() > 0) {
            $message = array(
                'code' => '200',
                'message' => 'success',

                'merchantbykategori' => $merchantbykategori
            );

            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }

    public function allmerchant_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);


        $service = $dec_data->service;
        $kategorymerchant = $this->Customer_model->kategorymerchantbyfitur($service)->result();
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;

        $allmerchantnearby = $this->Customer_model->allmerchantnearby($long, $lat, $service)->result_array();

        if (!empty($allmerchantnearby)) {
            foreach ($allmerchantnearby as $row) {
                $newData = [];

                $keys = array_keys($row);

                $merchant_category = $this->Customer_model->merchantcategoryjoin($row['merchant_id']);

                foreach ($keys as $key => $value) {
                    $newData[$value] = $row[$value];
                }

                $newData['merchant_category'] = $merchant_category;

                $dataNew[] = $newData;
            }
        } else {
            $dataNew = [];
        }

        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {
            $message = array(
                'code' => '200',
                'message' => 'success',

                'kategorymerchant' => $kategorymerchant,
                'allmerchantnearby' => $dataNew


            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }

    public function allmerchantbykategori_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);


        $service = $dec_data->service;

        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;
        $category = $dec_data->category;
        $allmerchantnearbybykategori = $this->Customer_model->allmerchantnearbybykategori($long, $lat, $service, $category)->result();
        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {
            $message = array(
                'code' => '200',
                'message' => 'success',


                'allmerchantnearby' => $allmerchantnearbybykategori


            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }

    public function searchmerchant_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $like = $dec_data->like;
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;
        $service = $dec_data->service;
        $searchmerchantnearby = $this->Customer_model->searchmerchantnearby($like, $long, $lat, $service);
        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {
            $message = array(
                'code' => '200',
                'message' => 'success',


                'allmerchantnearby' => $searchmerchantnearby


            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }

    public function merchantbyid_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $idmerchant = $dec_data->idmerchant;
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;

        $merchantbyid = $this->Customer_model->merchantbyid($idmerchant, $long, $lat)->row();
        $itemstatus = $this->Customer_model->itemstatus($idmerchant)->row();
        if (empty($itemstatus->promo_status)) {
            $itempromo = '0';
        } else {
            $itempromo = $itemstatus->promo_status;
        }


        $itembyid = $this->Customer_model->itembyidnew($idmerchant)->result_array();

        $newItem = [];

        if (!empty($itembyid)) {
            foreach ($itembyid as $ibi) {
                $extra = $ibi['extra'];
                $level = $ibi['level'];
                $varian = $ibi['varian'];

                $newExtra = [];
                $newLevel = [];
                $newVarian = [];

                if (!empty($extra) && $extra != '') {
                    $pecahE = explode(';', $extra);

                    foreach ($pecahE as $e) {

                        if ($e != '') {



                            $pecahE1 = explode(':', $e);

                            $dataExtra = [
                                'nama'  => $pecahE1[0],
                                'harga'  => $pecahE1[1],
                            ];

                            $newExtra[] = $dataExtra;
                        }
                    }
                }

                if (!empty($level) && $level != '') {
                    $pecahL = explode(';', $level);

                    foreach ($pecahL as $e) {

                        if ($e != '') {



                            $pecahL1 = explode(':', $e);

                            $dataLevel = [
                                'nama'  => $pecahL1[0],
                                'harga'  => $pecahL1[1],
                            ];

                            $newLevel[] = $dataLevel;
                        }
                    }
                }

                if (!empty($varian) && $varian != '') {
                    $pecahV = explode(';', $varian);

                    foreach ($pecahV as $e) {

                        if ($e != '') {

                            $pecahV1 = explode(':', $e);

                            $dataVarian = [
                                'nama'  => $pecahV1[0],
                                'harga'  => $pecahV1[1],
                            ];

                            $newVarian[] = $dataVarian;
                        }
                    }
                }

                $ibi['extra'] = $newExtra;
                $ibi['level'] = $newLevel;
                $ibi['varian'] = $newVarian;
                $newItem[] = $ibi;
            }
        }

        $kategoriitem = $this->Customer_model->kategoriitemnew($idmerchant)->Result();

        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {

            $message = array(
                'code'              => '200',
                'message'           => 'success',
                'idfitur'           => $merchantbyid->service_id,
                'idmerchant'        => $merchantbyid->merchant_id,
                'namamerchant'      => $merchantbyid->merchant_name,
                'alamatmerchant'    => $merchantbyid->merchant_address,
                'latmerchant'       => $merchantbyid->merchant_latitude,
                'longmerchant'      => $merchantbyid->merchant_longitude,
                'bukamerchant'      => $merchantbyid->open_hour,
                'tutupmerchant'     => $merchantbyid->close_hour,
                'descmerchant'      => $merchantbyid->merchant_desc,
                'category'          => $this->Customer_model->merchantcategoryjoin($merchantbyid->merchant_id),
                'fotomerchant'      => $merchantbyid->merchant_image,
                'telpcmerchant'     => $merchantbyid->merchant_telephone_number,
                'distance'          => $merchantbyid->distance,
                'partner'           => $merchantbyid->partner,
                'promo'             => $itempromo,
                'itembyid'          => $newItem,
                'kategoriitem'      => $kategoriitem


            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }

    public function itembykategori_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $idmerchant = $dec_data->id;

        $itemk = $dec_data->category;
        $itembykategori = $this->Customer_model->itembykategorinew($idmerchant, $itemk)->result();

        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {

            $message = array(
                'code'              => '200',
                'message'           => 'success',
                'itembycategory'    => $itembykategori


            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }

    public function itembyfilter_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $jarak = $dec_data->jarak;
        $category = $dec_data->category;
        $cuisine = $dec_data->cuisine;
        $tag = $dec_data->tag;
        $price_min = $dec_data->price_min;
        $price_max = $dec_data->price_max;
        $rate = $dec_data->rate;
        $latitude = $dec_data->latitude;
        $longitude = $dec_data->longitude;

        $dataReq = [
            'jarak'     => $jarak,
            'category'  => $category,
            'cuisine'   => $cuisine,
            'tag'       => $tag,
            'price_min' => $price_min,
            'price_max' => $price_max,
            'rate'      => $rate,
            'latitude'  => $latitude,
            'longitude' => $longitude
        ];

        $itembyfilter = $this->Customer_model->itembyfilter($dataReq);

        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {

            $message = array(
                'code'              => '200',
                'message'           => 'success',
                'itembyfilter'          => $itembyfilter


            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }


    public function searchitem_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $like = $dec_data->like;
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;
        $searchItem = $this->Customer_model->searchItem($like, $lat, $long)->result_array();
        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {
            $message = array(
                'code' => '200',
                'message' => 'success',


                'searchItem' => $searchItem


            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }

    function rate_driver_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);


        $data_rate = array();

        if ($dec_data->note == "") {
            $data_rate = array(
                'customer_id' => $dec_data->customer_id,
                'driver_id' => $dec_data->driver_id,
                'rating' => $dec_data->rating,
                'transaction_id' => $dec_data->transaction_id
            );
        } else {
            $data_rate = array(
                'customer_id' => $dec_data->customer_id,
                'driver_id' => $dec_data->driver_id,
                'rating' => $dec_data->rating,
                'transaction_id' => $dec_data->transaction_id,
                'note' => $dec_data->note
            );
        }

        $finish_transaksi = $this->Customer_model->rate_driver($data_rate);

        if ($finish_transaksi) {
            $message = array(
                'message' => 'success',
                'data' => []
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'fail',
                'data' => []
            );
            $this->response($message, 200);
        }
    }

    function rate_merchant_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);


        $data_rate = array();

        if ($dec_data->note == "") {
            $data_rate = array(
                'customer_id' => $dec_data->customer_id,
                'merchant_id' => $dec_data->merchant_id,
                'rating' => $dec_data->rating,
                'transaction_id' => $dec_data->transaction_id
            );
        } else {
            $data_rate = array(
                'customer_id' => $dec_data->customer_id,
                'merchant_id' => $dec_data->merchant_id,
                'rating' => $dec_data->rating,
                'transaction_id' => $dec_data->transaction_id,
                'note' => $dec_data->note
            );
        }

        $finish_transaksi = $this->Customer_model->rate_merchant($data_rate);

        if ($finish_transaksi) {
            $message = array(
                'message' => 'success',
                'data' => []
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'fail',
                'data' => []
            );
            $this->response($message, 200);
        }
    }

    public function intentstripe_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $condition = array(
            'id' => $dec_data->userid,
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {
            require_once APPPATH . "third_party/stripe/init.php";
            $app_settings = $this->Customer_model->get_settings();
            foreach ($app_settings as $item) {
                $stripe = array(
                    "secret_key" => $item['stripe_secret_key']
                );
            }
            \Stripe\Stripe::setApiKey($stripe['secret_key']);
            $intent =  \Stripe\Paymentintent::create([
                'amount' => $dec_data->price,
                'currency' => "usd",
                'payment_method_types' => ['card'],
                'metadata' => [
                    'user_id' => $dec_data->userid
                ],
            ]);

            $message = array(
                'message' => 'success',
                'data' => $intent->client_secret
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'error',
                'data' => ''
            );
            $this->response($message, 200);
        }
    }

    public function stripeaction_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {
            require_once APPPATH . "third_party/stripe/init.php";
            $app_settings = $this->Customer_model->get_settings();
            foreach ($app_settings as $item) {
                $stripe = array(
                    "secret_key" => $item['stripe_secret_key']
                );
            }
            \Stripe\Stripe::setApiKey($stripe['secret_key']);
            $intent = \Stripe\PaymentIntent::retrieve($dec_data->id);
            $charges = $intent->charges->data;



            if ($charges[0]->metadata->user_id == $dec_data->userid && $charges[0]->amount_refunded == 0 && empty($charges[0]->failure_code) && $charges[0]->paid == 1) {
                //order details 
                $amount = $charges[0]->amount;
                $status = $charges[0]->status;

                $datatopup = array(
                    'id'    => $this->uuid->v4(),
                    'id_user' => $charges[0]->metadata->user_id,
                    'keterangan' => 'Stripe-' . $dec_data->name . '-' . $dec_data->id,
                    'type' => 'Topup',
                    'wallet_amount' => $amount,
                    'status' => 2
                );

                if ($status == 'succeeded') {
                    $this->Customer_model->insertwallet($datatopup);
                    $saldolama = $this->Customer_model->saldouser($charges[0]->metadata->user_id);
                    $saldobaru = $saldolama->row('balance') + $amount;
                    $balance = array('balance' => $saldobaru);
                    $this->Customer_model->addsaldo($charges[0]->metadata->user_id, $balance);

                    $message = array(
                        'code' => '200',
                        'message' => 'success',
                        'data' => ''
                    );
                    $this->response($message, 200);
                } else {
                    $message = array(
                        'code' => '201',
                        'message' => 'error',
                        'data' => ''
                    );
                    $this->response($message, 200);
                }
            } else {
                $message = array(
                    'code' => '202',
                    'message' => 'error',
                    'data' => []
                );
                $this->response($message, 200);
            }
        } else {
            $message = array(
                'message' => 'error',
                'data' => ''
            );
            $this->response($message, 200);
        }
    }

    public function topupstripe_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $name = $dec_data->name;
        $email = $dec_data->email;
        $card_num = $dec_data->card_num;
        $card_cvc = $dec_data->cvc;
        $card_exp = explode("/", $dec_data->expired);

        $product = $dec_data->product;
        $number = $dec_data->number;
        $price = $dec_data->price;

        $iduser = $dec_data->id;

        //include Stripe PHP library
        require_once APPPATH . "third_party/stripe/init.php";

        //set api key
        $app_settings = $this->Customer_model->get_settings();
        foreach ($app_settings as $item) {
            $stripe = array(
                "secret_key"      => $item['stripe_secret_key'],
                "publishable_key" => $item['stripe_published_key']
            );

            if ($item['stripe_status'] == '1') {
                \Stripe\Stripe::setApiKey($stripe['secret_key']);
            } else if ($item['stripe_status'] == '2') {
                \Stripe\Stripe::setApiKey($stripe['publishable_key']);
            } else {
                \Stripe\Stripe::setApiKey("");
            }
        }

        $tokenstripe = \Stripe\Token::create([
            'card' => [
                'number' => $card_num,
                'exp_month' => $card_exp[0],
                'exp_year' => $card_exp[1],
                'cvc' => $card_cvc,
            ],
        ]);


        if (!empty($tokenstripe['id'])) {

            //add customer to stripe
            $customer = \Stripe\Customer::create(array(
                'email' => $email,
                'source' => $tokenstripe['id']
            ));

            //item information
            $itemName = $product;
            $itemNumber = $number;
            $itemPrice = $price;
            $currency = "usd";
            $orderID = "INV-" . time();

            //charge a credit or a debit card
            $charge = \Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount'   => $itemPrice,
                'currency' => $currency,
                'description' => $itemNumber,
                'metadata' => array(
                    'item_id' => $itemNumber
                )
            ));

            //retrieve charge details
            $chargeJson = $charge->jsonSerialize();

            //check whether the charge is successful
            if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {
                //order details 
                $amount = $chargeJson['amount'];
                $balance_transaction = $chargeJson['balance_transaction'];
                $currency = $chargeJson['currency'];
                $status = $chargeJson['status'];
                $date = date("Y-m-d H:i:s");

                $datatopup = array(
                    'id'    => $this->uuid->v4(),
                    'keterangan' => 'Stripe-' . $name . '-' . $card_num,
                    'type' => 'Topup',
                    'wallet_amount' => $chargeJson['amount'],
                    'status' => 2,
                    'id_user' => $iduser
                );

                if ($status == 'succeeded') {
                    $topupdata = $this->Customer_model->insertwallet($datatopup);
                    $saldolama = $this->Customer_model->saldouser($iduser);
                    $saldobaru = $saldolama->row('balance') + $itemPrice;
                    $balance = array('balance' => $saldobaru);
                    $this->Customer_model->addsaldo($iduser, $balance);

                    $message = array(
                        'code' => '200',
                        'message' => 'success',
                        'data' => []
                    );
                    $this->response($message, 200);
                } else {
                    $message = array(
                        'code' => '201',
                        'message' => 'error',
                        'data' => []
                    );
                    $this->response($message, 200);
                }
            } else {
                $message = array(
                    'code' => '202',
                    'message' => 'error',
                    'data' => []
                );
                $this->response($message, 200);
            }
        } else {
            echo "Invalid Token";
            $statusMsg = "";
        }
    }

    public function topuppaypal_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $iduser = $dec_data->id;
        $bank = $dec_data->bank;
        $nama = $dec_data->nama;
        $amount = $dec_data->amount;
        $card = $dec_data->card;
        $email = $dec_data->email;
        $phone = $dec_data->phone_number;

        $datatopup = array(
            'id'    => $this->uuid->v4(),
            'keterangan' => $bank . '-' . $nama . '-' . $card,
            'type' => 'Topup',
            'status' => 2,
            'id_user' => $iduser,
            'wallet_amount' => $amount
        );
        $check_exist = $this->Customer_model->check_exist($email, $phone);

        if ($check_exist) {
            $this->Customer_model->insertwallet($datatopup);
            $saldolama = $this->Customer_model->saldouser($iduser);
            $saldobaru = $saldolama->row('balance') + $amount;
            $balance = array('balance' => $saldobaru);
            $this->Customer_model->addsaldo($iduser, $balance);

            $message = array(
                'code' => '200',
                'message' => 'success',
                'data' => []
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'You have insufficient balance',
                'data' => []
            );
            $this->response($message, 200);
        }
    }

    public function withdraw_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $iduser = $dec_data->id;
        $bank = $dec_data->bank;
        $nama = $dec_data->nama;
        $amount = $dec_data->amount;
        $card = $dec_data->card;
        $email = $dec_data->email;
        $phone = $dec_data->phone_number;

        $saldolama = $this->Customer_model->saldouser($iduser);
        $datawithdraw = array(
            'id'    => $this->uuid->v4(),
            'keterangan' => '',
            'status' => 1,
            'wallet_amount' => $amount,
            'id_user' => $iduser,
            'type' => $dec_data->type,
        );
        $check_exist = $this->Customer_model->check_exist($email, $phone);

        if ($dec_data->type ==  "topup") {
            $withdrawdata = $this->Customer_model->insertwallet($datawithdraw);

            $message = array(
                'code' => '200',
                'message' => 'success',
                'data' => ''
            );
            $this->response($message, 200);
        } else {

            if ($saldolama->row('balance') >= $amount && $check_exist) {
                $withdrawdata = $this->Customer_model->insertwallet($datawithdraw);

                $message = array(
                    'code' => '200',
                    'message' => 'success',
                    'data' => ''
                );
                $this->response($message, 200);
            } else {
                $message = array(
                    'code' => '201',
                    'message' => 'You have insufficient balance',
                    'data' => ''
                );
                $this->response($message, 200);
            }
        }
    }

    function list_ride_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $near = $this->Customer_model->get_driver_ride($dec_data->latitude, $dec_data->longitude, $dec_data->service);
        $message = array(
            'data' => $near->result()
        );
        $this->response($message, 200);
    }

    function list_bank_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $near = $this->Customer_model->listbank();
        $message = array(
            'data' => $near->result()
        );
        $this->response($message, 200);
    }

    function list_car_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $near = $this->Customer_model->get_driver_car($dec_data->latitude, $dec_data->longitude, $dec_data->service);
        $message = array(
            'data' => $near->result()
        );
        $this->response($message, 200);
    }

    function detail_fitur_get()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $app_settings = $this->Customer_model->get_settings();
        $cost = $this->Customer_model->get_biaya();
        foreach ($app_settings as $item) {
            $message = array(
                'data' => $cost,
                'currency' => $item['app_currency'],
            );
            $this->response($message, 200);
        }
    }

    function request_transaksi_post()
    {
        // if (!isset($_SERVER['PHP_AUTH_USER'])) {
        //     header("WWW-Authenticate: Basic realm=\"Private Area\"");
        //     header("HTTP/1.0 401 Unauthorized");
        //     return false;
        // } else {
        //     $cek = $this->Customer_model->check_banned_user($_SERVER['PHP_AUTH_USER']);
        //     if ($cek) {
        //         $message = array(
        //             'message' => 'fail',
        //             'data' => 'Status User Banned'
        //         );
        //         $this->response($message, 200);
        //     }
        // }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $kode = 'TRX' . date('Ymdhis');

        $data_req = array(
            'kode_trx'  => $kode,
            'customer_id' => $dec_data->customer_id,
            'service_order' => $dec_data->service_order,
            'start_latitude' => $dec_data->start_latitude,
            'start_longitude' => $dec_data->start_longitude,
            'end_latitude' => $dec_data->end_latitude,
            'end_longitude' => $dec_data->end_longitude,
            'distance' => $dec_data->distance,
            'price' => $dec_data->price,
            'estimate_time' => $dec_data->estimasi,
            'order_time' => date('Y-m-d H:i:s'),
            'pickup_address' => $dec_data->pickup_address,
            'destination_address' => $dec_data->destination_address,
            'final_cost' => $dec_data->price,
            'promo_discount' => 0,
            'wallet_payment' => $dec_data->wallet_payment,
            'trip' => $dec_data->trip
        );

        $request = $this->Customer_model->insert_transaksi($data_req);
        if ($request['status']) {
            $message = array(
                'message' => 'success',
                'data' => $request['data']
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'fail',
                'data' => $request['data']
            );
            $this->response($message, 200);
        }
    }

    function check_status_transaksi_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $dataTrans = array(
            'transaction_id' => $dec_data->transaction_id
        );

        $getStatus = $this->Customer_model->check_status($dataTrans);
        $this->response($getStatus, 200);
    }

    function user_cancel_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $data_req = array(
            'transaction_id' => $dec_data->transaction_id
        );
        $cancel_req = $this->Customer_model->user_cancel_request($data_req);
        if ($cancel_req['status']) {
            $this->Driver_model->delete_chat($cancel_req['iddriver'], $cancel_req['idpelanggan']);
            $message = array(
                'message' => 'canceled',
                'data' => []
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'cancel fail',
                'data' => []
            );
            $this->response($message, 200);
        }
    }

    function liat_lokasi_driver_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $getLoc = $this->Customer_model->get_driver_location($dec_data->id);
        $message = array(
            'status' => true,
            'data' => $getLoc->result()
        );
        $this->response($message, 200);
    }

    function detail_transaksi_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $gettrans = $this->Customer_model->transaction($dec_data->id);
        $getdriver = $this->Customer_model->detail_driver($dec_data->driver_id);
        $getitem = $this->Customer_model->detail_item($dec_data->id);

        $message = array(
            'status' => true,
            'data' => $gettrans->result(),
            'driver' => $getdriver->result(),
            'item' => $getitem->result(),

        );
        $this->response($message, 200);
    }

    function detail_berita_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $getberita = $this->Customer_model->beritadetail($dec_data->id);
        $message = array(
            'status' => true,
            'data' => $getberita->result()
        );
        $this->response($message, 200);
    }

    function all_berita_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $getberita = $this->Customer_model->allberita();
        $message = array(
            'status' => true,
            'data' => $getberita
        );
        $this->response($message, 200);
    }

    function edit_profile_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);
        $check_exist_phone = $this->Customer_model->check_exist_phone_edit($decoded_data->id, $decoded_data->phone_number);
        $check_exist_email = $this->Customer_model->check_exist_email_edit($decoded_data->id, $decoded_data->email);
        if ($check_exist_phone) {
            $message = array(
                'code' => '201',
                'message' => 'phone already exist',
                'data' => []
            );
            $this->response($message, 201);
        } else if ($check_exist_email) {
            $message = array(
                'code' => '201',
                'message' => 'email already exist',
                'data' => []
            );
            $this->response($message, 201);
        } else {

            $condition = array(
                'phone_number' => $decoded_data->phone_number
            );
            $condition2 = array(
                'phone_number' => $decoded_data->no_telepon_lama
            );

            if ($decoded_data->customer_image == null && $decoded_data->fotopelanggan_lama == null) {
                $datauser = array(
                    'customer_fullname' => $decoded_data->customer_fullname,
                    'phone_number' => $decoded_data->phone_number,
                    'phone' => $decoded_data->phone,
                    'email' => $decoded_data->email,
                    'countrycode' => $decoded_data->countrycode,
                    'dob' => $decoded_data->dob,
                    'jk' => $decoded_data->jk,
                );
            } else {
                $image = $decoded_data->customer_image;
                $namafoto = time() . '-' . rand(0, 99999) . ".jpg";
                $path = "images/customer/" . $namafoto;
                file_put_contents($path, base64_decode($image));

                $photo = $decoded_data->fotopelanggan_lama;
                $path = "./images/customer/$photo";
                unlink("$path");


                $datauser = array(
                    'customer_fullname' => $decoded_data->customer_fullname,
                    'phone_number' => $decoded_data->phone_number,
                    'phone' => $decoded_data->phone,
                    'email' => $decoded_data->email,
                    'customer_image' => $namafoto,
                    'countrycode' => $decoded_data->countrycode,
                    'dob' => $decoded_data->dob
                );
            }


            $cek_login = $this->Customer_model->get_data_pelanggan($condition2);
            if ($cek_login->num_rows() > 0) {
                $upd_user = $this->Customer_model->edit_profile($datauser, $decoded_data->no_telepon_lama);
                $getdata = $this->Customer_model->get_data_pelanggan($condition);
                $message = array(
                    'code' => '200',
                    'message' => 'success',
                    'data' => $getdata->result()
                );
                $this->response($message, 200);
            } else {
                $message = array(
                    'code' => '404',
                    'message' => 'error data',
                    'data' => []
                );
                $this->response($message, 200);
            }
        }
    }

    function wallet_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);
        $getWallet = $this->Customer_model->getwallet($decoded_data->id);
        $message = array(
            'status' => true,
            'data' => $getWallet->result()
        );
        $this->response($message, 200);
    }

    function history_progress_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }
        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);
        $getWallet = $this->Customer_model->all_transaksi($decoded_data->id);
        $message = array(
            'status' => true,
            'data' => $getWallet->result()
        );
        $this->response($message, 200);
    }

    function max_weight_get($id = null)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        } else {
            $cek = $this->Customer_model->check_banned_user($_SERVER['PHP_AUTH_USER']);
            if ($cek) {
                $message = array(
                    'message' => 'fail',
                    'data' => 'Status User Banned'
                );
                $this->response($message, 200);
            }
        }

        if ($id == null) {
            $data = $this->Customer_model->getMaxWeight();
        } else {
            $data = $this->Customer_model->getMaxWeight($id);
        }

        if ($data) {
            $message = array(
                'code'  > 200,
                'message' => 'found',
                'data' => $data
            );
        } else {
            $message = array(
                'code'  > 404,
                'message' => 'failed',
                'data' => ''
            );
        }

        $this->response($message, 200);
    }

    function driver_job_get($id = null)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        } else {
            $cek = $this->Customer_model->check_banned_user($_SERVER['PHP_AUTH_USER']);
            if ($cek) {
                $message = array(
                    'message' => 'fail',
                    'data' => 'Status User Banned'
                );
                $this->response($message, 200);
            }
        }

        if ($id == null) {
            $data = $this->Customer_model->getDriverJob();
        } else {
            $data = $this->Customer_model->getDriverJob($id);
        }

        if ($data) {
            $message = array(
                'code'  => '200',
                'message' => 'found',
                'data' => $data
            );
        } else {
            $message = array(
                'code'  => '404',
                'message' => 'failed',
                'data' => ''
            );
        }

        $this->response($message, 200);
    }

    function request_transaksi_send_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        } else {
            $cek = $this->Customer_model->check_banned_user($_SERVER['PHP_AUTH_USER']);
            if ($cek) {
                $message = array(
                    'message' => 'fail',
                    'data' => 'Status User Banned'
                );
                $this->response($message, 200);
            }
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $someday = $dec_data->someday;

        if ($someday == '1') {
            $order_time = date('Y-m-d H:i:s');
        } else {
            $order_time = $dec_data->order_time;
        }

        $kode = 'TRX' . date('Ymdhis');

        $data_req = array(
            'kode_trx'          => $kode,
            'customer_id' => $dec_data->customer_id,
            'service_order' => $dec_data->service_order,
            'start_latitude' => $dec_data->start_latitude,
            'start_longitude' => $dec_data->start_longitude,
            'end_latitude' => $dec_data->end_latitude,
            'end_longitude' => $dec_data->end_longitude,
            'distance' => $dec_data->distance,
            'price' => $dec_data->price,
            'estimate_time' => $dec_data->estimasi,
            'order_time' => $order_time,
            'pickup_address' => $dec_data->pickup_address,
            'destination_address' => $dec_data->destination_address,
            'final_cost' => $dec_data->price,
            'promo_discount' => $dec_data->promo_discount,
            'wallet_payment' => $dec_data->wallet_payment
        );


        $dataDetail = array(
            'sender_name' => $dec_data->sender_name,
            'sender_phone' => $dec_data->sender_phone,
            'receiver_name' => $dec_data->receiver_name,
            'receiver_phone' => $dec_data->receiver_phone,
            'goods_item' => $dec_data->goods_item
        );

        $request = $this->Customer_model->insert_transaksi_send($data_req, $dataDetail);
        if ($request['status']) {
            $message = array(
                'message' => 'success',
                'data' => $request['data']->result()
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'fail',
                'data' => []
            );
            $this->response($message, 200);
        }
    }

    function find_driver_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);

        $dataReq = [
            'transaction_id' => $decoded_data->transaction_id,
            'driver_job'    => $decoded_data->driver_job
        ];

        $driver = $this->Customer_model->find_driver_send($dataReq);

        if ($driver['status']) {

            $dataTrx = $this->trx->getTrxDriver($driver['data']['id'], null, 'id', $decoded_data->transaction_id);

            $dataFCM = [
                'title' => 'Driver Jempoot',
                'body'  => 'Halo, ada pesanan nih untuk kamu',
                'type'  => 'transaksi',
                'data'  => $dataTrx
            ];


            notif_fcm($dataFCM, $driver['data']['id']);

            $message = array(
                'message' => 'success',
                'data' => $driver
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'no driver found',
                'data' => []
            );
            $this->response($message, 200);
        }
    }

    function find_packet_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $dataTrans = array(
            'kode_trx' => $dec_data->kode_trx
        );

        $getStatus = $this->Customer_model->find_packet($dataTrans);
        $this->response($getStatus, 200);
    }

    function cancel_find_driver_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);

        $dataReq = [
            'transaction_id' => $decoded_data->transaction_id,
            'keterangan' => $decoded_data->keterangan
        ];

        $cancel = $this->Customer_model->cancel_find_driver_send($dataReq);


        if ($cancel[0]) {

            $dataTrx = $this->trx->getTrxDriver($cancel[1], null, 'id', $decoded_data->transaction_id);

            $dataFCM = [
                'title' => 'Driver Jempoot',
                'body'  => 'Halo, customer membatalkan pesananny',
                'type'  => 'transaksi'
            ];

            $dataTrx['type'] = 'transaksi';

            notif_fcm($dataFCM, $cancel[1], $dataTrx);

            $message = array(
                'message' => 'success',
                'data' => ''
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 200);
        }
    }

    function changepass_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $decoded_data = json_decode($data);
        $reg_id = array(
            'password' => sha1($decoded_data->new_password)
        );

        $condition = array(
            'password' => sha1($decoded_data->password),
            'phone_number' => $decoded_data->phone_number
        );
        $condition2 = array(
            'password' => sha1($decoded_data->new_password),
            'phone_number' => $decoded_data->phone_number
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);
        $message = array();

        if ($cek_login->num_rows() > 0) {
            $upd_regid = $this->Customer_model->edit_profile($reg_id, $decoded_data->phone_number);
            $get_pelanggan = $this->Customer_model->get_data_pelanggan($condition2);

            $message = array(
                'code' => '200',
                'message' => 'found',
                'data' => $get_pelanggan->result()
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '404',
                'message' => 'wrong password',
                'data' => []
            );
            $this->response($message, 200);
        }
    }

    function report_driver_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $image1 = $dec_data->img_1;

        if ($image1 != '') {

            $namafoto1 = time() . '-' . rand(0, 99999) . ".jpg";
            $path1 = "./images/reportdriver/" . $namafoto1;
            file_put_contents($path1, base64_decode($image1));

            $this->file->compress($path1, 300, 400);
        } else {
            $namafoto1 = null;
        }

        $image2 = $dec_data->img_2;

        if ($image2 != '') {

            $namafoto2 = time() . '-' . rand(0, 99999) . ".jpg";
            $path2 = "./images/reportdriver/" . $namafoto2;
            file_put_contents($path2, base64_decode($image2));

            $this->file->compress($path2, 300, 400);
        } else {
            $namafoto2 = null;
        }

        $image3 = $dec_data->img_3;

        if ($image3 != '') {

            $namafoto3 = time() . '-' . rand(0, 99999) . ".jpg";
            $path3 = "./images/reportdriver/" . $namafoto3;
            file_put_contents($path3, base64_decode($image3));

            $this->file->compress($path3, 300, 400);
        } else {
            $namafoto3 = null;
        }

        $image4 = $dec_data->img_4;


        if ($image4 != '') {

            $namafoto4 = time() . '-' . rand(0, 99999) . ".jpg";
            $path4 = "./images/reportdriver/" . $namafoto4;
            file_put_contents($path4, base64_decode($image4));

            $this->file->compress($path4, 300, 400);
        } else {
            $namafoto4 = null;
        }

        $image5 = $dec_data->img_5;

        if ($image5 != '') {

            $namafoto5 = time() . '-' . rand(0, 99999) . ".jpg";
            $path5 = "./images/reportdriver/" . $namafoto5;
            file_put_contents($path5, base64_decode($image5));

            $this->file->compress($path5, 300, 400);
        } else {
            $namafoto5 = null;
        }


        $id_report = 'T' . date('ymdhis');

        $dataSave = [
            'id_report'         => $id_report,
            'id_driver'         => $dec_data->id_driver,
            'id_customer'       => $dec_data->id_customer,
            'category_report'   => $dec_data->category_report,
            'keterangan_report' => $dec_data->keterangan_report,
            'report_img_1'      => $namafoto1,
            'report_img_2'      => $namafoto2,
            'report_img_3'      => $namafoto3,
            'report_img_4'      => $namafoto4,
            'report_img_5'      => $namafoto5,
        ];

        $report = $this->db->insert('report_driver', $dataSave);


        if ($report) {
            $message = array(
                'code'  => '200',
                'message' => 'success',
                'data' => 'No Tiket ' . $id_report
            );
        } else {
            $message = array(
                'code'  => '400',
                'message' => 'failed',
                'data' => ''
            );
        }
        $this->response($message, 200);
    }

    function alldriver_get($id = null)
    {
        $near = $this->Customer_model->get_driver_location_admin();
        $message = array(
            'data' => $near->result()
        );
        $this->response($message, 200);
    }

    function alltransactionpickup_get()
    {
        $near = $this->Customer_model->getAlltransaksipickup();
        $message = array(
            'data' => $near->result()
        );
        $this->response($message, 200);
    }

    function alltransactiondestination_get()
    {
        $near = $this->Customer_model->getAlltransaksidestination();
        $message = array(
            'data' => $near->result()
        );
        $this->response($message, 200);
    }

    function inserttransaksimerchant_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        } else {
            $cek = $this->Customer_model->check_banned_user($_SERVER['PHP_AUTH_USER']);
            if ($cek) {
                $message = array(
                    'message' => 'fail',
                    'data' => 'Status User Banned'
                );
                $this->response($message, 200);
            }
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $kode_trx = 'TRX' . date('Ymdhis');

        $data_transaksi = array(
            'kode_trx'          => $kode_trx,
            'customer_id'      => $dec_data->customer_id,
            'service_order'       => $dec_data->service_order,
            'start_latitude'    => $dec_data->start_latitude,
            'start_longitude'   => $dec_data->start_longitude,
            'end_latitude'      => $dec_data->end_latitude,
            'end_longitude'     => $dec_data->end_longitude,
            'distance'             => $dec_data->distance,
            'price'             => $dec_data->price,
            'order_time'       => date('Y-m-d H:i:s'),
            'estimate_time'     => $dec_data->estimasi,
            'pickup_address'       => $dec_data->pickup_address,
            'destination_address'     => $dec_data->destination_address,
            'promo_discount'      => $dec_data->promo_discount,

            'wallet_payment'      => $dec_data->wallet_payment,
        );
        $total_belanja = [
            'total_belanja'     => $dec_data->total_biaya_belanja,
        ];



        $dataDetail = [
            'merchant_id'   => $dec_data->id_resto,
            'total_price'   => $dec_data->total_biaya_belanja,
            'validation_code'   => rand(0, 9999),

        ];



        $result = $this->Customer_model->insert_data_transaksi_merchant($data_transaksi, $dataDetail, $total_belanja);

        if ($result['status'] == true) {


            $pesanan = $dec_data->pesanan;

            foreach ($pesanan as $pes) {
                $item[] = [
                    'item_note' => $pes->note,
                    'item_id' => $pes->item_id,
                    'merchant_id' => $dec_data->id_resto,
                    'transaction_id' => $result['transaction_id'],
                    'item_amount' => $pes->qty,
                    'extra_trx' => $pes->extra_trx,
                    'variant_trx' => $pes->variant_trx,
                    'level_trx' => $pes->level_trx,
                    'total_cost' => $pes->total_cost,
                ];
            }

            $request = $this->Customer_model->insert_data_item($item);


            $dataReturn = $this->Customer_model->dataTransaksiMerchantById($result['transaction_id']);

            $id_user = $dataReturn['merchant_id'];

            if ($request['status']) {

                $dataFCM = [
                    'title' => 'Customer Jempoot',
                    'body'  => 'Yeay, ada pesanan baru nih dari customer',
                    'type'  => 'transaksi'
                ];

                $dataReturn['type'] = 'transaksi';

                notif_fcm($dataFCM, $id_user, $dataReturn);

                $message = array(
                    'message' => 'success',
                    'data' => $dataReturn


                );
                $this->response($message, 200);
            } else {
                $message = array(
                    'message' => 'fail',
                    'data' => []

                );
                $this->response($message, 200);
            }
        } else {
            $message = array(
                'message' => 'fail',
                'data' => []

            );
            $this->response($message, 200);
        }
    }

    function payout_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $dataTrans = array(
            'customer_id'       => $dec_data->customer_id,
            'transaction_id'    => $dec_data->transaction_id
        );

        $payOut = $this->Customer_model->payout($dataTrans);

        if ($payOut) {
            $message = [
                'code'  => 200,
                'message'   => 'success',
                'data'      => $payOut
            ];
        } else {
            $message = [
                'code'  => 400,
                'message'   => 'failed',
                'data'      => ''
            ];
        }

        $this->response($message, 200);
    }

    function cancel_payout_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $dataTrans = array(
            'customer_id'       => $dec_data->customer_id,
            'transaction_id'    => $dec_data->transaction_id
        );

        $payOut = $this->Customer_model->cancel_payout($dataTrans);

        if ($payOut) {
            $message = [
                'code'  => 200,
                'message'   => 'success',
                'data'      => $payOut
            ];
        } else {
            $message = [
                'code'  => 400,
                'message'   => 'failed',
                'data'      => ''
            ];
        }

        $this->response($message, 200);
    }

    function address_get($customer_id, $by = null, $kode = null)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        if ($by == 'id') {

            $address = $this->Customer_model->getAddressById($kode);
        } else {
            $address = $this->Customer_model->getAddress($customer_id);
        }

        if ($address) {
            $message = [
                'code'      => 200,
                'message'   => 'success',
                'data'      => $address
            ];
        } else {
            $message = [
                'code'      => 400,
                'message'   => 'failed',
                'data'      => ''
            ];
        }

        $this->response($message, 200);
    }

    function address_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $action = $dec_data->action;
        $id_customer = $dec_data->id_customer;
        $name_address = $dec_data->name_address;
        $latitude = $dec_data->latitude;
        $longitude = $dec_data->longitude;
        $address = $dec_data->address;
        $notes = $dec_data->notes;
        $id_address = $dec_data->id_address;

        $dataSave = [
            'id_customer'   => $id_customer,
            'name_address'  => $name_address,
            'name_address'  => $name_address,
            'latitude'      => $latitude,
            'longitude'     => $longitude,
            'address'       => $address,
            'notes'         => $notes
        ];

        if ($action == 'add') {
            $dataSave['id_address'] = $this->uuid->v4();

            $save = $this->Customer_model->saveAddress($dataSave);
        } else {

            $dataSave['id_address'] = $id_address;

            $save = $this->Customer_model->saveAddress($dataSave, $id_address);
        }

        if ($save) {
            $message = [
                'code'      => 200,
                'message'   => 'success'
            ];
        } else {
            $message = [
                'code'      => 400,
                'message'   => 'failed'
            ];
        }

        $this->response($message, 200);
    }

    function address_delete($id_address)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $delete = $this->Customer_model->deleteAddress($id_address);

        if ($delete) {
            $message = [
                'code'      => 200,
                'message'   => 'success'
            ];
        } else {
            $message = [
                'code'      => 400,
                'message'   => 'failed'
            ];
        }

        $this->response($message, 200);
    }

    function member_get($customer_id)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }


        $member = $this->Customer_model->getMember($customer_id);


        if ($member) {
            $message = [
                'code'      => 200,
                'message'   => 'success',
                'data'      => $member,
                'url_image' => base_url('images/member')
            ];
        } else {
            $message = [
                'code'      => 400,
                'message'   => 'failed',
                'data'      => ''
            ];
        }

        $this->response($message, 200);
    }

    function member_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $id_customer = $dec_data->id_customer;
        $identity_number = $dec_data->identity_number;

        $identity_image = $dec_data->identity_image;
        $identity_imageold = $dec_data->identity_imageold;

        if ($identity_image != '') {

            $nameidentityimage = time() . '-' . rand(0, 99999) . ".jpg";
        } else {
            $nameidentityimage = '';
        }


        $dataSave = [
            'id_customer'       => $id_customer,
            'identity_number'   => $identity_number,
            'identity_image'    => $nameidentityimage
        ];

        $save = $this->Customer_model->saveMember($dataSave);


        if ($save['status']) {

            $path = "images/member/" . $nameidentityimage;
            file_put_contents($path, base64_decode($identity_image));

            $this->file->compress($path, 400, 300);

            if ($identity_imageold != '') {

                $path = "./images/member/$identity_imageold";
                unlink("$path");
            }


            $message = [
                'code'      => 200,
                'message'   => $save['message']
            ];
        } else {
            $message = [
                'code'      => 400,
                'message'   => $save['message']
            ];
        }

        $this->response($message, 200);
    }


    function inserttransaksigokidz_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        } else {
            $cek = $this->Customer_model->check_banned_user($_SERVER['PHP_AUTH_USER']);
            if ($cek) {
                $message = array(
                    'message' => 'fail',
                    'data' => 'Status User Banned'
                );
                $this->response($message, 200);
            }
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $kode_trx = 'TRX' . date('Ymdhis');

        $data_req = array(
            'kode_trx'      => $kode_trx,
            'customer_id' => $dec_data->customer_id,
            'service_order' => $dec_data->service_order,
            'start_latitude' => $dec_data->start_latitude,
            'start_longitude' => $dec_data->start_longitude,
            'end_latitude' => $dec_data->end_latitude,
            'end_longitude' => $dec_data->end_longitude,
            'distance' => $dec_data->distance,
            'price' => $dec_data->price,
            'estimate_time' => $dec_data->estimate_time,
            'order_time' => date('Y-m-d H:i:s'),
            'pickup_address' => $dec_data->pickup_address,
            'destination_address' => $dec_data->destination_address,
            'app_cost' => $dec_data->app_cost,
            'final_cost' => $dec_data->price,
            'promo_discount' => $dec_data->promo_discount,
            'wallet_payment' => $dec_data->wallet_payment,
            'trip' => $dec_data->trip,
            'pickup_time' => $dec_data->pickup_time,
            'dropoff_time' => $dec_data->dropoff_time,
            'is_recurring' => $dec_data->is_recurring
        );

        $is_recurring  = $dec_data->is_recurring;

        if ($is_recurring == '1') {

            $data_langganan = [
                'id_langganan'  => $this->uuid->v4(),
                'start_date'    => $dec_data->start_date,
                'end_date'      => $dec_data->end_date,
                'duration'      => $dec_data->duration
            ];

            $request = $this->Customer_model->insert_transaksi_gokidz($data_req, $data_langganan);
        } else {
            $request = $this->Customer_model->insert_transaksi_gokidz($data_req);
        }

        if ($request['status']) {
            $message = array(
                'message' => 'success',
                'data' => $request['data']
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'fail',
                'data' => $request['data']
            );
            $this->response($message, 200);
        }
    }


    function trxgokidz_get($customer_id, $service, $by = null, $kode = null)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        } else {
            $cek = $this->Customer_model->check_banned_user($_SERVER['PHP_AUTH_USER']);
            if ($cek) {
                $message = array(
                    'message' => 'fail',
                    'data' => 'Status User Banned'
                );
                $this->response($message, 200);
            }
        }

        $where = [
            'transaction.customer_id'   => $customer_id,
            'transaction.service_order' => $service
        ];

        if ($by == 'recurring') {
            $where['transaction.is_recurring'] = '1';
        } else if ($by == 'id') {
            $where['transaction.id'] = $kode;
        } else if ($by == 'status') {
            $where['transaction_history.status'] = $kode;
        }


        $request = $this->Customer_model->get_data_transaksi_gokidz($where);

        if ($request) {
            $message = array(
                'message' => 'success',
                'data' => $request->result()
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'message' => 'fail',
                'data' => ''
            );
            $this->response($message, 200);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////


}
