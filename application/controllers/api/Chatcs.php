<?php
//'tes' => number_format(200 / 100, 2, ",", "."),
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Chatcs extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->database();
        // $this->load->model('Driver_model');
        // $this->load->model('Customer_model');
        // $this->load->model('Merchantapi_model', 'mrc');
        // $this->load->model('Merchantdata_model', 'mrcd');
        $this->load->model('Chatcs_model', 'mchatcs');
        $this->load->model('File_model', 'file');
        date_default_timezone_set(time_zone);
    }

    function index_get()
    {
        $this->response("Api for ouride!", 200);
    }

    public function chat_get($id_message)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $getChat = $this->mchatcs->getChat($id_message);

        if ($getChat['status']) {
            $message = array(
                'code' => '200',
                'data' => $getChat['data']
            );
        } else {
            $message = array(
                'code' => '404',
                'data' => ''
            );
        }
        $this->response($message, 200);
    }

    public function startChat_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $id_user = $dec_data->id_user;

        $saveStartChat = $this->mchatcs->startChat($id_user);

        if ($saveStartChat['status']) {
            $message = array(
                'code' => '201',
                'message' => 'berhasil terhubung ke cs',
                'data' => $saveStartChat['data']
            );
            $this->response($message, 201);
        } else {
            $message = array(
                'code' => '400',
                'message' => 'gagal terhubung ke cs',
                'data' => ''
            );
            $this->response($message, 400);
        }
    }

    public function endChat_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $id_message = $dec_data->id_message;

        $saveEndChat = $this->mchatcs->endChat($id_message);

        if ($saveEndChat['status']) {
            $message = array(
                'code' => '200',
                'message' => 'komunikasi dengan cs sudah diselesaikan',
                'data' => $saveEndChat['data']
            );
            $this->response($message, 201);
        } else {
            $message = array(
                'code' => '400',
                'message' => 'gagal menutup komunikasi dengan cs',
                'data' => ''
            );
            $this->response($message, 400);
        }
    }

    public function sendChat_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $id_message = $dec_data->id_message;
        $id_user = $dec_data->id_user;
        $message = $dec_data->message;
        $photo = $dec_data->photo;

        $namafoto = '';
        $textmessage = '';

        if ($photo != '') {
            $namafoto .= time() . '-' . rand(0, 99999) . ".jpg";
            $path = "images/chatcs/" . $namafoto;
            file_put_contents($path, base64_decode($photo));

            $this->file->compress($path, 600, 400);
        }

        if ($message != '') {
            $textmessage .= $message;
        }

        $dataSendChat = [
            'id_message'    => $id_message,
            'id_user'       => $id_user,
            'message'       => $textmessage,
            'photo'         => $namafoto
        ];

        $saveSendChat = $this->mchatcs->sendChat($dataSendChat);

        if ($saveSendChat['status']) {
            $messageR = array(
                'code' => '200',
                'message' => 'pesan berhasil dikirim',
                'data' => $saveSendChat['data']
            );
            $this->response($messageR, 201);
        } else {
            $messageR = array(
                'code' => '400',
                'message' => 'pesan gagal dikirim',
                'data' => ''
            );
            $this->response($messageR, 400);
        }
    }
}
