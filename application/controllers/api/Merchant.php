<?php
//'tes' => number_format(200 / 100, 2, ",", "."),
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Merchant extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->database();
        $this->load->model('Merchantapi_model', 'merchant');
        $this->load->model('Merchantdata_model', 'mrcd');
        $this->load->model('File_model', 'file');
        date_default_timezone_set(time_zone);
    }

    function login_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = $this->post();
        // $reg_id = array(
        //     'merchant_token' => $data['token']
        // );

        $condition = array(
            'password' => sha1($data['password']),
            'partner_telephone' => $data['phone_number'],
            //'token' => $data->token
        );
        $check_banned = $this->merchant->check_banned($data['phone_number']);
        if ($check_banned) {
            $message = array(
                'message' => 'banned',
                'data' => []
            );
            $this->response($message, 200);
        } else {
            $cek_login = $this->merchant->get_data_merchant($condition);
            $message = array();

            if ($cek_login->num_rows() > 0) {
                // $upd_regid = $this->merchant->edit_profile($reg_id, $data['phone_number']);
                $get_merchant = $this->merchant->get_data_merchant($condition);
                // $this->merchant->edit_status_login($data['phone_number']);
                $message = array(
                    'code' => '200',
                    'message' => 'found',
                    'data' => $get_merchant->result()
                );
                $this->response($message, 200);
            } else {
                $message = array(
                    'code' => '404',
                    'message' => 'wrong phone or password',
                    'data' => []
                );
                $this->response($message, 200);
            }
        }
    }

    function changePassword_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->savePassword($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    function data_home_get($merchant_id, $date_today)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $profit_today = $this->merchant->total_history_daily($date_today, $merchant_id)->row_array();
        $sell_last_week = $this->merchant->transaksi_last_week($date_today, $merchant_id)->result_array();
        $order_masuk = $this->merchant->transaksi_home($merchant_id)->result_array();
        $siap_kirim = $this->merchant->transaksi_siap_kirim($merchant_id)->result_array();
        $total_produk = $this->merchant->totalitemactive($merchant_id)->row_array();

        if ($profit_today['daily'] != null) {
            $profitToday = $profit_today;
        } else {
            $profitToday = ['daily' => 0];
        }


        $data = [
            'profit_today'  => $profitToday,
            'sell_last_week'  => $sell_last_week,
            'order_masuk'  => count($order_masuk),
            'siap_dikirim'  => count($siap_kirim),
            'total_produk'  => $total_produk,
        ];


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }


    function orderan_masuk_get($merchant_id)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }


        $transaksi = $this->merchant->transaksi_home($merchant_id)->result_array();

        if (!empty($transaksi)) {

            foreach ($transaksi as $row) {
                $keys = array_keys($row);

                foreach ($keys as $key => $value) {
                    $newData[$value] = $row[$value];
                }

                $item_transaction = $this->merchant->item_transaksi($row['transaction_id'])->result_array();

                $newData['detail_item'] = $item_transaction;

                $data['transaksi'][] = $newData;
            }
        } else {
            $data['transaksi'] = 0;
        }


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data,
            'url'   => base_url('image/driver')
        );
        $this->response($message, 200);
    }


    function siap_kirim_get($merchant_id)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $transaksi = $this->merchant->transaksi_siap_kirim($merchant_id)->result_array();

        if (!empty($transaksi)) {

            foreach ($transaksi as $row) {
                $keys = array_keys($row);

                foreach ($keys as $key => $value) {
                    $newData[$value] = $row[$value];
                }

                $item_transaction = $this->merchant->item_transaksi($row['transaction_id'])->result_array();

                $newData['detail_item'] = $item_transaction;

                $data['transaksi'][] = $newData;
            }
        } else {
            $data['transaksi'] = 0;
        }


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data,
            'url'   => base_url('image/driver')
        );
        $this->response($message, 200);
    }


    function statusPendaftaran_get($phone_number)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = $this->mrcd->getStatusPendaftaran($phone_number);

        if ($data) {
            $message = array(
                'code' => '200',
                'message' => 'found',
                'data' => $data
            );
        } else {
            $message = array(
                'code' => '400',
                'message' => 'not found',
                'data' => ''
            );
        }


        $this->response($message, 200);
    }

    function cuisine_get()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data['cuisine'] = $this->merchant->itemcuisine();


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }

    function tag_get()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data['tag'] = $this->merchant->itemtag();


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }

    function category_get($merchant_id, $by = null, $kode = null)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        if ($by == 'id') {
            $data['kategori'] = $this->merchant->kategori_byidnew($merchant_id, $kode);
        } else {
            $data['kategori_active'] = $this->merchant->kategori_activenew($merchant_id);
            $data['kategori_nonactive'] = $this->merchant->kategori_nonactivenew($merchant_id);
        }


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }

    function category_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->savecategory($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    function item_get($merchant_id, $by = null, $kode = null)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        if ($by == 'cat') {
            $item = $this->merchant->itembycatactive($merchant_id, $kode)->result_array();

            $looping = true;
        } else if ($by == 'id') {
            $item = $this->merchant->itembyid($merchant_id, $kode)->row_array();

            $looping = false;
        } else {
            $item = $this->merchant->itemallactive($merchant_id)->result_array();

            $looping = true;
        }

        if (!empty($item)) {

            if ($looping) {
                $newData = [];

                foreach ($item as $row) {
                    $keys = array_keys($row);

                    $item_tag = $this->merchant->itemtagjoin($row['item_id']);

                    foreach ($keys as $key => $value) {
                        $newData[$value] = $row[$value];
                    }

                    $newData['item_tag'] = $item_tag;

                    $data['item'][] = $newData;
                }
            } else {
                $keys = array_keys($item);

                $item_tag = $this->merchant->itemtagjoin($item['item_id']);

                foreach ($keys as $key => $value) {
                    $newData[$value] = $item[$value];
                }

                $newData['item_tag'] = $item_tag;

                $data['item'] = $newData;
            }
        } else {
            $data['item'] = [];
        }


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }

    function item_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $post = $this->post();

        $image = $post['item_image'];
        $namafoto = time() . '-' . rand(0, 99999) . ".jpg";


        $save = $this->merchant->saveitem($this->post(), $namafoto);

        if ($save) {


            $path = "images/itemphoto/" . $namafoto;
            file_put_contents($path, base64_decode($image));

            $this->file->compress($path, 300, 400);

            $message = array(

                'code' => '200',

                'message' => 'success',

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    function promo_get($merchant_id, $by = null, $id = null)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        if ($by == 'id') {
            $data['promo'] = $this->merchant->itempromo($merchant_id, $id)->row_array();
        } else {
            $data['promo'] = $this->merchant->itempromo($merchant_id)->result_array();
        }


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }

    function voucher_get($merchant_id, $by = null, $id = null)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        if ($by == 'id') {
            $data['voucher'] = $this->merchant->getvoucher($merchant_id, $by, $id)->row_array();
        }else if ($by == 'isvalid') {
            $data['voucher'] = $this->merchant->getvoucher($merchant_id, $by, $id)->row_array();
        }else {
            $data['voucher'] = $this->merchant->getvoucher($merchant_id)->result_array();
        }


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }

    function voucher_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->savevoucher($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    function voucher_delete($id)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $idDec = json_decode(base64_decode($id), true);

        $delete = $this->merchant->deletevoucher($idDec);

        if ($delete) {

            $message = array(

                'code' => '204',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }


    function etalase_get($merchant_id, $by = null, $id = null)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        if ($by == 'id') {
            $data['etalase'] = $this->merchant->getetalase($merchant_id, $id)->row_array();
        } else {
            $data['etalase'] = $this->merchant->getetalase($merchant_id)->result_array();
        }


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }

    function etalase_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->saveetalase($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    function etalase_delete($id)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->deleteetalase($id);

        if ($save) {

            $message = array(

                'code' => '204',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    function transaksi_get($merchant_id, $by = null, $id = null)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        if ($by == 'id') {
            $data['transaksi'] = $this->merchant->transaksi_all($merchant_id, $id)->row_array();
            $data['transaksi']['detail_item'] = $this->merchant->item_transaksi($data['transaksi']['transaction_id'])->result_array();
        } else if ($by == 'date') {


            // if ($id != date('m-Y')) {


            // $dataDateTransaksi = $this->merchant->tanggal_transaksi($merchant_id, $id)->result_array();




            // if (!empty($dataDateTransaksi)) {
            //     foreach ($dataDateTransaksi as $row) {


            //         $transaksi = $this->merchant->transaksi_bydate($merchant_id, $row['date_trx'])->result_array();


            //         if (!empty($transaksi)) {
            //             foreach ($transaksi as $row1) {
            //                 $keys = array_keys($row1);

            //                 foreach ($keys as $key => $value) {
            //                     $newData[$value] = $row1[$value];
            //                 }

            //                 $item_transaction = $this->merchant->item_transaksi($row1['transaction_id'])->result_array();

            //                 $newData['detail_item'] = $item_transaction;

            //                 $transaksi_all[] = $newData;
            //             }
            //         }

            //         $transaksi_all_date[][$row['date_trx']] = $transaksi_all;
            //     }
            // } else {
            //     $transaksi_all_date = [];
            // }


            // $transaksi_today = $this->merchant->transaksi_today($merchant_id, $id)->result_array();

            // if (!empty($transaksi_today)) {

            //     foreach ($transaksi_today as $row) {
            //         $keys = array_keys($row);

            //         foreach ($keys as $key => $value) {
            //             $newData[$value] = $row[$value];
            //         }

            //         $item_transaction = $this->merchant->item_transaksi($row['transaction_id'])->result_array();

            //         $newData['detail_item'] = $item_transaction;

            //         $today[] = $newData;
            //     }
            // } else {
            //     $today = [];
            // }

            // $transaksi_yesterday = $this->merchant->transaksi_yesterday($merchant_id, $id)->result_array();

            // if (!empty($transaksi_yesterday)) {

            //     foreach ($transaksi_yesterday as $row) {
            //         $keys = array_keys($row);

            //         foreach ($keys as $key => $value) {
            //             $newData[$value] = $row[$value];
            //         }

            //         $item_transaction = $this->merchant->item_transaksi($row['transaction_id'])->result_array();

            //         $newData['detail_item'] = $item_transaction;

            //         $yesterday[] = $newData;
            //     }
            // } else {
            //     $yesterday = [];
            // }

            // $transaksi_week = $this->merchant->transaksi_week($merchant_id, $id)->result_array();

            // if (!empty($transaksi_week)) {

            //     foreach ($transaksi_week as $row) {
            //         $keys = array_keys($row);

            //         foreach ($keys as $key => $value) {
            //             $newData[$value] = $row[$value];
            //         }

            //         $item_transaction = $this->merchant->item_transaksi($row['transaction_id'])->result_array();

            //         $newData['detail_item'] = $item_transaction;

            //         $week[] = $newData;
            //     }
            // } else {
            //     $week = [];
            // }

            // $data = [
            //     'total'             => $this->merchant->total_pendapatan($merchant_id, $id)->row_array()['pendapatan'],
            //     'data_transaksi'    => $transaksi_all_date,
            // ];
            // } else {

            $transaksi = $this->merchant->transaksi_bydate1($merchant_id, $id)->result_array();

            if (!empty($transaksi)) {

                foreach ($transaksi as $row) {
                    $keys = array_keys($row);

                    foreach ($keys as $key => $value) {
                        $newData[$value] = $row[$value];
                    }

                    $item_transaction = $this->merchant->item_transaksi($row['transaction_id'])->result_array();

                    $newData['detail_item'] = $item_transaction;

                    $transaksi_all[] = $newData;
                }
            } else {
                $transaksi_all = [];
            }

            $data = [
                'total' => $this->merchant->total_pendapatan($merchant_id, $id)->row_array()['pendapatan'],
                'all' => $transaksi_all,
            ];
            // }
        } else if ($by == 'status') {

            $data['transaksi'] = [];
            $transaksi = $this->merchant->transaksi_bystatus($merchant_id, $id)->result_array();

            if (!empty($transaksi)) {

                foreach ($transaksi as $row) {
                    $keys = array_keys($row);

                    foreach ($keys as $key => $value) {
                        $newData[$value] = $row[$value];
                    }

                    $item_transaction = $this->merchant->item_transaksi($row['transaction_id'])->result_array();

                    $newData['detail_item'] = $item_transaction;

                    $data['transaksi'][] = $newData;
                }
            }
        } else {
            $data['transaksi'] = [];
            $transaksi = $this->merchant->transaksi_all($merchant_id)->result_array();

            if (!empty($transaksi)) {

                foreach ($transaksi as $row) {
                    $keys = array_keys($row);

                    foreach ($keys as $key => $value) {
                        $newData[$value] = $row[$value];
                    }

                    $item_transaction = $this->merchant->item_transaksi($row['transaction_id'])->result_array();

                    $newData['detail_item'] = $item_transaction;

                    $data['transaksi'][] = $newData;
                }
            }
        }


        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data,
            'url'   => base_url('image/driver')
        );
        $this->response($message, 200);
    }

    function promo_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->savepromo($this->post());

        if ($save) {

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    function edit_merchant_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->saveeditmerchant($this->post());

        if ($save['status']) {

            $message = array(

                'code' => '200',

                'message' => 'success',

                'merchant_logo' => $save['merchant_logo'],

                'merchant_image'    => $save['merchant_image'],

                'path_image'        => $save['path_image']

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    function driver_free_get()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data['driver'] = $this->merchant->getDriverFree();



        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }


    function accept_trx_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->saveAcceptTrx($this->post());

        $dataTrx = $this->merchant->transaksi_byid($this->post('transaction_id'))->row_array();

        $id_user = $dataTrx['customer_id'];

        if ($save) {

            $dataFCM = [
                'title' => 'Merchant Jempoot',
                'body'  => 'Pesanan anda sudah diterima oleh merchant',
                'type'  => '201',
            ];

            notif_fcm($dataFCM, $id_user);

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }


    function status_trx_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->saveStatusTrx($this->post());


        $dataTrx = $this->merchant->transaksi_byid($this->post('transaction_id'))->row_array();

        $id_user = $dataTrx['customer_id'];

        if ($save) {

            if ($this->post('status_trx_merchant') == '3') {

                $dataFCM = [
                    'title' => 'Merchant Jempoot',
                    'body'  => 'Pesanan anda sedang diantarkan oleh driver',
                    'type'  => '204'
                ];

                notif_fcm($dataFCM, $id_user);
            }

            $message = array(

                'code' => '200',

                'message' => 'success'

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }

    function find_driver_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $save = $this->merchant->findDriver($this->post());


        $dataTrx = $this->merchant->transaksi_byid($this->post('transaction_id'))->row_array();

        $id_user = $dataTrx['driver_id'];

        if ($save) {


            $dataTrx['type'] = 'transaksi';

            $dataFCM = [
                'title' => 'Merchant Jempoot',
                'body'  => 'Halo, ada orderan GoPek untuk kamu nih',
                'type'  => 'transaksi',
                'data'  => $dataTrx
            ];


            notif_fcm($dataFCM, $id_user);

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data'  => $save

            );
        } else {

            $message = array(

                'code' => '400',

                'message' => 'failed'

            );
        }

        $this->response($message, 200);
    }


    public function category_merchant_get()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = $this->merchant->getCategoryMerchant();



        $message = array(
            'code' => '200',
            'message' => 'found',
            'data' => $data
        );
        $this->response($message, 200);
    }
}
