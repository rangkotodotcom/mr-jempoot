<?php

class Gopek extends CI_Controller
{


    function getDetailMenu()
    {
        $json = file_get_contents("php://input");
        $re = json_decode($json);

        $merchantDetail = $this->db->query("
        SELECT merchant.*, (SELECT SUM(rating)/COUNT(*) FROM merchant_rating WHERE merchant.merchant_id IN ('$re->restoId')) as rating FROM merchant 
        INNER JOIN merchant_rating ON merchant.merchant_id=merchant_rating.merchant_id 
        WHERE merchant.merchant_id IN ('$re->restoId') GROUP BY merchant.merchant_id
        ")->row();
        // echo json_encode($merchantDetail);

        $mealDetail = $this->db->query("SELECT * FROM item WHERE item_id IN ('$re->menuId') AND merchant_id IN ('$re->restoId')")->row();

        echo json_encode(['restodata' => $merchantDetail, 'mealdata' => $mealDetail, 'message' => "data ditemukan", "code" => 200]);
    }

    function getAllGopek()
    {
        $gopek = $this->db->query("SELECT * FROM item INNER JOIN merchant ON item.merchant_id=merchant.merchant_id LIMIT 15")->result_array();
        $data = [];
        foreach ($gopek as $g) {
            $extra = $g['extra'];
            $level = $g['level'];
            $varian = $g['varian'];

            $newExtra = [];
            $newLevel = [];
            $newVarian = [];

            if (!empty($extra) && $extra != '') {
                $pecahE = explode(';', $extra);

                foreach ($pecahE as $e) {

                    if ($e != '') {



                        $pecahE1 = explode(':', $e);

                        $dataExtra = [
                            'nama'  => $pecahE1[0],
                            'harga'  => $pecahE1[1],
                        ];

                        $newExtra[] = $dataExtra;
                    }
                }
            }

            if (!empty($level) && $level != '') {
                $pecahL = explode(';', $level);

                foreach ($pecahL as $e) {

                    if ($e != '') {



                        $pecahL1 = explode(':', $e);

                        $dataLevel = [
                            'nama'  => $pecahL1[0],
                            'harga'  => $pecahL1[1],
                        ];

                        $newLevel[] = $dataLevel;
                    }
                }
            }

            if (!empty($varian) && $varian != '') {
                $pecahV = explode(';', $varian);

                foreach ($pecahV as $e) {

                    if ($e != '') {



                        $pecahV1 = explode(':', $e);

                        $dataVarian = [
                            'nama'  => $pecahV1[0],
                            'harga'  => $pecahV1[1],
                        ];

                        $newVarian[] = $dataVarian;
                    }
                }
            }

            $g['extra'] = $newExtra;
            $g['level'] = $newLevel;
            $g['varian'] = $newVarian;
            $data[] = $g;
        }
        if (!empty($data)) {
            die(json_encode(['message' => 'data found', 'item_gopek' => $data, 'code' => 200]));
        } else {
            die(json_encode(['message' => 'data not found', 'code' => 404]));
        }
    }

    function getGopekItem()
    {
        $jenis = $this->input->get("jenis");
        $gopek = $this->db->query("SELECT * FROM item INNER JOIN merchant ON item.merchant_id=merchant.merchant_id WHERE jenis IN ('$jenis')")->result_array();
        $data = [];
        foreach ($gopek as $g) {
            $extra = $g['extra'];
            $level = $g['level'];
            $varian = $g['varian'];

            $newExtra = [];
            $newLevel = [];
            $newVarian = [];

            if (!empty($extra) && $extra != '') {
                $pecahE = explode(';', $extra);

                foreach ($pecahE as $e) {

                    if ($e != '') {



                        $pecahE1 = explode(':', $e);

                        $dataExtra = [
                            'nama'  => $pecahE1[0],
                            'harga'  => $pecahE1[1],
                        ];

                        $newExtra[] = $dataExtra;
                    }
                }
            }

            if (!empty($level) && $level != '') {
                $pecahL = explode(';', $level);

                foreach ($pecahL as $e) {

                    if ($e != '') {



                        $pecahL1 = explode(':', $e);

                        $dataLevel = [
                            'nama'  => $pecahL1[0],
                            'harga'  => $pecahL1[1],
                        ];

                        $newLevel[] = $dataLevel;
                    }
                }
            }

            if (!empty($varian) && $varian != '') {
                $pecahV = explode(';', $varian);

                foreach ($pecahV as $e) {

                    if ($e != '') {

                        $pecahV1 = explode(':', $e);

                        $dataVarian = [
                            'nama'  => $pecahV1[0],
                            'harga'  => $pecahV1[1],
                        ];

                        $newVarian[] = $dataVarian;
                    }
                }
            }

            $g['extra'] = $newExtra;
            $g['level'] = $newLevel;
            $g['varian'] = $newVarian;
            $data[] = $g;
        }
        if (!empty($data)) {
            die(json_encode(['message' => 'data found', 'item_gopek' => $data, 'code' => 200]));
        } else {
            die(json_encode(['message' => "data not found", 'code' => 404]));
        }
    }

    function searchGopek()
    {
        $term = $this->input->get("q");
        $gopek = $this->db->query("SELECT * FROM item INNER JOIN merchant ON item.merchant_id=merchant.merchant_id WHERE item_name LIKE '%$term%'")->result_array();
        $data = [];
        foreach ($gopek as $g) {
            $extra = $g['extra'];
            $level = $g['level'];
            $varian = $g['varian'];

            $newExtra = [];
            $newLevel = [];
            $newVarian = [];

            if (!empty($extra) && $extra != '') {
                $pecahE = explode(';', $extra);

                foreach ($pecahE as $e) {

                    if ($e != '') {



                        $pecahE1 = explode(':', $e);

                        $dataExtra = [
                            'nama'  => $pecahE1[0],
                            'harga'  => $pecahE1[1],
                        ];

                        $newExtra[] = $dataExtra;
                    }
                }
            }

            if (!empty($level) && $level != '') {
                $pecahL = explode(';', $level);

                foreach ($pecahL as $e) {

                    if ($e != '') {



                        $pecahL1 = explode(':', $e);

                        $dataLevel = [
                            'nama'  => $pecahL1[0],
                            'harga'  => $pecahL1[1],
                        ];

                        $newLevel[] = $dataLevel;
                    }
                }
            }

            if (!empty($varian) && $varian != '') {
                $pecahV = explode(';', $varian);

                foreach ($pecahV as $e) {

                    if ($e != '') {



                        $pecahV1 = explode(':', $e);

                        $dataVarian = [
                            'nama'  => $pecahV1[0],
                            'harga'  => $pecahV1[1],
                        ];

                        $newVarian[] = $dataVarian;
                    }
                }
            }

            $g['extra'] = $newExtra;
            $g['level'] = $newLevel;
            $g['varian'] = $newVarian;
            $data[] = $g;
        }
        if (!empty($data)) {
            die(json_encode(['message' => 'data found', 'item_gopek' => $data, 'code' => 200]));
        } else {
            die(json_encode(['message' => "data not found", 'code' => 404]));
        }
    }
}
