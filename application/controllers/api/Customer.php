<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once(dirname(__FILE__) . "/Baseapi.php");

class Customer extends Baseapi
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->model('Customer_model');
        $this->load->model('Driver_model');
        date_default_timezone_set(time_zone);
    }


    function home()
    {
        // header("Content-Type: application/json");

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);
        $slider = $this->Customer_model->sliderhome();
        $service = $this->Customer_model->fiturhome();
        $allfitur = $this->Customer_model->fiturhomeall();
        $rating = $this->Customer_model->ratinghome();
        $balance = $this->Customer_model->saldouser($dec_data->id);
        $app_settings = $this->Customer_model->get_settings();
        $news = $this->Customer_model->beritahome();
        $kategorymerchant = $this->Customer_model->kategorymerchant()->result();
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;
        $merchantpromo = $this->Customer_model->merchantpromo($long, $lat)->result();
        $merchantnearby = $this->Customer_model->merchantnearby($long, $lat);

        $m = $this->db->select("merchant.merchant_id , merchant.merchant_name , merchant.merchant_address , merchant.merchant_latitude , merchant.merchant_longitude , merchant.open_hour , merchant.close_hour ,
            merchant.merchant_desc , merchant.merchant_category , merchant.merchant_image , merchant.merchant_telephone_number , merchant.merchant_status , merchant.merchant_open_status, balance.balance,
            (6371 * acos(cos(radians($lat)) * cos(radians(merchant.merchant_latitude)) * cos(radians(merchant.merchant_longitude) - radians( $long)) + sin(radians($lat)) * sin( radians(merchant.merchant_latitude)))) AS distance
            ,(SELECT item.promo_status FROM item where merchant.merchant_id = item.merchant_id AND item_status = 1  ORDER BY promo_status DESC LIMIT 1) AS promo_status, service.minimum_distance, service.minimum_wallet")
            ->from('merchant')
            ->where('merchant.merchant_status = 1')
            // ->where('partner.partner_status = 1')
            ->where('balance.balance >= service.minimum_wallet')
            ->having("promo_status is not null")
            ->having("distance <= service.minimum_distance")
            ->order_by('distance ASC')
            ->join('service', 'merchant.service_id = service.service_id', 'left')
            // ->join('partner', 'merchant.merchant_id = partner.merchant_id', 'left')
            ->join('balance', 'partner.partner_id = balance.id_user', 'left')
            ->limit('4');
        $query =  $m->get_compiled_select();

        var_dump($query);

        // $allDataMealNearBy = array();
        // foreach($merchantnearby as $mn){
        //     $item = $this->db->query("SELECT * FROM item WHERE merchant_id IN ('".$mn['merchant_id']."')")->result();
        //     foreach($item as $k=>$i){
        //         if(count($allDataMealNearBy) <= 20){
        //             $allDataMealNearBy[] = $i;
        //         }else{
        //             break;
        //         }
        //     }
        //     // array_push($allDataMeal, $item);
        // }

        // // echo json_encode(['data' =>$allDataMealNearBy]);

        // //get makan disukai paling banyak

        // $allFavMeal = $this->db->query("SELECT * FROM item ORDER BY disuka DESC LIMIT 20")->result();
        // // echo json_encode($allFavMeal);

        // $condition = array(
        //     'phone_number' => $dec_data->phone_number,
        //     'status' => '1'
        // );
        // $cek_login = $this->Customer_model->get_data_pelanggan($condition);
        // $payu = $this->Customer_model->payusettings()->result();
        // foreach ($app_settings as $item) {
        //     if ($cek_login->num_rows() > 0) {
        //         $message = array(
        //             'code' => '200',
        //             'message' => 'success',
        //             'balance' => $balance->row('balance'),
        //             'currency' => $item['app_currency'],
        //             'currency_text' => $item['app_currency_text'],
        //             'app_aboutus' => $item['app_aboutus'],
        //             'app_contact' => $item['app_contact'],
        //             'app_website' => $item['app_website'],
        //             'stripe_active' => $item['stripe_active'],
        //             'stripe_publish' => $item['stripe_published_key'],
        //             'paypal_key' => $item['paypal_key'],
        //             'paypal_mode' => $item['paypal_mode'],
        //             'paypal_active' => $item['paypal_active'],
        //             'app_email' => $item['app_email'],
        //             'slider' => $slider,
        //             'service' => $service,
        //             'allfitur' => $allfitur,
        //             ///'ratinghome' => $rating,
        //             'beritahome' => $news,
        //             'kategorymerchanthome' => $kategorymerchant,
        //             // 'merchantnearby' => $merchantnearby,
        //             'nearby_meal' => $allDataMealNearBy,
        //             'fav_meal' => $allFavMeal,
        //             'merchantpromo' => $merchantpromo,
        //             'data' => $cek_login->result(),
        //             'payu' => $payu


        //         );
        //         unset($message['app_aboutus']);
        //         echo json_encode(['message' => $message, 'code' =>200]);
        //     } else {
        //         $message = array(
        //             'code' => '201',
        //             'message' => 'failed',
        //             'data' => []
        //         );
        //         $this->response($message, 201);
        //     }
        // }
    }

    function allmerchant()
    {
        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);


        $service = $dec_data->service;
        $kategorymerchant = $this->Customer_model->kategorymerchantbyfitur($service)->result();
        $long = $dec_data->longitude;
        $lat = $dec_data->latitude;

        $allmerchantnearby = $this->Customer_model->allmerchantnearby($long, $lat, $service)->result();
        $condition = array(
            'phone_number' => $dec_data->phone_number,
            'status' => '1'
        );
        $cek_login = $this->Customer_model->get_data_pelanggan($condition);

        if ($cek_login->num_rows() > 0) {
            $message = array(
                'code' => '200',
                'message' => 'success',

                'kategorymerchant' => $kategorymerchant,
                'allmerchantnearby' => $allmerchantnearby


            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => []
            );
            $this->response($message, 201);
        }
    }

    public function doRegister()
    {
        $re = file_get_contents("php://input");
        $json = json_decode($re, true);
        $json['password'] = sha1($json['password']);
        $ins = $this->crud->insertData("customer", $json);
        $dataIns = array(
            'id_user' => $json['id'],
            'balance' => 0
        );
        $insSaldo = $this->db->insert('balance', $dataIns);
        if ($ins > 0) {
            $this->success();
        } else {
            $this->internalError('Akun yang didaftarkan sudah ada');
        }
    }



    public function doLogin()
    {
        $json = file_get_contents("php://input");
        $data = json_decode($json);

        $user = $this->Customer_model->get_data_pelanggan(['phone_number' => $data->phone_number, 'password' => sha1($data->password)])->row();

        if (!empty($user)) {

            $reg_id = array(
                'token' => $data->token
            );

            $upd_regid = $this->Customer_model->edit_profile($reg_id, $data->phone_number);
            $this->success("data ditemukan", "user_data", $user);
        } else {
            $this->notFound();
        }
    }
}
