<?php

class Checkrecurring extends CI_Controller
{

    public function index()
    {
        $this->db->select('transaction.*');
        $this->db->select('transaction_langganan.*');

        $this->db->from('transaction');

        $this->db->join('transaction_langganan', 'transaction.id=transaction_langganan.transaction_id', 'left');

        $this->db->where('transaction.trip', 'Pulang Pergi');
        $this->db->or_where('transaction.is_recurring', '1');

        $dataTransaksi = $this->db->get()->result_array();

        if (!empty($dataTransaksi)) {
            foreach ($dataTransaksi as $row) {

                $is_recurring = $row['is_recurring'];
                $driver_id = $row['driver_id'];
                $customer_id = $row['customer_id'];

                $transaction_id = $row['id'];

                $status_config = '2';

                if ($is_recurring == '1') {
                    $start_date = $row['start_date'];
                    $end_date = $row['end_date'];


                    if (time() > strtotime($start_date and time() < strtotime($end_date))) {

                        if ($row['trip'] == 'Pulang Pergi') {

                            $dropoff = $row['dropoff_time'];

                            $titleD = 'Driver Jempoot';
                            $bodyD = 'Halo, Waktu silahkan jemput customer anda lagi, waktu dropoff nya adalah ' . $dropoff;

                            $titleC = 'Customer Jempoot';
                            $bodyC = 'Halo, Driver langganan anda akan menjemput anda sebentar lagi, tunggu ya!';


                            if (time() > strtotime($dropoff) - (10 * 60) and time() < strtotime($dropoff)) {
                                $this->db->set('status', $status_config);
                                $this->db->where('driver_id', $driver_id);
                                $updateConfig = $this->db->update('config_driver');
                            } else {
                                $updateConfig = false;
                            }
                        } else if ($row['trip'] == 'Sekali Jalan') {
                            $pickup = $row['pickup_time'];

                            $titleD = 'Driver Jempoot';
                            $bodyD = 'Halo, Waktu silahkan jemput customer anda lagi, waktu pickup nya adalah ' . $pickup;

                            $titleC = 'Customer Jempoot';
                            $bodyC = 'Halo, Driver langganan anda akan menjemput anda sebentar lagi, tunggu ya!';


                            if (time() > strtotime($pickup) - (10 * 60) and time() < strtotime($pickup)) {
                                $this->db->set('status', $status_config);
                                $this->db->where('driver_id', $driver_id);
                                $updateConfig = $this->db->update('config_driver');
                            } else {
                                $updateConfig = false;
                            }
                        } else {
                            $updateConfig = false;
                        }
                    } else {
                        $updateConfig = false;
                    }
                } else {
                    $dateNow = date('Y-m-d');
                    $order_time = date('Y-m-d', strtotime($row['order_time']));

                    if ($dateNow == $order_time) {


                        if ($row['trip'] == 'Pulang Pergi') {

                            $status_config = '2';

                            $dropoff = $row['dropoff_time'];

                            $titleD = 'Driver Jempoot';
                            $bodyD = 'Halo, Waktu silahkan jemput customer anda lagi, waktu dropoff nya adalah ' . $dropoff;

                            $titleC = 'Customer Jempoot';
                            $bodyC = 'Halo, Driver anda akan menjemput anda sebentar lagi, tunggu ya!';


                            if (time() > strtotime($dropoff) - (10 * 60) and time() < strtotime($dropoff)) {
                                $this->db->set('status', $status_config);
                                $this->db->where('driver_id', $driver_id);
                                $updateConfig = $this->db->update('config_driver');
                            } else {
                                $updateConfig = false;
                            }
                        } else {
                            $updateConfig = false;
                        }
                    } else {
                        $updateConfig = false;
                    }
                }

                if ($updateConfig) {

                    $dataTrx = $this->Transactionapi_model->getTransaksiById($transaction_id);

                    $dataTrx['type'] = 'transaksi';

                    $dataFCMD = [
                        'title'             => $titleD,
                        'body'              => $bodyD,
                        'type'              => 'transaksi',
                        'data'              => $dataTrx
                    ];


                    notif_fcm($dataFCMD, $driver_id);

                    $dataFCMC = [
                        'title'             => $titleC,
                        'body'              => $bodyC,
                        'type'              => 'transaksi',
                        'data'              => $dataTrx
                    ];

                    notif_fcm($dataFCMC, $customer_id);
                }
            }
        }
    }
}
