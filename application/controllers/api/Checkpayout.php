<?php

class Checkpayout extends CI_Controller
{

    public function index()
    {
        $this->db->select('*');

        $this->db->from('tb_wallet_payout');

        $this->db->where('status_payout !=', 'completed');
        $this->db->or_where('status_payout !=', 'rejected');
        $this->db->or_where('status_payout !=', 'failed');

        $dataPayout = $this->db->get()->result_array();

        if (!empty($dataPayout)) {
            foreach ($dataPayout as $row) {

                $reference_no = $row['reference_no'];

                $method = 'GET';
                $url = 'iris/api/v1/payouts/' . $reference_no;
                $type = 'query';


                $response = restclientapprover($method, $url, $type);

                $status = $response['status'];

                $this->db->set('status_payout', $status);
                $this->db->where('reference_no', $reference_no);
                $this->db->update('tb_wallet_payout');
            }
        }
    }
}
