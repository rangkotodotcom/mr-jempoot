<?php


class History extends CI_Controller{
    function getHistory(){
        $id = $this->input->get('service');
        $userid = $this->input->get("userid");
        $status = $this->input->get('status');
        
        $history = $this->db->query("SELECT * FROM transaction INNER JOIN transaction_history ON transaction.id=transaction_history.transaction_id
        WHERE service_order IN ('$id') AND customer_id IN ('$userid') AND status IN ('$status')
        ")->result_array();
        
        $data = [];
        foreach($history as $d){
            $d['driver'] = $this->db->query("SELECT * FROM driver WHERE id IN ('".$d['driver_id']."')")->row();
            $d['distance'] =(int) str_replace(".", "", $d['distance']);
            $data[] = $d;
        }
        if(!empty($data)){
            die(json_encode(['history' => $data, 'message' => "data found", 'code' =>200]));
        }else{
            die(json_encode(['message' => 'data not found', 'code' => 404]));
        }
    }
}