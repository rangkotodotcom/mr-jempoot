<?php

//'tes' => number_format(200 / 100, 2, ",", "."),

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Gokidz extends REST_Controller
{
    function langganan(){
         if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }
        
        
        $re = file_get_contents("php://input");
        $json = json_decode($re, true);
        
        if(!empty($json)){
            $ins = $this->db->insert("langganan", $json);
            if($ins){
                echo json_encode(['message' => 'Langganan berhasil', 'code' => 200]);
            }else{
                echo json_encode(['message' => 'Kesalahan server. Langganan gagal', 'code' => 500]);
                
            }
        }
    }
    
}