<?php

//'tes' => number_format(200 / 100, 2, ",", "."),

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Transactionapi extends REST_Controller

{


    public function __construct()

    {

        parent::__construct();

        $this->load->helper("url");

        $this->load->database();

        $this->load->model('Transactionapi_model', 'trx');

        $this->load->model('Appsettings_model', 'appset');

        $this->load->model('Service_model', 'service');

        $this->load->model('Walletapi_model', 'wlt');

        $this->load->model('File_model', 'file');

        date_default_timezone_set(time_zone);
    }



    function index_get()

    {

        $this->response("Api for ouride!", 200);
    }



    function service_get()

    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }



        $allService = $this->trx->getAllservice();

        if (!empty($allService)) {
            foreach ($allService as $row => $value) {
                if ($value['active'] == '1') {

                    $newAllservice[] = [
                        'service_id'        => $value['service_id'],
                        'service'           => $value['service'],
                        'icon'              => $value['icon'],
                        'cost'              => $value['cost'],
                        'minimum_cost'      => $value['minimum_cost'],
                        'minimum_distance'  => $value['minimum_distance'],
                        'maks_distance'     => $value['maks_distance'],
                        'minimum_wallet'    => $value['minimum_wallet'],
                        'commision'         => $value['commision'],
                        'cost_desc'         => $value['cost_desc'],
                        'driver_job'        => $value['driver_job'],
                        'description'       => $value['description'],
                        'home'              => $value['home']
                    ];
                }
            }
        } else {
            $newAllservice = [];
        }


        $message = array(

            'code' => '200',

            'message' => 'found',

            'data' => $newAllservice

        );


        $this->response($message, 200);
    }



    function transaction_get($id_user, $service_id, $by = null, $kode = null)
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $kodeU = substr($id_user, 0, 1);

        if ($kodeU == 'D') {
            $getTrx = $this->trx->getTrxDriver($id_user, $service_id, $by, $kode);
        } else if ($kodeU == 'C') {
            $getTrx = $this->trx->getTrxCustomer($id_user, $service_id, $by, $kode);
        } else {
            $getTrx = [];
        }




        $message = array(

            'code' => '200',

            'message' => 'found',

            'data' => $getTrx

        );

        $this->response($message, 200);
    }


    function statustrx_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $savestatustrx = $this->trx->savestatustrx($this->post());

        $driver_id = $this->post('driver_id');
        $transaction_id = $this->post('transaction_id');
        $keterangan = $this->post('keterangan');

        if ($savestatustrx) {

            $data = $this->trx->getTrxDriver($driver_id, null, 'id', $transaction_id);

            $id_user = $data[0]['customer_id'];

            $service_order = $data[0]['service_order'];

            if ($this->post('status') == '3') {

                if ($service_order == '21') {

                    $dataFCM = [
                        'title' => 'Merchant Jempoot',
                        'body'  => 'Halo, orderan anda sudah diterima oleh driver',
                        'type'  => '202'
                    ];
                } else {
                    $dataFCM = [
                        'title' => 'Customer Jempoot',
                        'body'  => 'Halo, orderan anda sudah diterima, driver kami sedang menuju ke tempat anda',
                        'type'  => 'transaksi',
                        'data'  => $data
                    ];
                }

                notif_fcm($dataFCM, $id_user);

                if ($keterangan == '') {
                    $body = 'Transaksi dengan kode ' . $data[0]['kode_trx'] . ' berhasil diterima, silahkan lanjutkan transaksinya';
                } else {
                    $body = 'Transaksi dengan kode ' . $data[0]['kode_trx'] . ' berstatus ' . $keterangan;
                }

                $dataFCMD = [
                    'title' => 'Driver Jempoot',
                    'body'  => $body,
                    'type'  => 'transaksi',
                    'data'  => $data
                ];

                notif_fcm($dataFCMD, $driver_id);
            } else if ($this->post('status') == '4') {
                if ($service_order == '21') {

                    $dataFCM = [
                        'title' => 'Merchant Jempoot',
                        'body'  => 'Halo, orderan anda sudah sampai. Selamat makan!',
                        'type'  => '205'
                    ];
                } else {
                    $dataFCM = [
                        'title' => 'Customer Jempoot',
                        'body'  => 'Halo, orderan anda sudah selesai. Semoga hari anda menyenangkan, jangan lupa order kami lagi ya!',
                        'type'  => 'transaksi',
                        'data'  => $data
                    ];
                }
                notif_fcm($dataFCM, $id_user);

                $dataFCMD = [
                    'title' => 'Driver Jempoot',
                    'body'  => 'Transaksi dengan kode ' . $data[0]['kode_trx'] . ' sudah berhasil',
                    'type'  => 'transaksi',
                    'data'  => $data
                ];

                notif_fcm($dataFCMD, $driver_id);
            } else if ($this->post('status') == '5') {
                $dataFCM = [
                    'title' => 'Pembatalan transaksi',
                    'body'  => 'Halo, transaksi anda dengan kode ' . $data[0]['kode_trx'] . ' dibatalkan',
                    'type'  => '205'
                ];

                notif_fcm($dataFCM, $id_user);

                notif_fcm($dataFCM, $driver_id);
            }


            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => $data

            );
        } else {
            $message = array(

                'code' => '400',

                'message' => 'failed',

                'data' => ''

            );
        }


        $this->response($message, 200);
    }


    function listbank_get()

    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }



        $allListBank = $this->wlt->getalllistbank();


        $message = array(

            'code' => '200',

            'message' => 'found',

            'data' => $allListBank

        );


        $this->response($message, 200);
    }


    function notif_get($id_user)

    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }



        $allnotif = $this->trx->getnotif($id_user);


        $message = array(

            'code' => '200',

            'message' => 'found',

            'data' => $allnotif

        );


        $this->response($message, 200);
    }


    function notif_post()

    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }



        $savenotif = $this->trx->savenotif($this->post());

        if ($savenotif) {
            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => ''

            );
        } else {
            $message = array(

                'code' => '400',

                'message' => 'failed',

                'data' => ''

            );
        }


        $this->response($message, 200);
    }


    function notif_delete($id_user)

    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }



        $clearallnotif = $this->trx->clearallnotif($id_user);

        if ($clearallnotif) {
            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => ''

            );
        } else {
            $message = array(

                'code' => '400',

                'message' => 'failed',

                'data' => ''

            );
        }


        $this->response($message, 200);
    }


    public function app_cost_get()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }



        $app = $this->appset->getappbyid();


        if ($app) {

            $appcost = [
                'app_currency'      => $app['app_currency'],
                'app_currency_text' => $app['app_currency_text'],
                'app_cost'          => $app['app_cost']
            ];

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => $appcost

            );
        } else {
            $message = array(

                'code' => '400',

                'message' => 'failed',

                'data' => ''

            );
        }


        $this->response($message, 200);
    }


    function val_cus_trans_post()

    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }



        $savevalcus = $this->trx->savevalcus($this->post());

        $driver_id = $this->post('driver_id');
        $customer_id = $this->post('customer_id');
        $transaction_id = $this->post('transaction_id');

        if ($savevalcus) {

            $data = $this->trx->getTrxDriver($driver_id, null, 'id', $transaction_id);

            $dataFCMC = [
                'title' => 'Penilaian Customer',
                'body'  => 'Driver menilai anda untuk transaksi dengan kode ' . $data[0]['kode_trx'],
                'type'  => 'nilaicustomer'
            ];
            notif_fcm($dataFCMC, $customer_id);

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => ''

            );
        } else {
            $message = array(

                'code' => '400',

                'message' => 'failed',

                'data' => ''

            );
        }


        $this->response($message, 200);
    }


    function ver_code_merchant_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }

        $vercodmer = $this->trx->vercodmer($this->post());

        $transaction_id = $this->post('transaction_id');
        $driver_id = $this->post('driver_id');


        if ($vercodmer) {


            $data = $this->trx->getTrxDriver($driver_id, null, 'id', $transaction_id);

            $id_user = $data[0]['customer_id'];

            $dataFCM = [
                'title' => 'Merchant Jempoot',
                'body'  => 'Halo, driver sudah sampai di merchant nih',
                'type'  => '203'
            ];

            notif_fcm($dataFCM, $id_user);

            $dataFCMD = [
                'title' => 'Driver Jempoot',
                'body'  => 'Verifikasi kode berhasil, silahkan lanjutkan transaksi',
                'type'  => 'transaksi',
                'data'  => $data[0],
            ];

            notif_fcm($dataFCMD, $driver_id);

            $merchant_id = $data[0]['merchant_id'];

            $dataFCMM = [
                'title' => 'Verifikasi Kode',
                'body'  => 'Hore, Kode driver sudah cocok dengan kode transaksi di merchant',
                'type'  => 'transaksi',
                'data'  => $data[0]
            ];

            notif_fcm($dataFCMM, $merchant_id);

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => $data

            );
        } else {
            $message = array(

                'code' => '400',

                'message' => 'failed',

                'data' => ''

            );
        }


        $this->response($message, 200);
    }


    function pay_to_merchant_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $data_req = array(
            'driver_id' => $dec_data->id,
            'transaction_id' => $dec_data->transaction_id
        );

        $data_tr = array(
            'driver_id' => $dec_data->id,
            'id' => $dec_data->transaction_id
        );

        $finish_transaksi = $this->trx->PayMerchant($data_req, $data_tr, $dec_data->price_to_merchant);
        if ($finish_transaksi['status']) {


            $data = $this->trx->getTrxDriver($dec_data->id, null, 'id', $dec_data->transaction_id);

            $merchant_id = $data[0]['merchant_id'];

            $dataFCM = [
                'title' => 'Pembayaran pesanan',
                'body'  => 'Hore, pesanan dengan kode transaksi ' . $data[0]['kode_trx'] . ' berhasil dibayar',
                'type'  => 'transaksi',
                'data'  => $data[0]
            ];

            notif_fcm($dataFCM, $merchant_id);
            notif_fcm($dataFCM, $dec_data->id);


            $message = array(
                'code' => 200,
                'message' => 'success pay to merchant',
                'data' => $data,
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => 400,
                'message' => 'fail',
                'data' => $finish_transaksi['message']
            );
            $this->response($message, 200);
        }
    }


    function pay_to_jempoot_post()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $data_req = array(
            'driver_id' => $dec_data->id,
            'transaction_id' => $dec_data->transaction_id
        );

        $data_tr = array(
            'driver_id' => $dec_data->id,
            'id' => $dec_data->transaction_id
        );

        $finish_transaksi = $this->trx->PayJempoot($data_req, $data_tr, $dec_data->price_to_jempoot);
        if ($finish_transaksi['status']) {

            $data = $this->trx->getTrxDriver($dec_data->id, null, 'id', $dec_data->transaction_id);

            $dataFCM = [
                'title' => 'Pembayaran biaya platform',
                'body'  => 'Hore, biaya platform untuk kode transaksi ' . $data[0]['kode_trx'] . ' berhasil dibayar',
                'type'  => 'transaksi',
                'data'  => $data[0]
            ];

            notif_fcm($dataFCM, $dec_data->id);

            $message = array(
                'code' => 200,
                'message' => 'success pay to jempoot',
                'data' => $data,
            );
            $this->response($message, 200);
        } else {
            $message = array(

                'code' => 400,
                'message' => 'fail',
                'data' => $finish_transaksi['message']
            );
            $this->response($message, 200);
        }
    }


    public function send_struk_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $imagestruk = $dec_data->struk;
        $namafotostruk = time() . '-' . rand(0, 99999) . ".jpg";
        $pathstruk = "images/photofile/struk/" . $namafotostruk;
        file_put_contents($pathstruk, base64_decode($imagestruk));

        $this->file->compress($pathstruk, 300, 400);


        $data_berkas = [
            'id_struk'          => $this->uuid->v4(),
            'transaction_id'    => $dec_data->transaction_id,
            'struk'             => $namafotostruk
        ];


        $struk = $this->trx->saveStruk($data_berkas);

        if ($struk) {

            $data = $this->trx->getTrxDriver($dec_data->driver_id, null, 'id', $dec_data->transaction_id);
            $customer_id = $data[0]['customer_id'];


            $dataFCM = [
                'title' => 'Struk Pembayaran',
                'body'  => 'Halo, driver mengirim struck pembayaran pesanan anda dengan kode transaksi ' . $data[0]['kode_trx'],
                'type'  => 'transaksi',
                'data'  => $data[0]
            ];

            notif_fcm($dataFCM, $customer_id);

            $dataFCMD = [
                'title' => 'Struk Pembayaran',
                'body'  => 'Halo, struck pembayaran pesanan  dengan kode transaksi ' . $data[0]['kode_trx'] . ' berhasil dikirim ke customer',
                'type'  => 'transaksi',
                'data'  => $data[0]
            ];

            notif_fcm($dataFCMD, $dec_data->driver_id);

            $message = array(
                'code' => '200',
                'message' => 'success',
                'data' => $data
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => ''
            );
            $this->response($message, 201);
        }
    }


    public function receive_packet_post()
    {

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            return false;
        }

        $data = file_get_contents("php://input");
        $dec_data = json_decode($data);

        $imagebefore = $dec_data->before;

        if ($imagebefore != '') {


            $namafotobefore = time() . '-' . rand(0, 99999) . ".jpg";
            $pathbefore = "images/photofile/packet/" . $namafotobefore;
            file_put_contents($pathbefore, base64_decode($imagebefore));

            $this->file->compress($pathbefore, 400, 400);
        } else {
            $namafotobefore = '';
        }


        $imageafter = $dec_data->after;

        if ($imageafter != '') {


            $namafotoafter = time() . '-' . rand(0, 99999) . ".jpg";
            $pathafter = "images/photofile/packet/" . $namafotoafter;
            file_put_contents($pathafter, base64_decode($imageafter));

            $this->file->compress($pathafter, 400, 400);
        } else {
            $namafotoafter = '';
        }

        $data_berkas = [
            'id_receive'        => $this->uuid->v4(),
            'transaction_id'    => $dec_data->transaction_id,
            'image_before'      => $namafotobefore,
            'image_after'       => $namafotoafter,
        ];


        $saveReceivePacket = $this->trx->saveReceivePacket($data_berkas);

        if ($saveReceivePacket) {

            $receivePacket = $this->trx->getReceivepacket($dec_data->transaction_id);


            $data = $this->trx->getTrxDriver($dec_data->driver_id, null, 'id', $dec_data->transaction_id);
            $customer_id = $data[0]['customer_id'];

            $dataFCM = [
                'title' => 'Pengiriman Paket',
                'body'  => 'Halo, driver mengirim foto paket yang anda kirim dengan kode transaksi ' . $data[0]['kode_trx'],
                'type'  => 'transaksi',
                'data'  => $data[0]
            ];

            notif_fcm($dataFCM, $customer_id);

            $dataFCMD = [
                'title' => 'Pengiriman Paket',
                'body'  => 'Halo, foto paket customer yang anda kirim dengan kode transaksi ' . $data[0]['kode_trx'] . ' berhasil dikirim ke customer',
                'type'  => 'transaksi',
                'data'  => $data[0]
            ];

            notif_fcm($dataFCMD, $dec_data->driver_id);

            $message = array(
                'code' => '200',
                'message' => 'success',
                'data' => $data,
                'receive_packet' => [
                    'data'  => $receivePacket,
                    'url'   => base_url('images/photofile/packet/')
                ]
            );
            $this->response($message, 200);
        } else {
            $message = array(
                'code' => '201',
                'message' => 'failed',
                'data' => ''
            );
            $this->response($message, 201);
        }
    }


    public function performance_driver_get($driver_id)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }



        $data['rating'] = $this->trx->getPerformanceDriver($driver_id, 'rate');
        $data['detail'] = $this->trx->getPerformanceDriver($driver_id, 'detail');
        $data['count_rate']['rate_1'] = $this->trx->getPerformanceDriver($driver_id, 'rate', '1');
        $data['count_rate']['rate_2'] = $this->trx->getPerformanceDriver($driver_id, 'rate', '2');
        $data['count_rate']['rate_3'] = $this->trx->getPerformanceDriver($driver_id, 'rate', '3');
        $data['count_rate']['rate_4'] = $this->trx->getPerformanceDriver($driver_id, 'rate', '4');
        $data['count_rate']['rate_5'] = $this->trx->getPerformanceDriver($driver_id, 'rate', '5');


        if ($data) {

            $message = array(

                'code' => '200',

                'message' => 'success',

                'data' => $data

            );
        } else {
            $message = array(

                'code' => '400',

                'message' => 'failed',

                'data' => ''

            );
        }


        $this->response($message, 200);
    }


    public function tambahJadwal()
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {

            header("WWW-Authenticate: Basic realm=\"Private Area\"");

            header("HTTP/1.0 401 Unauthorized");

            return false;
        }


        $re = file_get_contents("php://input");
        $json = json_decode($re, true);

        if (!empty($json)) {
            $ins = $this->db->insert("jadwal_pengiriman", $re);
            if ($ins) {
                echo json_encode(['message' => 'jadwal berhasil dibuat', 'code' => 200]);
            } else {
                echo json_encode(['message' => 'Kesalahan server. jadwal gagal ditambahkan', 'code' => 500]);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////





}
