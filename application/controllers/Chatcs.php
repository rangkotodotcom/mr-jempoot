<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Chatcs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('user_name') == NULL && $this->session->userdata('password') == NULL) {
            redirect(base_url() . "login");
        }

        $this->load->library('form_validation');
        $this->load->model('Chatcs_model', 'Mchatcs');
        $this->load->model('Appsettings_model', 'appset');
        $this->load->model('File_model', 'file');
    }

    public function index()
    {
        $getview['view'] = 'familyrelation';
        $getview['menu'] = $this->appset->getMenuAdmin();

        $chatcs = $this->Mchatcs->getChat();

        if ($chatcs['status']) {
            $data['chatcs'] = $chatcs['data'];
        } else {
            $data['chatcs'] = [];
        }

        $this->load->view('includes/header', $getview);
        $this->load->view('chatcs/view', $data);
        $this->load->view('includes/footer', $getview);
    }

    public function detail($id_message = null)
    {
        if (!isset($id_message)) redirect(base_url('chatcs'));

        $getview['view'] = 'familyrelation';
        $getview['menu'] = $this->appset->getMenuAdmin();


        $chatcs = $this->Mchatcs->getChat($id_message);

        if ($chatcs['status']) {
            $data['chatcs'] = $chatcs['data'];
            $id_user = $chatcs['data']['id_user'];

            $kode = substr($id_user, 0, 1);

            if ($kode == 'D') {
                $index_foto = 'foto_driver';
                $url_foto = '/images/driverphoto/';
            } else if ($kode == 'C') {
                $index_foto = 'foto_customer';
                $url_foto = '/images/customer/';
            } else if ($kode == 'M') {
                $index_foto = 'foto_merchant';
                $url_foto = '/images/merchant/';
            } else {
                $index_foto = '';
            }
        } else {
            $data['chatcs'] = [];
        }

        $data['index_foto'] = $index_foto;
        $data['url_foto'] = $url_foto;

        $this->load->view('includes/header', $getview);
        $this->load->view('chatcs/detail', $data);
        $this->load->view('includes/footer', $getview);
    }


    public function send()
    {
        $post = $this->input->post();
        $id_message = $post['id_message'];
        $id_user = $post['id_user'];
        $message = $post['message'];

        $namafoto = '';
        $textmessage = '';

        if ($_FILES['photo']['name'] != '') {
            $config['upload_path']     = './images/chatcs/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']         = 100;
            $config['file_name']     = 'name';
            $config['encrypt_name']     = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('photo')) {
                $namafoto .= html_escape($this->upload->data('file_name'));
                $this->file->compress('./images/chatcs/' . $namafoto, 600, 400);
            }
        }

        if ($message != '') {
            $textmessage .= $message;
        }

        $dataSendChat = [
            'id_message'    => $id_message,
            'id_user'       => 'Admin',
            'message'       => $textmessage,
            'photo'         => $namafoto
        ];

        $saveSendChat = $this->Mchatcs->sendChat($dataSendChat);

        if ($saveSendChat['status']) {
            $dataTrx = [
                [
                    'message'   => $message,
                    'photo'     => base_url('/images/chatcs/' . $namafoto)
                ]
            ];

            if($message != ''){
                $body = $message;
            }

            if($namafoto != ''){
                $body = base_url('/images/chatcs/' . $namafoto);
            }

            $dataFCM = [
                'title' => 'CS Jempoot',
                'body'  => $body,
                'data'  => $dataTrx,
                'type'  => 'chatcs'
            ];

            notif_fcm($dataFCM, $id_user);
            $this->session->set_flashdata('success', 'pesan berhasil dikirim');
            redirect(base_url('chatcs/detail/' . $id_message));
        } else {
            $this->session->set_flashdata('danger', 'pesan gagal dikirim');
        }
    }
}
