<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Webview extends CI_Controller
{

    public function index($date_today, $merchant_id)
    {
        $data['merchant_id'] = $merchant_id;
        $data['date_today'] = $date_today;
        $this->load->view('webview/view', $data);
    }


    public function faq()
    {

        $this->load->model('Faq_model', 'faq');
        $this->load->model('Appsettings_model', 'apps');

        $app = $this->apps->getappbyid();


        $data['title'] = 'Faq | ' . $app['app_name'];
        $data['faq'] = $this->faq->getAll();
        $this->load->view('webview/faq', $data);
    }
}
