<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('notif_fcm')) {

    function notif_fcm($dataFCM, $id_user, $dataTrx = null)
    {
        $kode = substr($id_user, 0, 1);

        $dataFCM['sound'] = 'default';

        $CI = &get_instance();

        if ($kode == 'M') {

            $index = 'merchant_token';

            $CI->db->select('partner.*');
            $CI->db->select('merchant.merchant_token');
            $CI->db->join('merchant', 'partner.merchant_id = merchant.merchant_id', 'left');
            $CI->db->where('partner.partner_id', $id_user);
            $data = $CI->db->get('partner')->row_array();
        } else if ($kode == 'C') {

            $index = 'token';

            $CI->db->select('token');
            $CI->db->where('id', $id_user);
            $data = $CI->db->get('customer')->row_array();
        } else if ($kode == 'D') {


            $index = 'reg_id';

            $CI->db->select('reg_id');
            $CI->db->where('id', $id_user);
            $data = $CI->db->get('driver')->row_array();
        } else {
            $index = 'merchant_token';

            $CI->db->select('merchant_token');
            $CI->db->where('merchant_id', $id_user);
            $data = $CI->db->get('merchant')->row_array();
        }

        if (!empty($data)) {
            $token = $data[$index];
        } else {
            $token = '';
        }


        if ($token == '') {
            return false;
        } else {

            $senderdata = array(
                'to'    => $token,
                'data'  => $dataFCM

            );

            $headers = [
                "Content-Type: application/json",
                "Authorization:key=" . keyfcm
            ];
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($senderdata));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($response) {

                if ($dataFCM['type'] != 'chatcs') {
                    $title = $dataFCM['title'];
                    $body = $dataFCM['body'];

                    $dataNotif = [
                        'id_notif'      => $CI->uuid->v4(),
                        'id_user'       => $id_user,
                        'title_notif'   => $title,
                        'text_notif'    => $body
                    ];

                    return $CI->db->insert('tb_notif', $dataNotif);
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }
    }
}
