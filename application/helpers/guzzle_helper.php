<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use GuzzleHttp\Client;

if (!function_exists('restclientapprover')) {

    function restclientapprover($method, $url, $type, $data = null)
    {
        $Mapp = model2model('appsettings_model');

        $app = $Mapp->getappbyid();

        if ($app['midtrans_payout_mode'] == 'production') {
            $baseUrlPayout = $app['midtrans_payout_baseurl'];
            $APIKeyPayout = $app['midtrans_payout_approver_apikey'];
            $MerchantKeyPayout = $app['midtrans_payout_merchantkey'];
        } else {
            $baseUrlPayout = $app['midtrans_payout_baseurl_sb'];
            $APIKeyPayout = $app['midtrans_payout_approver_apikey_sb'];
            $MerchantKeyPayout = $app['midtrans_payout_merchantkey_sb'];
        }

        $_client = new Client([
            'base_uri' => $baseUrlPayout,
            'timeout'   => 100
        ]);

        if ($type == 'query') {

            $senddata = [
                'headers'       => [
                    'Authorization' => 'Basic ' . base64_encode($APIKeyPayout),
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json'
                ],
                'http_errors'   => false,
                'query'         => $data
            ];
        } else if ($type == 'raw') {

            $senddata = [
                'headers'       => [
                    'Authorization' => 'Basic ' . base64_encode($APIKeyPayout),
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json'
                ],
                'http_errors'   => false,
                'body'          => $data
            ];
        }

        $debug = ['debug' => true];

        $response = $_client->request($method, $url, $senddata, $debug);

        $result = json_decode($response->getBody()->getContents(), true);

        return $result;
    }
}

if (!function_exists('restclientcreator')) {

    function restclientcreator($method, $url, $type, $data = null)
    {
        $Mapp = model2model('appsettings_model');

        $app = $Mapp->getappbyid();

        if ($app['midtrans_payout_mode'] == 'production') {
            $baseUrlPayout = $app['midtrans_payout_baseurl'];
            $APIKeyPayout = $app['midtrans_payout_creator_apikey'];
            $MerchantKeyPayout = $app['midtrans_payout_merchantkey'];
        } else {
            $baseUrlPayout = $app['midtrans_payout_baseurl_sb'];
            $APIKeyPayout = $app['midtrans_payout_creator_apikey_sb'];
            $MerchantKeyPayout = $app['midtrans_payout_merchantkey_sb'];
        }

        $_client = new Client([
            'base_uri' => $baseUrlPayout,
            'timeout'   => 100
        ]);

        if ($type == 'query') {

            $senddata = [
                'headers'       => [
                    'Authorization' => 'Basic ' . base64_encode($APIKeyPayout),
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json'
                ],
                'http_errors'   => false,
                'query'         => $data
            ];
        } else if ($type == 'raw') {

            $senddata = [
                'headers'       => [
                    'Authorization' => 'Basic ' . base64_encode($APIKeyPayout),
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json'
                ],
                'http_errors'   => false,
                'body'          => $data
            ];
        }

        $debug = ['debug' => true];

        $response = $_client->request($method, $url, $senddata, $debug);

        $result = json_decode($response->getBody()->getContents(), true);

        return $result;
    }
}
