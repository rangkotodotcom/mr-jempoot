<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Data list view starts -->
            <section id="data-thumb-view" class="data-thumb-view-header">

                <div class="card-header">
                    <h4>Cost Send <span><a class="btn btn-success float-right mb-1 text-white" href="<?= base_url(); ?>service/addcostsendview">
                                <i class="feather icon-plus mr-1"></i>Add Cost Send</a></span></h4>
                </div>
                <!-- merchant category Table starts -->
                <div class="table-responsive">
                    <table class="table data-thumb-view">
                        <thead>
                            <tr>
                            <th>No</th>
                            <th>Weight</th>
                            <th>Unit</th>
                            <th>Cost</th>
                            <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1;
                                foreach ($costsend as $mc) { ?>
                                    <tr>
                                        <h1 id="idkat<?= $i ?>" style="display:none;"><?= $mc['id_cost_send']; ?></h1>
                                        <td><?= $i ?></td>
                                        <td id="namkat<?= $i ?>"><?= $mc['weight']; ?></td>
                                        <td id="service<?= $i ?>"><?= $mc['unit']; ?></td>
                                        <td>
                                            <?= $currency ?>
                                            <?= number_format($mc['cost'], 2, ".", "."); ?>
                                        </td>
                                        <td>
                                        <span class="mr-1">
                                                                <a href="<?= base_url(); ?>service/editcostsendview/<?= $mc['id_cost_send']; ?>">
                                                                    <i class="feather icon-edit text-info"></i>
                                                                </a>
                                                            </span>
                                                            <span class="action-delete mr-1">
                                                                <a href="<?= base_url(); ?>service/deletecostsend/<?= $mc['id_cost_send']; ?>" onclick="return confirm ('are you sure want to delete?')">
                                                                    <i class="feather icon-trash text-danger"></i>
                                                                </a>
                                                            </span>
                                        </td>
                                    </tr>

                                <?php $i++;
                                } ?>

                        </tbody>
                    </table>
                </div>
                <!-- merchant category data Table ends -->

                
            </section>
            <!-- Data list view end -->
        </div>
    </div>
</div>
<!-- END: Content-->