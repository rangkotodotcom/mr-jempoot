<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- start add merchant category -->
            <div class="row match-height justify-content-center">
                <div class="col-md-8 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Add Cost Send</h4>
                        </div>
                        <section id="basic-vertical-layouts">
                            <div class="card-content">
                                <div class="card-body">
                                    <?= form_open_multipart('service/addcostsend'); ?>
                                    <form class="form form-vertical">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12 form-group">
                                                    <label for="category_name">Weight</label>
                                                    <input type="text" class="form-control" name="weight" placeholder="enter weight" required>
                                                </div>
                                                
                                                <div class="col-12 form-group">
                                                    <label for="category_name">Satuan</label>
                                                    <input type="text" class="form-control" name="unit" placeholder="exp (Kg)" required>
                                                </div>
                                                
                                                <div class="col-12 form-group">
                                                    <label for="category_name">Cost</label>
                                                    <input type="text" pattern="^\d+(\.|\,)\d{2}$" data-type="currency" class="form-control" name="cost" placeholder="" required>
                                                </div>
                                                <!-- end of add merchant category form -->

                                                <div class="col-12">
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <?= form_close(); ?>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            </section>

            <!-- end of add merchant category -->
        </div>
    </div>
</div>
<!-- END: Content-->