<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- start form add slider -->

            <section id="basic-vertical-layouts">
                <div class="row match-height justify-content-center">
                    <div class="col-md-6 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Add Souvenir</h4>
                            </div>
                            <br>
                            <div class="card-content">
                                <div class="card-body">
                                    <?= form_open_multipart('additional/addsouvenirdata'); ?>
                                    <form class="form form-vertical">
                                        <div class="form-body">
                                            <div class="row">

                                            <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="icon">Gambar</label>
                                                        <input type="file" name="img_souvenir" class="dropify" data-max-file-size="100kb" />
                                                    </div>
                                                </div>

                                                <div class="col-12 form-group">
                                                    <label for="newstitle">Name</label>
                                                    <input type="text" class="form-control" id="newstitle" name="name_souvenir" required>
                                                </div>
                                                
                                                <div class="col-12 form-group">
                                                    <label for="newstitle">Poin Buy</label>
                                                    <input type="text" class="form-control" id="newstitle" name="poin_buy" placeholder="ex 1000" required>
                                                </div>

                                                <div class="col-12 form-group">
                                                    <label for="newscategory">Status</label>
                                                    <select class="select2 form-group" name="status_souvenir" style="width:100%">
                                                        <option value="0">Nonactive</option>
                                                        <option value="1">Active</option>
                                                    </select>
                                                </div>

                                                <div class="col-12">
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <?= form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- end of form add slider -->
        </div>
    </div>
</div>
<!-- END: Content-->