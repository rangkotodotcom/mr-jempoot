<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Data list view starts -->
            <section id="data-thumb-view" class="data-thumb-view-header">

                <div class="card-header">
                    <h4>Item Cuisine <span><a class="btn btn-success float-right mb-1 text-white" href="<?= base_url(); ?>additional/additemcuisineview">
                                <i class="feather icon-plus mr-1"></i>Add Item Cuisine</a></span></h4>
                </div>
                <!-- merchant category Table starts -->
                <div class="table-responsive">
                    <table class="table data-thumb-view">
                        <thead>
                            <tr>
                            <th>No</th>
                                    <th>Cuisine Name</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1;
                                foreach ($itemcuisine as $mc) { ?>
                                    <tr>
                                        <h1 id="idkat<?= $i ?>" style="display:none;"><?= $mc['id_cuisine']; ?></h1>
                                        <h1 id="statm<?= $i ?>" style="display:none;"><?= $mc['status_cuisine']; ?></h1>
                                        <td><?= $i ?></td>
                                        <td id="namkat<?= $i ?>"><?= $mc['name_cuisine']; ?></td>
                                        <td>
                                            <div>
                                                <?php if ($mc['status_cuisine'] == 1) { ?>
                                                    <label class="badge badge-success">Active
                                                    </label>
                                                <?php } else { ?>
                                                    <label class="badge badge-danger">Non Active
                                                    </label>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td>
                                        <span class="mr-1">
                                                                <a href="<?= base_url(); ?>additional/edititemcuisineview/<?= $mc['id_cuisine']; ?>">
                                                                    <i class="feather icon-edit text-info"></i>
                                                                </a>
                                                            </span>
                                                            <span class="action-delete mr-1">
                                                                <a href="<?= base_url(); ?>additional/deleteitemcuisine/<?= $mc['id_cuisine']; ?>" onclick="return confirm ('are you sure want to delete?')">
                                                                    <i class="feather icon-trash text-danger"></i>
                                                                </a>
                                                            </span>
                                        </td>
                                    </tr>

                                <?php $i++;
                                } ?>

                        </tbody>
                    </table>
                </div>
                <!-- merchant category data Table ends -->

                
            </section>
            <!-- Data list view end -->
        </div>
    </div>
</div>
<!-- END: Content-->