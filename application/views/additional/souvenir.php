<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- withdraw data start -->
            <section id="data-thumb-view" class="data-thumb-view-header">
                <div class="card-header">
                    <h4>Services<span><a class="btn btn-success float-right mb-1 text-white" href="<?= base_url(); ?>additional/addsouvenir">
                                <i class="feather icon-plus mr-1"></i>Add Souvenir</a></span></h4>
                </div>
                <div class="table-responsive">
                    <table class="table data-thumb-view">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Souvenir</th>
                                <th>Gambar</th>
                                <th>Poin Buy</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;
                            foreach ($souvenir as $srvc) { ?>
                                <tr>
                                    <td class="product-name"><?= $i ?></td>
                                    <td class="product-name"><?= $srvc['name_souvenir']; ?></td>
                                    <td class="product-img">
                                        <div class="badge badge-primary">
                                            <img src="<?= base_url('images/souvenir/') . $srvc['img_souvenir']; ?>">
                                        </div>
                                    </td>
                                    <td class="product-name">
                                        <?= number_format($srvc['poin_buy'], 0, ".", ".") ?> Poin
                                    </td>

                                    <td>
                                        <?php if ($srvc['status_souvenir'] == 1) { ?>
                                            <label class="badge badge-success">Active</label>
                                        <?php } else { ?>
                                            <label class="badge badge-danger">Non Active</label>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <span class="action-edit mr-1">
                                            <a href="<?= base_url(); ?>additional/editsouvenir/<?= $srvc['id_souvenir']; ?>">
                                                <i class="feather icon-edit text-info"></i>
                                            </a>
                                        </span>
                                        <span class="action-delete mr-1">
                                            <span class="action-delete mr-1">
                                                <a onclick="return confirm ('Are You Sure?')" href="<?= base_url(); ?>additional/deletesouvenir/<?= $srvc['id_souvenir']; ?>">
                                                    <i class="feather icon-trash text-danger"></i></a>
                                            </span>
                                    </td>
                                <?php $i++;
                            } ?>
                        </tbody>
                    </table>
                </div>

            </section>
            <!-- end of withdraw data -->
        </div>
    </div>
</div>
<!-- END: Content-->