<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Wallet count starts -->
            <section id="statistics-card">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex align-items-start pb-0">
                                <div>
                                    <h2 class="text-bold-700 mb-0">
                                        <?php
                                        $apprevenue = ($totalpoinadd['total'] - $totalpoinredeem['total']);
                                        ?>
                                        <?= number_format($apprevenue, 0, ".", ".") ?>
                                    </h2>
                                    <p>Current App Revenue</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex align-items-start pb-0">
                                <div>
                                    <h2 class="text-bold-700 mb-0">

                                        <?= number_format($balancepoin['total'], 0, ".", ".") ?>
                                    </h2>
                                    <p>User Wallet Poin Amount</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex align-items-start pb-0">
                                <div>
                                    <h2 class="text-bold-700 mb-0">
                                        <?= number_format($totalpoinadd['total'], 0, ".", ".") ?>
                                    </h2>
                                    <p>Current Add Poin Amount</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex align-items-start pb-0">
                                <div>
                                    <h2 class="text-bold-700 mb-0">
                                        <?= number_format($totalpoinredeem['total'], 0, ".", ".") ?>
                                    </h2>
                                    <p>Current Redeem Poin Amount</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Wallet count end -->

            <!-- Wallet data start -->
            <section id="basic-tabs-components">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card overflow-hidden">
                            <div class="card-header">
                                <h4 class="card-title">Wallet Poin Data</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="allwallet-tab" data-toggle="tab" href="#allwallet" aria-controls="allwallet" role="tab" aria-selected="true">All Wallet</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="allwallet" aria-labelledby="allwallet-tab" role="tabpanel">
                                            <!-- recent all wallet table start -->
                                            <section id="basic-datatable">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">All Wallet Poin</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body card-dashboard">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped dataex-html5-selectors">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>Date</th>
                                                                                    <th>Users</th>
                                                                                    <th>Name</th>
                                                                                    <th>amount</th>
                                                                                    <th>Type</th>
                                                                                    <th>Keterangan</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php $i = 1;
                                                                                foreach ($walletpoin as $wlt) { ?>
                                                                                    <tr>
                                                                                        <td><?= $i ?></td>
                                                                                        <td><?= $wlt['date_wallet_poin'] ?></td>

                                                                                        <?php $caracter = substr($wlt['id_user'], 0, 1);
                                                                                        if ($caracter == 'C') { ?>
                                                                                            <td class="text-primary">Customer</td>
                                                                                        <?php } else { ?>
                                                                                            <td class="text-warning">Driver</td>

                                                                                        <?php } ?>

                                                                                        <td><?= $wlt['driver_name'] ?><?= $wlt['customer_fullname'] ?></td>
                                                                                        <?php if ($wlt['type'] == 'Add') { ?>
                                                                                            <td class="text-success">
                                                                                                <?= number_format($wlt['amount_poin'], 0, ".", ".") ?>
                                                                                            </td>

                                                                                        <?php } else { ?>
                                                                                            <td class="text-danger">
                                                                                                <?= number_format($wlt['amount_poin'], 0, ".", ".") ?>
                                                                                            </td>
                                                                                        <?php } ?>

                                                                                        <?php if ($wlt['type'] == 'Add') { ?>
                                                                                            <td>
                                                                                                <label class="badge badge-success"><?= $wlt['type'] ?></label>
                                                                                            </td>
                                                                                        <?php } else { ?>
                                                                                            <td>
                                                                                                <label class="badge badge-danger"><?= $wlt['type'] ?></label>
                                                                                            </td>
                                                                                        <?php } ?>


                                                                                        <td><?= $wlt['keterangan_poin'] ?></td>
                                                                                    </tr>
                                                                                <?php $i++;
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!-- end of all wallet table -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end of wallet data -->
        </div>
    </div>
</div>
<!-- END: Content-->