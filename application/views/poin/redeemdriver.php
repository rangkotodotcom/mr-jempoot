<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- top up data start -->
            <section id="data-thumb-view" class="data-thumb-view-header">
                <section id="basic-tabs-components">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card overflow-hidden">
                                <div class="card-header">
                                    <h4 class="card-title">Driver Redeem Poin</h4>
                                </div>

                                <div class="card-content">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="pending-tab" data-toggle="tab" href="#pending" aria-controls="banktransfer" role="tab" aria-selected="false">Pending Request</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="allredeem-tab" data-toggle="tab" href="#allredeem" aria-controls="allredeem" role="tab" aria-selected="true">All Redeem</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="pending" aria-labelledby="pending-tab" role="tabpanel">
                                                <!-- bank transfer table start -->
                                                <section id="basic-datatable">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h4 class="card-title">Redeem Request</h4>
                                                                </div>
                                                                <div class="card-content">
                                                                    <div class="card-body card-dashboard">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped dataex-html5-selectors">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>No.</th>
                                                                                        <th>Nama</th>
                                                                                        <th>Date</th>
                                                                                        <th>Souvenir</th>
                                                                                        <th>Gambar</th>
                                                                                        <th>Poin</th>
                                                                                        <th>Status</th>
                                                                                        <th>Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php $i = 1;
                                                                                    foreach ($redeemdriverpending as $wlt) { ?>
                                                                                        <tr>
                                                                                            <td><?= $i ?></td>
                                                                                            <td><?= $wlt['driver_name'] ?></td>
                                                                                            <td><?= date('D, j F Y', strtotime($wlt['date'])) ?></td>
                                                                                            <td><?= $wlt['name_souvenir'] ?></td>
                                                                                            <td>
                                                                                                <img src="<?= base_url('images/souvenir/') . $wlt['img_souvenir'] ?>" alt="<?= $wlt['name_souvenir']; ?>">
                                                                                            </td>
                                                                                            <td>
                                                                                                <?= number_format($wlt['poin_buy'], 0, ".", ".") ?></td>
                                                                                            </td>

                                                                                            <td>
                                                                                                <label class="badge badge-secondary text-dark">Pending</label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <a href="<?= base_url(); ?>poin/tconfirm/<?= $wlt['id_redeem_poin'] ?>/<?= $wlt['id_user'] ?>/<?= $wlt['poin_buy'] ?>">
                                                                                                    <button class="btn-sm btn-outline-success">Confirm</button>
                                                                                                </a>
                                                                                                <a href="<?= base_url(); ?>poin/tcancel/<?= $wlt['id_redeem_poin'] ?>/<?= $wlt['id_user'] ?>">
                                                                                                    <button onclick="return confirm ('Are You Sure?')" class="btn-sm btn-outline-danger">Cancel</button>
                                                                                                </a>

                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php $i++;
                                                                                    } ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <!-- end of bank transfer table -->
                                            </div>

                                            <div class="tab-pane" id="allredeem" aria-labelledby="allredeem-tab" role="tabpanel">
                                                <!-- all top up table start -->
                                                <section id="basic-datatable">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h4 class="card-title">All Redeem Poin</h4>
                                                                </div>
                                                                <div class="card-content">
                                                                    <div class="card-body card-dashboard">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped dataex-html5-selectors">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>No.</th>
                                                                                        <th>Nama</th>
                                                                                        <th>Date</th>
                                                                                        <th>Souvenir</th>
                                                                                        <th>Gambar</th>
                                                                                        <th>Poin</th>
                                                                                        <th>Status</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php $i = 1;
                                                                                    foreach ($redeemdriver as $wlt) { ?>
                                                                                        <tr>
                                                                                            <td><?= $i ?></td>
                                                                                            <td><?= $wlt['driver_name'] ?></td>
                                                                                            <td><?= date('D, J F Y', strtotime($wlt['date'])) ?></td>
                                                                                            <td><?= $wlt['name_souvenir'] ?></td>
                                                                                            <td>
                                                                                                <img src="<?= base_url('images/souvenir/') . $wlt['img_souvenir'] ?>" alt="<?= $wlt['name_souvenir']; ?>">
                                                                                            </td>
                                                                                            <td class="text-success">
                                                                                                <?= number_format($wlt['poin_buy'], 0, ".", ".") ?></td>

                                                                                            <?php if ($wlt['status_redeem'] == '0') { ?>
                                                                                                <td>
                                                                                                    <label class="badge badge-secondary text-dark">Pending</label>
                                                                                                </td>
                                                                                            <?php }
                                                                                            if ($wlt['status_redeem'] == '1') { ?>
                                                                                                <td>
                                                                                                    <label class="badge badge-success">Success</label>
                                                                                                </td>
                                                                                            <?php }
                                                                                            if ($wlt['status_redeem'] == '2') { ?>
                                                                                                <td>
                                                                                                    <label class="badge badge-danger">Canceled</label>
                                                                                                </td>
                                                                                            <?php } ?>
                                                                                        </tr>
                                                                                    <?php $i++;
                                                                                    } ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <!-- end of all top up table -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
            <!-- end of top up data -->
        </div>
    </div>
</div>
<!-- END: Content-->