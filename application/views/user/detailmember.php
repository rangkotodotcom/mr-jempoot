<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- customer data Start -->
            <!-- customer tabs start -->
            <section id="basic-tabs-components">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="card overflow-hidden">
                            <div class="card-header">
                                <h4 class="card-title">Member Detail</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">

                                    <table class="table table-striped dataex-html5-selectors">
                                        <tr>
                                            <td>User ID</td>
                                            <td><?= $member['id_customer']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Customer Fullname</td>
                                            <td><?= $member['customer_fullname']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Identity Number</td>
                                            <td><?= $member['identity_number']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>User Image</td>
                                            <td>
                                                <img class="round" style="width: 200px; height: 200px;" src="<?= base_url('images/customer/') . $member['customer_image']; ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Identity Image</td>
                                            <td>
                                                <img class="round" style="width: 400px; height: 200px;" src="<?= base_url('images/member/') . $member['identity_image']; ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Birthday</td>
                                            <td><?= $member['dob']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Sex</td>
                                            <td><?= $member['jk']; ?></td>
                                        </tr>

                                        <?php if ($member['status_member'] == '1') : ?>

                                            <tr>
                                                <td>Status Member</td>
                                                <td>
                                                    <label class="badge badge-dark">Pending</label>
                                                </td>
                                            </tr>

                                        <?php else : ?>

                                            <tr>
                                                <td>No Member</td>
                                                <td><?= $member['no_member']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td>
                                                    <?php if ($member['type'] == 'c') : ?>

                                                        <span class="badge badge-red">Classic</span>

                                                    <?php elseif ($member['type'] == 's') : ?>

                                                        <span class="badge badge-default">Silver</span>

                                                    <?php elseif ($member['type'] == 'g') : ?>

                                                        <span class="badge badge-yellow">Gold</span>

                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Valid Until</td>
                                                <td><?= $member['valid_until']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Status Member</td>
                                                <td>
                                                    <?php if ($member['status_member'] == '2') { ?>
                                                        <label class="badge badge-success">Aktif</label>
                                                    <?php } else if ($member['status_member'] == '3') { ?>
                                                        <label class="badge badge-danger">Tidak Aktif</label>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah Transaksi</td>
                                                <td><?= $trx; ?></td>
                                            </tr>

                                        <?php endif; ?>

                                    </table>
                                </div>

                                <div class="card-footer">
                                    <?php if ($member['status_member'] == '1') { ?>
                                        <span class="mr-1 ml-1">
                                            <a class="btn btn-primary" href="<?= base_url(); ?>user/approvmember/<?= $member['id_member']; ?>">
                                                Approv
                                            </a>
                                        </span>
                                        <span class="mr-1 ml-1">
                                            <a class="btn btn-warning" href="<?= base_url(); ?>user/rejectmember/<?= $member['id_member']; ?>">
                                                Reject
                                            </a>
                                        </span>
                                    <?php } else { ?>

                                        <span class="mr-1 ml-1">
                                            <a class="btn btn-success" href="<?= base_url(); ?>user/upgrademember/<?= $member['id_member']; ?>">
                                                Upgrade
                                            </a>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end of customer tab -->
            <!-- end of customer data -->
        </div>
    </div>
</div>
<!-- END: Content-->