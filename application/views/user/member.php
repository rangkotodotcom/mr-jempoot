<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- customer data Start -->
            <!-- customer tabs start -->
            <section id="basic-tabs-components">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card overflow-hidden">
                            <div class="card-header">
                                <h4 class="card-title">Member Data</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="allmember-tab" data-toggle="tab" href="#allmember" aria-controls="allmember" role="tab" aria-selected="true">All Member</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="aktifmember-tab" data-toggle="tab" href="#aktifmember" aria-controls="aktifmember" role="tab" aria-selected="false">Member Akitf</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="nonaktifmember-tab" data-toggle="tab" href="#nonaktifmember" aria-controls="nonaktifmember" role="tab" aria-selected="false">Member Non Akitf</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="allmember" aria-labelledby="allmember-tab" role="tabpanel">
                                            <!-- start all customer data table -->
                                            <section id="column-selectors">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">All Member Data</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body card-dashboard">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped dataex-html5-selectors">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No</th>
                                                                                    <th>Users Id</th>
                                                                                    <th>Full Name</th>
                                                                                    <th>Image Identity</th>
                                                                                    <th>Identity No</th>
                                                                                    <th>Member No</th>
                                                                                    <th>Type</th>
                                                                                    <th>Status</th>
                                                                                    <th>Actions</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php $i = 1;
                                                                                foreach ($member as $cstm) { ?>
                                                                                    <tr>
                                                                                        <td><?= $i ?></td>
                                                                                        <td><?= $cstm['id'] ?></td>
                                                                                        <td><?= $cstm['customer_fullname'] ?></td>
                                                                                        <td>
                                                                                            <img class="round" style="width: 40px; height: 40px;" src="<?= base_url('images/member/') . $cstm['identity_image']; ?>">
                                                                                        </td>
                                                                                        <td><?= $cstm['identity_number'] ?></td>
                                                                                        <td><?= $cstm['no_member'] ?></td>
                                                                                        <td>
                                                                                            <?php if ($cstm['type'] == 'c') : ?>

                                                                                                <span class="badge badge-red">Classic</span>

                                                                                            <?php elseif ($cstm['type'] == 's') : ?>

                                                                                                <span class="badge badge-default">Silver</span>

                                                                                            <?php elseif ($cstm['type'] == 'g') : ?>

                                                                                                <span class="badge badge-yellow">Gold</span>

                                                                                            <?php else : ?>

                                                                                                <span></span>

                                                                                            <?php endif; ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php if ($cstm['status_member'] == '1') { ?>
                                                                                                <label class="badge badge-dark">Pending</label>
                                                                                            <?php } else if ($cstm['status_member'] == '2') { ?>
                                                                                                <label class="badge badge-success">Aktif</label>
                                                                                            <?php } else if ($cstm['status_member'] == '3') { ?>
                                                                                                <label class="badge badge-red">Tidak Aktif</label>
                                                                                            <?php } ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <span class="">
                                                                                                <a href="<?= base_url(); ?>user/detailmember/<?= $cstm['id_member'] ?>">
                                                                                                    <i class="feather icon-eye text-success"></i>
                                                                                                </a>
                                                                                            </span>
                                                                                            <?php if ($cstm['status_member'] == '3') { ?>
                                                                                                <span class="mr-1 ml-1">
                                                                                                    <a href="<?= base_url(); ?>user/unblockmember/<?= $cstm['id_member']; ?>">
                                                                                                        <i class="feather icon-lock text-info"></i>
                                                                                                    </a>
                                                                                                </span>
                                                                                            <?php } else if ($cstm['status_member'] == '2') { ?>
                                                                                                <span class="mr-1 ml-1">
                                                                                                    <a href="<?= base_url(); ?>user/blockmember/<?= $cstm['id_member']; ?>">
                                                                                                        <i class="feather icon-unlock text-dark"></i>
                                                                                                    </a>
                                                                                                </span>
                                                                                            <?php } ?>
                                                                                            <span class="action-delete ml-1">
                                                                                                <a onclick="return confirm ('Are You Sure?')" href="<?= base_url(); ?>user/deletemember/<?= $cstm['id_member']; ?>">
                                                                                                    <i class="feather icon-trash text-danger"></i></a>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                <?php $i++;
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!-- end of all customer data table -->
                                        </div>

                                        <div class="tab-pane" id="aktifmember" aria-labelledby="aktifmember-tab" role="tabpanel">
                                            <!-- start all blocked customer data table -->
                                            <section id="column-selectors">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">All Active Member Data</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body card-dashboard">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped dataex-html5-selectors">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No</th>
                                                                                    <th>Users Id</th>
                                                                                    <th>Full Name</th>
                                                                                    <th>Image Identity</th>
                                                                                    <th>Identity No</th>
                                                                                    <th>Member No</th>
                                                                                    <th>Type</th>
                                                                                    <th>Actions</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php $i = 1;
                                                                                foreach ($member as $cstm) {
                                                                                    if ($cstm['status_member'] == '2') { ?>
                                                                                        <tr>
                                                                                            <td><?= $i ?></td>
                                                                                            <td><?= $cstm['id'] ?></td>
                                                                                            <td><?= $cstm['customer_fullname'] ?></td>
                                                                                            <td>
                                                                                                <img class="round" style="width: 40px; height: 40px;" src="<?= base_url('images/member/') . $cstm['identity_image']; ?>">
                                                                                            </td>
                                                                                            <td><?= $cstm['identity_number'] ?></td>
                                                                                            <td><?= $cstm['no_member'] ?></td>
                                                                                            <td>
                                                                                                <?php if ($cstm['type'] == 'c') : ?>

                                                                                                    <span class="badge badge-red">Classic</span>

                                                                                                <?php elseif ($cstm['type'] == 's') : ?>

                                                                                                    <span class="badge badge-default">Silver</span>

                                                                                                <?php elseif ($cstm['type'] == 'g') : ?>

                                                                                                    <span class="badge badge-yellow">Gold</span>

                                                                                                <?php else : ?>

                                                                                                    <span></span>

                                                                                                <?php endif; ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <label class="badge badge-success">Aktif</label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span class="">
                                                                                                    <a href="<?= base_url(); ?>user/detailmember/<?= $cstm['id_member'] ?>">
                                                                                                        <i class="feather icon-eye text-success"></i>
                                                                                                    </a>
                                                                                                </span>
                                                                                                <span class="action-delete ml-1">
                                                                                                    <a onclick="return confirm ('Are You Sure?')" href="<?= base_url(); ?>user/deletemember/<?= $cstm['id_member']; ?>">
                                                                                                        <i class="feather icon-trash text-danger"></i></a>
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                <?php $i++;
                                                                                    }
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!-- end of all blocked customer data table -->
                                        </div>

                                        <div class="tab-pane" id="nonaktifmember" aria-labelledby="nonaktifmember-tab" role="tabpanel">
                                            <!-- start all blocked customer data table -->
                                            <section id="column-selectors">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">All Inactive Member Data</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body card-dashboard">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped dataex-html5-selectors">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No</th>
                                                                                    <th>Users Id</th>
                                                                                    <th>Full Name</th>
                                                                                    <th>Image Identity</th>
                                                                                    <th>Identity No</th>
                                                                                    <th>Member No</th>
                                                                                    <th>Type</th>
                                                                                    <th>Actions</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php $i = 1;
                                                                                foreach ($member as $cstm) {
                                                                                    if ($cstm['status_member'] == '3') { ?>
                                                                                        <tr>
                                                                                            <td><?= $i ?></td>
                                                                                            <td><?= $cstm['id'] ?></td>
                                                                                            <td><?= $cstm['customer_fullname'] ?></td>
                                                                                            <td>
                                                                                                <img class="round" style="width: 40px; height: 40px;" src="<?= base_url('images/member/') . $cstm['identity_image']; ?>">
                                                                                            </td>
                                                                                            <td><?= $cstm['identity_number'] ?></td>
                                                                                            <td><?= $cstm['no_member'] ?></td>
                                                                                            <td>
                                                                                                <?php if ($cstm['type'] == 'c') : ?>

                                                                                                    <span class="badge badge-red">Classic</span>

                                                                                                <?php elseif ($cstm['type'] == 's') : ?>

                                                                                                    <span class="badge badge-default">Silver</span>

                                                                                                <?php elseif ($cstm['type'] == 'g') : ?>

                                                                                                    <span class="badge badge-yellow">Gold</span>

                                                                                                <?php else : ?>

                                                                                                    <span></span>

                                                                                                <?php endif; ?>
                                                                                            </td>
                                                                                            <td>
                                                                                                <label class="badge badge-dark">Non Aktif</label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span class="">
                                                                                                    <a href="<?= base_url(); ?>user/detailmember/<?= $cstm['id_member'] ?>">
                                                                                                        <i class="feather icon-eye text-success"></i>
                                                                                                    </a>
                                                                                                </span>
                                                                                                <span class="action-delete ml-1">
                                                                                                    <a onclick="return confirm ('Are You Sure?')" href="<?= base_url(); ?>user/deletemember/<?= $cstm['id_member']; ?>">
                                                                                                        <i class="feather icon-trash text-danger"></i></a>
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                <?php $i++;
                                                                                    }
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!-- end of all blocked customer data table -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end of customer tab -->
            <!-- end of customer data -->
        </div>
    </div>
</div>
<!-- END: Content-->