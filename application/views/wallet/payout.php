<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- payout count starts -->
            <section id="statistics-card">
                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex align-items-start pb-0">
                                <div>
                                    <h2 class="text-bold-700 mb-0">
                                        <?= $currency ?>
                                        <?= number_format($balance['balance'], 2, ".", ".") ?>
                                    </h2>
                                    <p>Balance Midtrans IRIS</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- payout count end -->

            <!-- Payout data start -->
            <section id="basic-tabs-components">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card overflow-hidden">
                            <div class="card-header">
                                <h4 class="card-title">Payout Data</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="allpayout-tab" data-toggle="tab" href="#allpayout" aria-controls="allpayout" role="tab" aria-selected="true">All Payout</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="history-tab" data-toggle="tab" href="#history" aria-controls="history" role="tab" aria-selected="false">History</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="allpayout" aria-labelledby="allpayout-tab" role="tabpanel">
                                            <!-- recent all payout table start -->
                                            <section id="basic-datatable">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">All Payout</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body card-dashboard">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped dataex-html5-selectors">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>No. Reference</th>
                                                                                    <th>Date</th>
                                                                                    <th>Users</th>
                                                                                    <th>Name</th>
                                                                                    <th>Amount</th>
                                                                                    <th>Status</th>
                                                                                    <th>Action</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php $i = 1;
                                                                                foreach ($payout as $wlt) { ?>
                                                                                    <tr>
                                                                                        <td><?= $i ?></td>
                                                                                        <td><?= $wlt['reference_no'] ?></td>
                                                                                        <td><?= $wlt['create_at_payout'] ?></td>

                                                                                        <?php $caracter = substr($wlt['id_user'], 0, 1);
                                                                                        if ($caracter == 'C') { ?>
                                                                                            <td class="text-primary">Customer</td>
                                                                                        <?php } elseif ($caracter == 'M') { ?>
                                                                                            <td class="text-success">Merchant</td>
                                                                                        <?php } else { ?>
                                                                                            <td class="text-warning">Driver</td>

                                                                                        <?php } ?>

                                                                                        <td><?= $wlt['driver_name'] ?><?= $wlt['customer_fullname'] ?><?= $wlt['partner_name'] ?></td>

                                                                                        <td class="text-danger">
                                                                                            <?= $currency ?>
                                                                                            <?= number_format($wlt['wallet_amount'], 2, ".", ".") ?>
                                                                                        </td>

                                                                                        <?php if ($wlt['status_payout'] == 'queued') { ?>
                                                                                            <td>
                                                                                                <label class="badge badge-secondary text-dark">
                                                                                                    <?= $wlt['status_payout']; ?>
                                                                                                </label>
                                                                                            </td>
                                                                                        <?php }
                                                                                        if ($wlt['status_payout'] == 'approved' || $wlt['status_payout'] == 'processed') { ?>
                                                                                            <td>
                                                                                                <label class="badge badge-primary">
                                                                                                    <?= $wlt['status_payout']; ?>
                                                                                                </label>
                                                                                            </td>
                                                                                        <?php }
                                                                                        if ($wlt['status_payout'] == 'completed') { ?>
                                                                                            <td>
                                                                                                <label class="badge badge-success"><?= $wlt['status_payout']; ?></label>
                                                                                            </td>
                                                                                        <?php }
                                                                                        if ($wlt['status_payout'] == 'failed' || $wlt['status_payout'] == 'rejected') { ?>
                                                                                            <td>
                                                                                                <label class="badge badge-danger"><?= $wlt['status_payout']; ?></label>
                                                                                            </td>
                                                                                        <?php } ?>

                                                                                        <td>
                                                                                            <a href="javascript:;" data-referenceno="<?= $wlt['reference_no']; ?>" data-toggle="modal" data-target="#detailpayout">
                                                                                                <button class="btn-sm btn-outline-primary">Detail</button>
                                                                                            </a>
                                                                                        </td>
                                                                                    </tr>
                                                                                <?php $i++;
                                                                                } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!-- end of all payout table -->
                                        </div>

                                        <div class="tab-pane" id="history" aria-labelledby="history-tab" role="tabpanel">
                                            <!-- recent history table start -->
                                            <section id="basic-datatable">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">History</h4>
                                                            </div>
                                                            <div class="card-content">

                                                                <div class="row mb-2 mt-2">
                                                                    <div class="col-4">

                                                                        <input type="date" id="from_date" class="form-control" name="from_date" required="required">
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <input type="date" id="to_date" class="form-control" name="to_date" required="required">
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <button type="button" id="find-data" class="btn btn-md btn-success">Cari</button>
                                                                    </div>
                                                                </div>

                                                                <div class="card-body card-dashboard">

                                                                    <div class="table-responsive">
                                                                        <table id="table-history" class="table table-striped dataex-html5-selectors">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>No. Reference</th>
                                                                                    <th>Account Name</th>
                                                                                    <th>Account Number</th>
                                                                                    <th>Amount</th>
                                                                                    <th>Bank Name</th>
                                                                                    <th>Type</th>
                                                                                    <th>Status</th>
                                                                                    <th>Created At</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody id="tbody-data">
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!-- end of recent transaction table -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end of payout data -->
        </div>
    </div>
</div>
<!-- END: Content-->



<!-- Modal -->
<!-- detail payout info -->
<div class="modal fade text-left" id="detailpayout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Detail Payout</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pr-2 pl-2">

                <section id="basic-vertical-layouts">
                    <div class="form-body">
                        <div class="row">
                            <table class="table table-striped dataex-html5-selectors">
                                <tbody>
                                    <tr>
                                        <td>No Reference</td>
                                        <td id="reference-no"></td>
                                    </tr>
                                    <tr>
                                        <td>Account Name</td>
                                        <td id="beneficiary-name"></td>
                                    </tr>
                                    <tr>
                                        <td>Account Number</td>
                                        <td id="beneficiary-account"></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td id="amount"></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td id="beneficiary-email"></td>
                                    </tr>
                                    <tr>
                                        <td>Bank Name</td>
                                        <td id="bank"></td>
                                    </tr>
                                    <tr>
                                        <td>Note</td>
                                        <td id="notes"></td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td id="status"></td>
                                    </tr>
                                    <tr>
                                        <td>Created By</td>
                                        <td id="created-by"></td>
                                    </tr>
                                    <tr>
                                        <td>Created At</td>
                                        <td id="created-at"></td>
                                    </tr>
                                    <tr>
                                        <td>Updated At</td>
                                        <td id="updated-at"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!-- detail payout info -->