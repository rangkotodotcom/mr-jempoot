<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- withdraw data start -->
            <section id="data-thumb-view" class="data-thumb-view-header">
                <div class="card-header">
                    <h4>Customer Service</h4>
                </div>
                <div class="table-responsive">
                    <table class="table data-thumb-view">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID User</th>
                                <th>Nama User</th>
                                <th>Status Pesan</th>
                                <th>Date At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;
                            foreach ($chatcs as $srvc) { ?>
                                <tr>
                                    <td class="product-name"><?= $i ?></td>
                                    <td class="product-name"><?= $srvc['id_user']; ?></td>
                                    <td class="product-name">
                                        <?php $kode = substr($srvc['id_user'], 0, 1);
                                        if ($kode == 'D') {
                                            echo $srvc['driver_name'];
                                        } else if ($kode == 'M') {
                                            echo $srvc['partner_name'];
                                        } else {
                                            echo $srvc['customer_fullname'];
                                        } ?>
                                    </td>
                                    <td class="product-name">
                                        <?php if ($srvc['status_message'] == 0) { ?>
                                            <label class="badge badge-success">Berjalan</label>
                                        <?php } else { ?>
                                            <label class="badge badge-danger">Ditutup</label>
                                        <?php } ?>

                                    </td>
                                    <td>
                                        <?= date('j F Y H:i:s', strtotime($srvc['date_created'])); ?>
                                    </td>
                                    <td>
                                        <span class="mr-1">
                                            <a href="<?= base_url(); ?>chatcs/detail/<?= $srvc['id']; ?>">
                                                <i class="feather icon-eye text-info"></i>
                                            </a>
                                        </span>
                                    </td>
                                <?php $i++;
                            } ?>
                        </tbody>
                    </table>
                </div>

            </section>
            <!-- end of withdraw data -->
        </div>
    </div>
</div>
<!-- END: Content-->