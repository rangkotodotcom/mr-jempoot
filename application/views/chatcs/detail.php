<style>
    .msage {
        margin: 0 auto;
        max-width: 800px;
        padding: 0 20px;
    }

    .pesan {
        border: 2px solid #dedede;
        background-color: #f1f1f1;
        border-radius: 5px;
        padding: 10px;
        margin: 10px 0;
    }

    .saya {
        border-color: #ccc;
        background-color: #ddd;
    }

    .pesan::after {
        content: "";
        clear: both;
        display: table;
    }

    .pesan img.profil {
        float: left;
        max-width: 60px;
        width: 100%;
        margin-right: 20px;
        border-radius: 50%;
    }

    .pesan img.profil.right {
        float: right;
        margin-left: 20px;
        margin-right: 0;
    }

    .time-right {
        float: right;
        color: #aaa;
    }

    .time-left {
        float: left;
        color: #999;
    }
</style>
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Data list view starts -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Chat CS</h4>
                            </div>
                            <div class="card-header">
                                <a class="btn btn-success mb-1 text-white" href="<?= base_url(); ?>chatcs">Kembali</a>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="msage">

                                        <?php foreach ($chatcs['detail_chat'] as $row) : ?>
                                            <div class="pesan <?php if ($row['id_user'] == 'Admin') {
                                                                    echo "saya";
                                                                } ?>">

                                                <?php if ($row['id_user'] != 'Admin') : ?>

                                                    <img src="<?= base_url($url_foto . $chatcs[$index_foto]); ?>" alt="Avatar" class="profil" style="width:100%;">

                                                <?php else : ?>

                                                    <img src="https://dosenit.com/wp-content/uploads/2021/02/136-1369892_avatar-people-person-business-user-man-character-avatar.png" alt="Avatar" class="right profil" style="width:100%;">

                                                <?php endif; ?>

                                                <?php if ($row['message'] != '') : ?>
                                                    <p><?= $row['message']; ?></p>
                                                <?php endif; ?>

                                                <?php if ($row['photo'] != '') : ?>
                                                    <img src="<?= base_url('images/chatcs/' . $row['photo']); ?>" alt="" class="img-fluid mt-3">
                                                <?php endif; ?>

                                                <?php if ($row['id_user'] != 'Admin') : ?>

                                                    <span class="time-right"><?= $row['created_at']; ?></span>

                                                <?php else : ?>

                                                    <span class="time-left"><?= $row['created_at']; ?></span>

                                                <?php endif; ?>
                                            </div>

                                        <?php endforeach; ?>

                                        <?php if ($chatcs['status_message'] == '0') : ?>

                                            <hr class="mt-2 m-b-1">

                                            <form action="<?= base_url('chatcs/send') ?>" method="post" enctype="multipart/form-data">

                                                <input type="hidden" name="id_message" value="<?= $chatcs['id']; ?>">
                                                <input type="hidden" name="id_user" value="<?= $chatcs['id_user']; ?>">

                                                <div class="form-group">
                                                    <input type="file" class="form-control-file" name="photo">
                                                </div>

                                                <div class="form-group">
                                                    <textarea class="form-control" name="message" rows="2"></textarea>
                                                </div>

                                                <button class="btn btn-md btn-success">
                                                    <span class="fa fa-send"></span>
                                                </button>
                                            </form>

                                        <?php endif; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Data list view end -->
        </div>
    </div>
</div>
<!-- END: Content-->