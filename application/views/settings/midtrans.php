<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- start midtrans setting -->

            <section id="basic-vertical-layouts">
                <div class="row match-height justify-content-center">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Midtrans Settings</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <?= form_open_multipart('settings/editmidtrans'); ?>
                                    <form class="form form-vertical">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="midtrans">Midtrans ID </label>
                                                        <input type="text" class="form-control" id="midtrans" name="midtrans_id" value="<?= $appsettings['midtrans_id'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtrans_mode">Midtrans Mode</label>
                                                        <select name="midtrans_mode" id="midtrans_mode" class="select2 form-group" style="width:100%">
                                                            <option value="sandbox" <?php if ($appsettings['midtrans_mode'] == 'sandbox') { ?>selected<?php } ?>>Development Mode</option>
                                                            <option value="production" <?php if ($appsettings['midtrans_mode'] == 'production') { ?>selected<?php } ?>>Published</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-6">

                                                    <div class="form-group">
                                                        <label for="midtransclientkey">Midtrans Client Key</label>
                                                        <input type="text" class="form-control" id="midtransclientkey" name="midtrans_client_key" value="<?= $appsettings['midtrans_client_key'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtransserverkey">Midtrans Server Key</label>
                                                        <input type="text" class="form-control" id="midtransserverkey" name="midtrans_server_key" value="<?= $appsettings['midtrans_server_key'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtransbaseurl">Midtrans Base Url</label>
                                                        <input type="url" class="form-control" id="midtransbaseurl" name="midtrans_baseurl" value="<?= $appsettings['midtrans_baseurl'] ?>" required>
                                                    </div>

                                                </div>

                                                <div class="col-6">

                                                    <div class="form-group">
                                                        <label for="midtransclientkeysb">Midtrans Sandbox Client Key</label>
                                                        <input type="text" class="form-control" id="midtransclientkeysb" name="midtrans_client_key_sb" value="<?= $appsettings['midtrans_client_key_sb'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtransserverkeysb">Midtrans Sandbox Server Key</label>
                                                        <input type="text" class="form-control" id="midtransserverkeysb" name="midtrans_server_key_sb" value="<?= $appsettings['midtrans_server_key_sb'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtransbaseurlsb">Midtrans Sandbox Base Url</label>
                                                        <input type="url" class="form-control" id="midtransbaseurlsb" name="midtrans_baseurl_sb" value="<?= $appsettings['midtrans_baseurl_sb'] ?>" required>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                </div>
                                            </div>
                                    </form>
                                    <?= form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- end of midtrans setting data -->



            <!-- start midtrans payout setting -->

            <section id="basic-vertical-layouts">
                <div class="row match-height justify-content-center">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Midtrans Payout Settings</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <?= form_open_multipart('settings/editmidtranspayout'); ?>
                                    <form class="form form-vertical">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12">

                                                    <div class="form-group">
                                                        <label for="midtrans_payout_mode">Midtrans Payout Mode</label>
                                                        <select name="midtrans_payout_mode" id="midtrans_payout_mode" class="select2 form-group" style="width:100%">
                                                            <option value="sandbox" <?php if ($appsettings['midtrans_payout_mode'] == 'sandbox') { ?>selected<?php } ?>>Development Mode</option>
                                                            <option value="production" <?php if ($appsettings['midtrans_payout_mode'] == 'production') { ?>selected<?php } ?>>Published</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-6">

                                                    <div class="form-group">
                                                        <label for="midtranspayoutapproverapikey">Midtrans Payout Approver API Key</label>
                                                        <input type="text" class="form-control" id="midtranspayoutapproverapikey" name="midtrans_payout_approver_apikey" value="<?= $appsettings['midtrans_payout_approver_apikey'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtranspayoutcreatorapikey">Midtrans Payout Creator API Key</label>
                                                        <input type="text" class="form-control" id="midtranspayoutcreatorapikey" name="midtrans_payout_creator_apikey" value="<?= $appsettings['midtrans_payout_creator_apikey'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtranspayoutmerchantkey">Midtrans Merchant Key</label>
                                                        <input type="text" class="form-control" id="midtranspayoutmerchantkey" name="midtrans_payout_merchantkey" value="<?= $appsettings['midtrans_payout_merchantkey'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtranspayoutbaseurl">Midtrans Payout Base Url</label>
                                                        <input type="url" class="form-control" id="midtranspayoutbaseurl" name="midtrans_payout_baseurl" value="<?= $appsettings['midtrans_payout_baseurl'] ?>" required>
                                                    </div>

                                                </div>


                                                <div class="col-6">

                                                    <div class="form-group">
                                                        <label for="midtranspayoutapproverapikeysb">Midtrans Sandbox Payout Approver API Key</label>
                                                        <input type="text" class="form-control" id="midtranspayoutapproverapikeysb" name="midtrans_payout_approver_apikey_sb" value="<?= $appsettings['midtrans_payout_approver_apikey_sb'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtranspayoutcreatorapikeysb">Midtrans Sandbox Payout Creator API Key</label>
                                                        <input type="text" class="form-control" id="midtranspayoutcreatorapikeysb" name="midtrans_payout_creator_apikey_sb" value="<?= $appsettings['midtrans_payout_creator_apikey_sb'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtranspayoutmerchantkeysb">Midtrans Sandbox Merchant Key</label>
                                                        <input type="text" class="form-control" id="midtranspayoutmerchantkeysb" name="midtrans_payout_merchantkey_sb" value="<?= $appsettings['midtrans_payout_merchantkey_sb'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="midtranspayoutbaseurlsb">Midtrans Sandbox Payout Base Url</label>
                                                        <input type="url" class="form-control" id="midtranspayoutbaseurlsb" name="midtrans_payout_baseurl_sb" value="<?= $appsettings['midtrans_payout_baseurl_sb'] ?>" required>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                </div>
                                            </div>
                                    </form>
                                    <?= form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- end of midtrans payout setting data -->
        </div>
    </div>
</div>
<!-- END: Content-->