<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- start verihubs setting -->

            <section id="basic-vertical-layouts">
                <div class="row match-height justify-content-center">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Verihubs Settings</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <?= form_open_multipart('settings/editverihubs'); ?>
                                    <form class="form form-vertical">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="verihubs_mode">Verihubs Mode</label>
                                                        <select name="verihubs_mode" id="verihubs_mode" class="select2 form-group" style="width:100%">
                                                            <option value="sandbox" <?php if ($appsettings['verihubs_mode'] == 'sandbox') { ?>selected<?php } ?>>Development Mode</option>
                                                            <option value="production" <?php if ($appsettings['verihubs_mode'] == 'production') { ?>selected<?php } ?>>Published</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-6">

                                                    <div class="form-group">
                                                        <label for="verihubskey">Verihubs Key</label>
                                                        <input type="text" class="form-control" id="verihubskey" name="verihubs_key" value="<?= $appsettings['verihubs_key'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="verihubsappid">Verihubs App ID</label>
                                                        <input type="text" class="form-control" id="verihubsappid" name="verihubs_app_id" value="<?= $appsettings['verihubs_app_id'] ?>" required>
                                                    </div>

                                                </div>

                                                <div class="col-6">

                                                    <div class="form-group">
                                                        <label for="verihubskeysb">Verihubs Sandbox Key</label>
                                                        <input type="text" class="form-control" id="verihubskeysb" name="verihubs_key_sb" value="<?= $appsettings['verihubs_key_sb'] ?>" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="verihubsappidsb">Verihubs Sandbox App ID</label>
                                                        <input type="text" class="form-control" id="verihubsappidsb" name="verihubs_app_id_sb" value="<?= $appsettings['verihubs_app_id_sb'] ?>" required>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                </div>
                                            </div>
                                    </form>
                                    <?= form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- end of verihubs setting data -->

        </div>
    </div>
</div>
<!-- END: Content-->