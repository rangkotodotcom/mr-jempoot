<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Data list view starts -->
            <section id="data-thumb-view" class="data-thumb-view-header">

                <div class="card-header">
                    <h4>Bank Account Transfer
                        <span>
                            <a class="btn btn-success float-right mb-1 ml-1 text-white" href="<?= base_url(); ?>settings/syncbank">
                                <i class="feather icon-database mr-1"></i>Sync Bank IRIS
                            </a>
                        </span>
                        <span>
                            <a class="btn btn-success float-right mb-1 text-white" href="<?= base_url(); ?>settings/addbankaccount">
                                <i class="feather icon-plus mr-1"></i>Add Bank
                            </a>
                        </span>
                    </h4>
                </div>
                <!-- slider Table starts -->
                <div class="table-responsive">
                    <table class="table data-thumb-view">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Bank Logo</th>
                                <th>Bank Code</th>
                                <th>Bank Name</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;
                            foreach ($banktransfer as $bnk) { ?>
                                <tr>

                                    <td><?= $i ?></td>
                                    <td class="product-img"><img src="<?= base_url('images/bank/') . $bnk['logo_bank']; ?>"></td>
                                    <td class="product-name"><?= $bnk['kode_bank'] ?></td>
                                    <td class="product-name"><?= $bnk['name_bank'] ?></td>
                                    <td><?php if ($bnk['status_bank'] == 1) { ?>
                                            <label class="badge badge-primary">Active</label>
                                        <?php } else if ($bnk['status_bank'] == 0) { ?>
                                            <label class="badge badge-danger">Non Active</label>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <span class="action-edit mr-1">
                                            <a href="<?= base_url(); ?>settings/editbankaccount/<?= $bnk['id']; ?>">
                                                <i class="feather icon-edit text-info"></i>
                                            </a>
                                        </span>
                                        <span class="action-delete mr-1">
                                            <span class="action-delete mr-1">
                                                <a onclick="return confirm ('Are You Sure?')" href="<?= base_url(); ?>settings/deletebankdata/<?= $bnk['id']; ?>">
                                                    <i class="feather icon-trash text-danger"></i></a>
                                            </span>
                                        </span>
                                    </td>
                                </tr>

                            <?php $i++;
                            } ?>
                        </tbody>
                    </table>
                </div>
                <!-- slider data Table ends -->


            </section>
            <!-- Data list view end -->
        </div>
    </div>
</div>
<!-- END: Content-->