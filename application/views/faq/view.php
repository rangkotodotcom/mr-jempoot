<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Data list view starts -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">FAQ Data</h4>
                            </div>
                            <div class="card-header">
                                <a class="btn btn-success mb-1 text-white" href="<?= base_url(); ?>faq/add">
                                    <i class="feather icon-plus mr-1"></i>Add FAQ</a>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Question</th>
                                                    <th>Answer</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1;
                                                foreach ($faq as $nw) { ?>
                                                    <tr>
                                                        <td><?= $i ?></td>
                                                        <td><?= $nw['pertanyaan']; ?></td>
                                                        <td><?= $nw['jawaban']; ?></td>

                                                        <td>
                                                            <span class="mr-1">
                                                                <a href="<?= base_url(); ?>faq/edit/<?= $nw['id']; ?>">
                                                                    <i class="feather icon-edit text-info"></i>
                                                                </a>
                                                            </span>
                                                            <span class="mr-1">
                                                                <a href="<?= base_url(); ?>faq/delete/<?= $nw['id']; ?>" onclick="return confirm ('are you sure want to delete?')">
                                                                    <i class="feather icon-trash text-danger"></i>
                                                                </a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?php $i++;
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Data list view end -->
        </div>
    </div>
</div>
<!-- END: Content-->