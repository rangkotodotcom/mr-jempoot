<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- start add news -->
            <div class="row match-height justify-content-center">
                <div class="col-md-8 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit FAQ</h4>
                        </div>
                        <section id="basic-vertical-layouts">
                            <div class="card-content">
                                <div class="card-body">
                                    <?= form_open_multipart('faq/save'); ?>
                                    <form class="form form-vertical">
                                        <div class="form-body">
                                            <div class="row">
                                                <input type="hidden" class="form-control" name="id" id="newstitle" value="<?= $faq['id'] ?>">

                                                <div class="col-12 form-group">
                                                    <label for="newstitle">Question</label>
                                                    <input type="text" class="form-control" name="pertanyaan" id="newstitle" value="<?= $faq['pertanyaan'] ?>" required>
                                                </div>

                                                <div class="col-12 form-group">
                                                    <label for="newscontent">Answer</label>
                                                    <textarea type="text" class="form-control" id="summernoteExample1" placeholder="Location" name="jawaban" required><?= $faq['jawaban'] ?></textarea>
                                                </div>
                                                <!-- end of add news form -->

                                                <div class="col-12">
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <?= form_close(); ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

            <!-- end of add news -->
        </div>
    </div>
</div>
<!-- END: Content-->