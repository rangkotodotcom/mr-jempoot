<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="<?= base_url(); ?>asset/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url(); ?>asset/app-assets/images/ico/logoweb.png">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title><?= $title; ?></title>
</head>

<body class="bg-danger">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-4">
                <h2 class="text-center mb-5">Frequently Asked Questions (FAQ)</h2>

                <?php if (!empty($faq)) : foreach ($faq as $row) : ?>

                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item mb-3">
                                <h2 class="accordion-header" id="heading<?= $row['id']; ?>">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?= $row['id']; ?>" aria-expanded="true" aria-controls="collapse<?= $row['id']; ?>">
                                        <?= $row['pertanyaan']; ?>
                                    </button>
                                </h2>
                                <div id="collapse<?= $row['id']; ?>" class="accordion-collapse collapse" aria-labelledby="heading<?= $row['id']; ?>" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <?= $row['jawaban']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                <?php endforeach;
                endif; ?>
            </div>

        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>

</html>