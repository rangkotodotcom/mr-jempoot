<!DOCTYPE html>
<html class="loading" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Multi Service App With Customer App, Driver App, Merchant App and Admin Panel">
	<meta name="keywords" content="car rental, codeigniter, delivery, driver, grab, maps tracking, ordering, package, ride, send, stripe, taxi, transportation, uber, wallet">
	<meta name="author" content="ourdevelops">
	<title>Mr-Jempoot Admin</title>
	<link rel="apple-touch-icon" href="https://surelabsid.com/mr-jempoot/asset/app-assets/images/ico/apple-icon-120.png">
	<link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>/asset/app-assets/images/ico/logoweb.png">
	<link href="<?= base_url() ?>/asset/app-assets/css/ourdevelops/font.css" rel="stylesheet">

	<!-- BEGIN: Vendor CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/app-assets/vendors/css/vendors.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/app-assets/vendors/css/charts/apexcharts.css">

	<!-- END: Vendor CSS-->

	<!-- BEGIN: Theme CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/app-assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/app-assets/css/bootstrap-extended.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/app-assets/css/colors.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/app-assets/css/components.css">

	<!-- BEGIN: Page CSS-->

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/app-assets/css/core/menu/menu-types/vertical-menu.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/app-assets/css/core/colors/palette-gradient.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/asset/app-assets/css/ourdevelops/chart-dashboard.css">
	<!-- END: Page CSS-->


</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static">

	<input type="hidden" id="base" value="<?= base_url() ?>"></input>
	<input type="hidden" id="merchant_id" value="<?= $merchant_id ?>"></input>
	<input type="hidden" id="date_today" value="<?= $date_today ?>"></input>
	<input type="hidden" id="tokenode" value="pk.eyJ1Ijoic2lnaXRzdXJ5b25vMjUiLCJhIjoiY2pteWhrbjV1MG11cjNrbzZnMWd0c3VvbSJ9.QuvHBpzamfisO_uMEn1YCg"></input>


	<!-- BEGIN: Content-->

	<div class="container-fluid m-0 p-0">


		<!-- start of revenue & in proggress -->
		<div class="row">
			<!-- Line Area Chart -->
			<div class="col-lg-12 col-md-6 col-md-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Statistik Penjualan 1 Minggu Terakhir</h4>
					</div>
					<div class="card-content">
						<div class="card-body">
							<div id="line-area-chart"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end of transaction statistic -->


	</div>
	<!-- END: Content-->



	<!-- BEGIN: Vendor JS-->
	<script src="<?= base_url() ?>/asset/app-assets/vendors/js/vendors.min.js"></script>

	<!-- BEGIN Vendor JS-->

	<!-- BEGIN: Page Vendor JS-->
	<script src="<?= base_url() ?>/asset/app-assets/vendors/js/charts/apexcharts.min.js"></script>

	<!-- END: Page Vendor JS-->

	<!-- BEGIN: Theme JS-->
	<script src="<?= base_url() ?>/asset/app-assets/js/core/app-menu.js"></script>
	<script src="<?= base_url() ?>/asset/app-assets/js/core/app.js"></script>
	<script src="<?= base_url() ?>/asset/app-assets/js/scripts/components.js"></script>
	<!-- END: Theme JS-->

	<!-- BEGIN: Page JS-->

	<script>
		$(document).ready(function() {
			var base_url = $("#base").val();
			var merchant_id = $("#merchant_id").val();
			var date_today = $("#date_today").val();
			var $primary = "#a357d7";
			var $warning = "#FF9F43";
			var $info = "#0DCCE1";
			var $danger = "#EA5455";
			var $success = "#00db89";

			var themeColors = [$primary, $success, $danger, $warning, $info];

			var yaxis_opposite = false;
			if ($("html").data("textdirection") == "rtl") {
				yaxis_opposite = true;
			}

			$.ajax({
				url: base_url + "api/adminapi/webview/" + date_today + "/" + merchant_id,
				type: "GET",
				success: function(data) {

					var lineAreaOptions = {
						chart: {
							height: 280,
							type: "area",
						},
						colors: themeColors,
						dataLabels: {
							enabled: false,
						},
						stroke: {
							curve: "smooth",
						},
						series: [{
								name: "Pendapatan",
								data: [
									data.data['sevenday']['profit'],
									data.data['sixday']['profit'],
									data.data['fiveday']['profit'],
									data.data['fourday']['profit'],
									data.data['threeday']['profit'],
									data.data['twoday']['profit'],
									data.data['oneday']['profit']
								],
							},

						],
						legend: {
							offsetY: -10,
						},
						xaxis: {
							categories: [
								data.data['sevenday']['date'],
								data.data['sixday']['date'],
								data.data['fiveday']['date'],
								data.data['fourday']['date'],
								data.data['threeday']['date'],
								data.data['twoday']['date'],
								data.data['oneday']['date']
							],
						},
						yaxis: {
							opposite: yaxis_opposite,
						},
						tooltip: {
							x: {
								format: "",
							},
						},
					};
					var lineAreaChart = new ApexCharts(
						document.querySelector("#line-area-chart"),
						lineAreaOptions
					);
					lineAreaChart.render();

					//Chart1 ends //
				},
			});



		});
	</script>







	<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>