

(function (window, document, $) {

  

  $('#detailpayout').on('show.bs.modal', function (event) {
		var div = $(event.relatedTarget); // Tombol dimana modal di tampilkan
		var modal = $(this);

    let base_url = $('#base').val();

    
    let reference_no = div.data('referenceno');

    let action = base_url + 'wallet/getDetailPayout/' + reference_no;

    $.ajax({
      type: "POST",
      url: action,
      dataType: 'json',
      success: function(msg) {


        modal.find('#reference-no').html(msg.reference_no);
        modal.find('#beneficiary-name').html(msg.beneficiary_name);
        modal.find('#beneficiary-account').html(msg.beneficiary_account);
        modal.find('#amount').html(msg.amount);
        modal.find('#beneficiary-email').html(msg.beneficiary_email);
        modal.find('#bank').html(msg.bank);
        modal.find('#notes').html(msg.notes);
        
        if(msg.status == 'completed'){
          modal.find('#status').html(`
            <p class="badge badge-success">` + msg.status + `</p>
          `);
        }else if(msg.status == 'processed' || msg.status == 'approved'){
          modal.find('#status').html(`
            <p class="badge badge-primary">` + msg.status + `</p>
          `);
        }else if(msg.status == 'rejected' || msg.status == 'failed'){
          modal.find('#status').html(`
            <p class="badge badge-danger">` + msg.status + `</p>
          `);
        }else if(msg.status == 'queued'){
          modal.find('#status').html(`
            <p class="badge badge-secondary text-dark">` + msg.status + `</p>
          `);
        }

        modal.find('#created-by').html(msg.created_by);
        modal.find('#created-at').html(msg.created_at);
        modal.find('#updated-at').html(msg.updated_at);
          
        
      }
    });

	});


  $('#find-data').on('click', function () {

    let base_url = $('#base').val();

    let from_date = $('#from_data').val();
    let to_date = $('#to_data').val();

    let action = base_url + 'wallet/findHistoryPayout/';

    $('#tbody-data').html('');

    let i = 1;

    $.ajax({
      type: "POST",
      url: action,
      data:{
        from_date:from_date,
        to_date: to_date
      },
      dataType: 'json',
      success: function(msg) {
        msg.forEach(function(prop){
					$('#tbody-data').append(`
						<tr>
              <td>`
                + i++ +
              `</td>
              <td>`
                + prop.reference_no +
              `</td>
              <td>`
                + prop.beneficiary_name +
              `</td>
              <td>`
                + prop.beneficiary_account +
              `</td>
              <td>`
                + prop.amount +
              `</td>
              <td>`
                + prop.account +
              `</td>
              <td>`
                + prop.type +
              `</td>
              <td>`
                + prop.status +
              `</td>
              <td>`
                + prop.created_at +
              `</td>
						</tr>
					`);
					
				});

        $('#table-history').DataTable();
      }
    });

	});


  $('#rejectW').on('show.bs.modal', function (event) {
		var div = $(event.relatedTarget); // Tombol dimana modal di tampilkan
		var modal = $(this);

    let id = div.data('id');

    modal.find('#id').attr('value', id);
    
	});


  /***** Component Variables *****/
  var alertValidationInput = $(".alert-validation"),
    alertRegex = /^[0-9]+$/,
    alertValidationMsg = $(".alert-validation-msg"),
    accordion = $(".accordion"),
    collapseTitle = $(".collapse-title"),
    collapseHoverTitle = $(".collapse-hover-title"),
    dropdownMenuIcon = $(".dropdown-icon-wrapper .dropdown-item");

  /***** Alerts *****/
  /* validation with alert */
  alertValidationInput.on('input', function () {
    if (alertValidationInput.val().match(alertRegex)) {
      alertValidationMsg.css("display", "none");
    } else {
      alertValidationMsg.css("display", "block");
    }
  });

  /***** Carousel *****/
  // For Carousel With Enabled Keyboard Controls
  $(document).on("keyup", function (e) {
    if (e.which == 39) {
      $('.carousel[data-keyboard="true"]').carousel('next');
    } else if (e.which == 37) {
      $('.carousel[data-keyboard="true"]').carousel('prev');
    }
  })

  // To open Collapse on hover
  if (accordion.attr("data-toggle-hover", "true")) {
    collapseHoverTitle.closest(".card").on("mouseenter", function () {
      $(this).children(".collapse").collapse("show");
    });
  }
  // Accordion with Shadow - When Collapse open
  $('.accordion-shadow .collapse-header .card-header').on("click", function () {
    var $this = $(this);
    $this.parent().siblings(".collapse-header.open").removeClass("open");
    $this.parent(".collapse-header").toggleClass("open");
  });

  /***** Dropdown *****/
  // For Dropdown With Icons
  dropdownMenuIcon.on("click", function () {
    $(".dropdown-icon-wrapper .dropdown-toggle i").remove();
    $(this).find("i").clone().appendTo(".dropdown-icon-wrapper .dropdown-toggle");
    $(".dropdown-icon-wrapper .dropdown-toggle .dropdown-item").removeClass("dropdown-item");
  });
+


  /***** Chips *****/
  // To close chips
  $('.chip-closeable').on('click', function () {
    $(this).closest('.chip').remove();
  })
})(window, document, jQuery);
